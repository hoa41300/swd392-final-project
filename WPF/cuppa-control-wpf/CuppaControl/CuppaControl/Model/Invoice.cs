﻿using System;
using System.Collections.Generic;

namespace CuppaControl.Model
{
    [Serializable]
    public class Invoice
    {
        public int Payment { get; set; }
        public int VoucherId { get; set; }
        public List<InvoiceDetail> InvoiceDetails { get; set; }
    }
}
