﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuppaControl.Model
{
    public class InvoiceModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public int Payment { get; set; }
        public double Total { get; set; }
        public string Status { get; set; }
        public string Username { get; set; }
        public int? VoucherId { get; set; }
        public List<InvoiceDetailModel> InvoiceDetails { get; set; }

    }
}
