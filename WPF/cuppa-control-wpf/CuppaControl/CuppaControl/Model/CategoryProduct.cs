﻿namespace CuppaControl.Model
{
    public class CategoryProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ProductImg { get; set; }
        public double Price { get; set; }
    }
}
