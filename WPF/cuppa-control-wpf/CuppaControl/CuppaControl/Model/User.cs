﻿using System;

namespace CuppaControl.Model
{
    public class User
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string StartToWork { get; set; }
        public string Phone { get; set; }
        public string Role { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string ContractExpiration { get; set; }
        public string? Avatar { get; set; }
        public string Gender { get; set; }
        public string Status { get; set; }
        public string DateOfBirth { get; set; }
        public string CreateDate { get; set; }
        public string CofficientsSalary { get; set; }
    }
}
