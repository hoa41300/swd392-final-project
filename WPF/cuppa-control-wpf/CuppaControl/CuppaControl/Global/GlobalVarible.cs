﻿using CuppaControl.Model;

namespace CuppaControl.Global
{
    public static class GlobalVarible
    {
        public static User User { get; set; }
        public static string Token { get; set; }

        public const string BaseUrl = "http://localhost:5108/api";
        static GlobalVarible()
        {
            User = null;
            Token = null;
        }

        public static void Logout()
        {
            User = null;
            Token = null;
        }
    }
}
