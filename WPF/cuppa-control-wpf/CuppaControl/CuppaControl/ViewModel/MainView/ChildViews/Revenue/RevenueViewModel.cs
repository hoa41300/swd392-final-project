﻿using System;
using System.Windows;
using System.Windows.Input;

namespace CuppaControl.ViewModel
{
    public class RevenueViewModel : ViewModelBase
    {
        #region Fields
        private ViewModelBase _currentChildView;
        private string _caption;
        private Visibility _createBtnVisibility;
        private Visibility _listBtnVisibility;
        #endregion

        #region Properties
        public ViewModelBase CurrentChildView
        {
            get
            {
                return _currentChildView;
            }
            set
            {
                _currentChildView = value;
                OnPropertyChanged(nameof(CurrentChildView));
            }
        }
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                OnPropertyChanged(nameof(Caption));
            }
        }
        public Visibility CreateBtnVisibility
        {
            get
            {
                return _createBtnVisibility;
            }
            set
            {
                _createBtnVisibility = value;
                OnPropertyChanged(nameof(CreateBtnVisibility));
            }
        }

        public Visibility ListBtnVisibility
        {
            get
            {
                return _listBtnVisibility;
            }
            set
            {
                _listBtnVisibility = value;
                OnPropertyChanged(nameof(ListBtnVisibility));
            }
        }
        #endregion

        #region Commands
        public ICommand ShowCreateInvoiceViewCommand { get; }
        public ICommand ShowInvoiceListViewCommand { get; }
        #endregion

        #region Constructors
        public RevenueViewModel()
        {
            ShowCreateInvoiceViewCommand = new ViewModelCommand(ExecuteShowCreateInvoiceViewCommand);
            ShowInvoiceListViewCommand = new ViewModelCommand(ExecuteShowInvoiceListViewCommand);

            ExecuteShowInvoiceListViewCommand(null);
        }



        #endregion

        #region Methods
        private void ExecuteShowCreateInvoiceViewCommand(object obj)
        {
            CurrentChildView = new CreateInvoiceViewModel();
            CreateBtnVisibility = Visibility.Collapsed;
            ListBtnVisibility = Visibility.Visible;
            Caption = "Create New Invoice";
        }

        private void ExecuteShowInvoiceListViewCommand(object obj)
        {
            CurrentChildView = new InvoiceListViewModel();
            ListBtnVisibility = Visibility.Collapsed;
            CreateBtnVisibility = Visibility.Visible;
            Caption = "Invoice List";
        }
        #endregion
    }
}
