﻿using CuppaControl.Global;
using CuppaControl.Model;
using FontAwesome.Sharp;
using System;
using System.Net;
using System.Windows;
using System.Windows.Input;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace CuppaControl.ViewModel
{
    public class MainViewViewModel : ViewModelBase
    {
        #region Fields
        private ViewModelBase _currentChildView;
        private string _caption;
        private IconChar _icon;

        private string _username;
        private string _image;
        #endregion

        #region Properties
        public string Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                OnPropertyChanged(nameof(Image));
            }
        }
        public ViewModelBase CurrentChildView
        {
            get
            {
                return _currentChildView;
            }
            set
            {
                _currentChildView = value;
                OnPropertyChanged(nameof(CurrentChildView));
            }
        }
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                OnPropertyChanged(nameof(Caption));
            }
        }
        public IconChar Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
                OnPropertyChanged(nameof(Icon));
            }
        }
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                OnPropertyChanged(nameof(Username));
            }
        }

        #endregion

        #region Commands
        public ICommand ShowWarehouseViewCommand { get; }
        public ICommand ShowProductViewCommand { get; }
        public ICommand ShowRevenueViewCommand { get; }
        public ICommand ShowUserDetailViewCommand { get; }
        public ICommand ShowCheckinCheckoutViewCommand { get; }
        public ICommand LogoutCommand { get; }

        public Action CloseMainViewAction { get; set; }
        public Action OpenLoginViewAction { get; set; }

        #endregion

        #region Constructors
        public MainViewViewModel()
        {
            AssignData();

            ShowWarehouseViewCommand = new ViewModelCommand(ExecuteShowWarehouseViewCommand);
            ShowProductViewCommand = new ViewModelCommand(ExecuteShowProductViewCommand);
            ShowRevenueViewCommand = new ViewModelCommand(ExecuteShowRevenueViewCommand);
            ShowUserDetailViewCommand = new ViewModelCommand(ExecuteShowUserDetailViewCommand);
            ShowCheckinCheckoutViewCommand = new ViewModelCommand(ExecuteShowCheckinCheckoutViewCommand);
            LogoutCommand = new ViewModelCommand(ExecuteLogoutCommand);

            ExecuteShowProductViewCommand(null);
            if(GlobalVarible.User != null)
            {
                Username = GlobalVarible.User.FullName;
            }
        }

        #endregion

        #region Methods
        private void ExecuteShowCheckinCheckoutViewCommand(object obj)
        {
            CurrentChildView = new CheckinCheckoutViewModel();
            Caption = "Check-in/Check-out";
            Icon = IconChar.Check;
        }
        private void ExecuteShowRevenueViewCommand(object obj)
        {
            CurrentChildView = new RevenueViewModel();
            Caption = "Invoice";
            Icon = IconChar.DollarSign;
        }

        private void ExecuteShowProductViewCommand(object obj)
        {
            CurrentChildView = new ProductViewModel();
            Caption = "Products";
            Icon = IconChar.CartShopping;
        }

        private void ExecuteShowWarehouseViewCommand(object obj)
        {
            CurrentChildView = new WarehouseViewModel();
            Caption = "Goods";
            Icon = IconChar.Warehouse;
        }

        private void ExecuteShowUserDetailViewCommand(object obj)
        {
            CurrentChildView = new UserDetailViewModel();
            Caption = "User Detail";
            Icon = IconChar.User;
        }

        private void ExecuteLogoutCommand(object obj)
        {
            // Invalidate tokens and user data
            GlobalVarible.Logout();

            // Close main view and open login view
            OpenLoginViewAction();
            CloseMainViewAction();
        }

        private void AssignData()
        {
            if (GlobalVarible.User != null)
            {
                Image = (GlobalVarible.User.Avatar == null) ? "/Images/pepe_the_frog.jpg" : GlobalVarible.User.Avatar;
            }
        }
        #endregion
    }
}
