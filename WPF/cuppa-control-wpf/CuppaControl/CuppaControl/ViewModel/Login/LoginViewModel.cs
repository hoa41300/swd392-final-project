﻿using CuppaControl.Global;
using CuppaControl.Model;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;

namespace CuppaControl.ViewModel
{
    class LoginViewModel : ViewModelBase
    {
        #region Fields
        private static readonly HttpClient client = new HttpClient();

        private string _username;
        private string _password;
        private string _errorMessage;
        private string _email;
        private Visibility _windowVisibility;
        private Visibility _loginFormVisibility;
        private Visibility _passwordRecoveryVisibility;

        #endregion

        #region Properties
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                OnPropertyChanged(nameof(Username));
            }
        }
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                OnPropertyChanged(nameof(Email));
            }
        }
        public Visibility WindowVisibility
        {
            get
            {
                return _windowVisibility;
            }
            set
            {
                _windowVisibility = value;
                OnPropertyChanged(nameof(WindowVisibility));
            }
        }
        public Visibility LoginFormVisibility
        {
            get
            {
                return _loginFormVisibility;
            }
            set
            {
                _loginFormVisibility = value;
                OnPropertyChanged(nameof(LoginFormVisibility));
            }
        }
        public Visibility PasswordRecoveryVisibility
        {
            get
            {
                return _passwordRecoveryVisibility;
            }
            set
            {
                _passwordRecoveryVisibility = value;
                OnPropertyChanged(nameof(PasswordRecoveryVisibility));
            }
        }

        #endregion

        #region Commands
        public ICommand LoginCommand { get; }
        public ICommand SendPassword { get; }
        public ICommand ShowLoginForm { get; }
        public ICommand ShowPasswordRecovery { get; }
        #endregion

        #region Constructors
        public LoginViewModel()
        {
            WindowVisibility = Visibility.Visible;
            LoginFormVisibility = Visibility.Visible;
            PasswordRecoveryVisibility = Visibility.Collapsed;

            LoginCommand = new ViewModelCommand(ExecuteLoginCommand, CanExecuteLoginCommand);
            SendPassword = new ViewModelCommand(ExecuteSendPassword, CanExecuteSendPassword);
            ShowLoginForm = new ViewModelCommand(ExecuteShowLoginForm);
            ShowPasswordRecovery = new ViewModelCommand(ExecuteShowPasswordRecovery);
        }

        #endregion

        #region Methods
        private bool CanExecuteLoginCommand(object obj)
        {
            bool validData;
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                validData = false;
            }
            else
            {
                validData = true;
            }

            return validData;
        }

        private async void ExecuteLoginCommand(object obj)
        {
            try
            {
                var response = GlobalVarible.BaseUrl
                    .AppendPathSegment("user")
                    .AppendPathSegment("login")
                    .WithHeader("Accept", "multipart/form-data")
                    .AllowAnyHttpStatus()
                    .PostMultipartAsync(mp => mp
                    .AddString("Email", Username)
                    .AddString("Password", Password))
                    .Result;
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    
                    GlobalVarible.Token = result["result"]["token"].ToString();

                    GlobalVarible.User = JsonConvert.DeserializeObject<User>(result["result"]["user"].ToString());

                    WindowVisibility = Visibility.Collapsed;
                }
                else if (response.StatusCode == 400 || response.StatusCode == 404)
                {
                    ErrorMessage = "Incorrect Username or Password";
                }

            }
            catch (FlurlHttpException ex)
            {
                if (ex.StatusCode == 400 || ex.StatusCode == 404)
                {
                    ErrorMessage = "Incorrect Username or Password";
                }
            }
        }

        private bool CanExecuteSendPassword(object obj)
        {
            if (string.IsNullOrEmpty(Email))
            {
                return false;
            }

            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(Email.Trim());

            return match.Success;
        }

        private void ExecuteSendPassword(object obj)
        {
            throw new NotImplementedException();
        }

        private void ExecuteShowPasswordRecovery(object obj)
        {
            ErrorMessage = string.Empty;

            LoginFormVisibility = Visibility.Collapsed;
            PasswordRecoveryVisibility = Visibility.Visible;
        }

        private void ExecuteShowLoginForm(object obj)
        {
            ErrorMessage = string.Empty;

            PasswordRecoveryVisibility = Visibility.Collapsed;
            LoginFormVisibility = Visibility.Visible;
        }

        #endregion

    }
}
