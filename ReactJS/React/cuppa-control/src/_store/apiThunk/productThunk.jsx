import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  getProductByFilter,
  createProduct,
  getProductById,
} from "_helpers/api/product";

//thunk API to get product list
export const getProductList = createAsyncThunk(
  "product/getProductList",
  async (
    { name, category, status, sortName, sortPrice, page, size },
    thunkAPI
  ) => {
    try {
      const response = await getProductByFilter(
        name,
        category,
        status,
        sortName,
        sortPrice,
        page,
        size
      );
      return response;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data);
    }
  }
);

// thunk API for create product
export const createProductThunk = createAsyncThunk(
  "product/createProduct",
  async ({ name, price, category, description }, thunkAPI) => {
    try {
      const response = await createProduct({
        name,
        price,
        category,
        description,
      });
      return response;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data);
    }
  }
);

// thunk API for get product by id
export const getProductByIdThunk = createAsyncThunk(
  "product/getById",
  async ({ id }, thunkAPI) => {
    try {
      const response = await getProductById(id);
      return response;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.response.data);
    }
  }
);
