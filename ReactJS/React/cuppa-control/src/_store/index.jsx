import { configureStore } from "@reduxjs/toolkit";
import authSlice from "./slices/auth.slice";
import userSlice from "./slices/user.slice";

//Store in redux is used to store state of application
export const store = configureStore({
  reducer: {
    auth: authSlice,
    user: userSlice,
  },
});
