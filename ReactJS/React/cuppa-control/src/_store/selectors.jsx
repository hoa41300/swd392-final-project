export const selectAuthUser = (state) => state.auth;

export const productListStore = (state) => state.productList.entity;
export const productStore = (state) => state.product.entity;
