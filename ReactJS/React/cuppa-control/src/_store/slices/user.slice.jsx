import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "users",
  initialState: {
    // user: [],
    user: [{ contents: [] }],
    loading: "idle",
    error: null,
  },
  reducers: {
    deactivateUser: (state, action) => {
      state.user.map((item) => {
        if (item.id === action.payload.id) {
          item.status = 0;
        }
      });
    },
  },
  extraReducers: {},
});

export default userSlice.reducer;
