import { createSlice } from "@reduxjs/toolkit";
import { getProductList } from "_store/apiThunk/productThunk";

const productListSlice = createSlice({
  name: "productList",
  initialState: {
    entity: [],
    loading: false,
    error: null,
  },
  //reducers is empty mean that we can not change directly
  //We can just change throw extra reducer, which depend on thunks
  reducers: {},
  extraReducers: (builder) => {
    builder
      //Get product list
      //state is from redux store
      .addCase(getProductList.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(getProductList.fulfilled, (state, action) => {
        state.loading = false;
        state.entity = action.payload;
      })
      .addCase(getProductList.rejected, (state, action) => {
        state.loading = false;
        state.error = action.payload;
      });
  },
});

export default productListSlice;
