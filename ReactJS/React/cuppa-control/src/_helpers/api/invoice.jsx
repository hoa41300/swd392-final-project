import api from "./baseAPI";

export const getInvoiceByFilter = async (pageNumber, pageSize, date) => {
  var dateQuery = "";
  if (date !== "") {
    dateQuery = `&date=${date}`;
  }

  const response = await api.get(
    `/api/invoice/filter?pageSize=${pageSize}&pageNumber=${pageNumber}${dateQuery}`
  );

  return response.data;
};

export const getInvoiceById = async (id) => {
  const response = await api.get(`/api/invoice/id?id=${id}`);
  return response.data;
};
