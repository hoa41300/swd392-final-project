import axios from "axios";
const URL =
  // "http://mangareaderproject-env.eba-simnmphm.us-east-1.elasticbeanstalk.com/";
  // "http://localhost:5000/";
  // "http://localhost:8080/";
  "http://localhost:5108/";

const instance = axios.create({
  baseURL: URL,
  headers: {
    // "Content-Type": "application/json",
    "Content-Type": "multipart/form-data",
  },
});

export const login = async (credential) => {
  const response = await instance.post("/api/user/login", credential);
  // const response = await instance.post("/api/auth/authenticate", credential);
  return response.data;
};


