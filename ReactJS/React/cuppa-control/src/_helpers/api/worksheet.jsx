import api from "./baseAPI";

export const getWorkSheetByDate = async (date) => {
  const response = await api.get(`/api/work-sheet?date=${date}`);
  return response.data;
};
