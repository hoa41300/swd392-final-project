import api from "./baseAPI";

export const createNewGood = async (data) => {
  const response = await api.post(`/api/inventory`, data);
  return response.data;
};

export const getGoodById = async (id) => {
  const response = await api.get(`/api/inventory/${id}`);
  return response.data;
};

export const getGoodByFilter = async (
  name,
  category,
  isInUse,
  sortName,
  sortQuantity,
  page,
  size
) => {
  let searchName = "";
  let searchCategory = `Category=${category}&`;
  let searchStatus = `isInUser=${isInUse}&`;
  let searchSortName = `sortName=${sortName}&`;
  let searchSortQuantity = `sortQuantity=${sortQuantity}&`;
  let searchPage = `pageNumber=${page}&`;
  let searchSize = `pageSize=${size}`;
  if (name?.trim() !== "") {
    searchName = `Name=${name}&`;
  }

  // if(typeof isInUse !== "undefined") {
  //   searchStatus = `isInUse=${isInUse}&`;
  // }

  // switch (isInUse) {
  //   case "inUse":
  //     searchStatus = `isInUse=${true}&`;
  //     break;
  //   case "notInUse":
  //     searchStatus = `isInUse=${false}&`;
  //     break;
  //   default:
  //     break;
  // }

  // const response = await api.get(
  //   `/api/inventory/filter?${searchName}${searchCategory}${searchStatus}${searchSortName}${searchSortQuantity}${searchPage}${searchSize}`
  // );
  const response = await api.get(
    `/api/inventory/filter?${searchName}${searchCategory}${searchStatus}${searchSortName}${searchSortQuantity}${searchPage}${searchSize}`
  );

  return response.data;
};

export const updateGoodInformation = async ({
  id,
  Name,
  CategoryId,
  isInUser,
}) => {
  const response = await api.put(
    `/api/inventory?id=${id}`,
    { Name, CategoryId, isInUser },
    {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    }
  );
  return response.data;
};

export const udpateGoodQuantity = async (id, quantity) => {
  const response = await api.patch(
    `/api/inventory/quantity?id=${id}&quantity=${quantity}`
  );
  return response.data;
};

export const getGoodCategories = async () => {
  const response = await api.get(`/api/inventory/category/list`);
  return response.data;
};
