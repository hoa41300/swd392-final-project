import api from "./baseAPI";

export const getProductByFilter = async (
  name,
  categoryId,
  sortName,
  status,
  sortPrice,
  page,
  size
) => {
  // let searchName = "";
  // let searchCategory = `CategoryId=${category}&`;
  // let searchStatus = `Status=${status}&`;
  // let searchSortName = `sortName=${sortName}&`;
  // let searchSortPrice = `sortPrice=${sortPrice}&`;
  // let searchPage = `page=${page}&`;
  // let searchSize = `size=${size}`;
  // if (name !== "") {
  //   searchName = `Name=${name}&`;
  // }

  // const response = await api.get(
  //   `/api/product/filter?${searchName}${searchCategory}${searchStatus}${searchSortName}${searchSortPrice}${searchPage}${searchSize}`
  // );

  var outOfStockQuery = "";

  if (status === "OutOfStock") {
    status = "";
    outOfStockQuery = "OutOfStock=true&";
  }

  const response = await api.get(
    `/api/product/filter?Name=${name}&CategoryId=${categoryId}&Sortname=${sortName}&Status=${status}&Sortprice=${sortPrice}&${outOfStockQuery}pageNumber=${page}&pageSize=${size}`
  );
  return response.data;
};

export const getProductById = async (id) => {
  const response = await api.get(`/api/product?id=${id}`);
  return response.data;
};

export const createProduct = async (data) => {
  const response = await api.post(`/api/product`, data);
  return response.data;
};

export const updateProduct = async (data) => {
  const response = await api.put(`/api/product?id=${data?.id}`, data);
  return response.data;
};

export const getProductCategories = async () => {
  const response = await api.get(`/api/product/category/list`);
  return response.data;
};

export const updateProductImage = async (file) => {
  const formData = new FormData();
  formData.append("image", file);

  const response = await api.patch("/api/product/image", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });

  return response.data;
};

export const updateProductStatus = async (id) => {
  const response = await api.patch(`/api/product?id=${id}`);
  return response.data;
};


