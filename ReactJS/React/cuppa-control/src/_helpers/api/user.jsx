import api from "./baseAPI";

export const getUserDetail = async (userName) => {
  const response = await api.get(`api/User/UserDetail?username=${userName}`);
  return response.data;
};

export const getUserList = async () => {
  const response = await api.get(`api/User/UserDetailList`);
  return response.data;
};

export const getCurrentAccount = async () => {
  const response = await api.get(`/api/user/account`);
  return response.data;
};

export const changePassword = async (data) => {
  const response = await api.patch(`/api/user/password?id=${data?.id}`, data);
  return response.data;
};

export const filterUserList = async (
  name,
  role,
  status,
  gender,
  sortCreatedDate,
  sortName,
  page,
  size
) => {
  let searchName = "";
  let searchRole = `role=${role}&`;
  let searchStatus = `status=${status}&`;
  let searchGender = `gender=${gender}&`;
  let searchCreatedDate = `sortCreateDate=${sortCreatedDate}&`;
  let searchSortName = `sortName=${sortName}&`;
  let searchPage = `pageNumber=${page}&`;
  let searchSize = `pageSize=${size}`;
  if (name !== "") {
    searchName = `name=${name}&`;
  }
  const response = await api.get(
    `/api/user/filter?${searchName}${searchRole}${searchStatus}${searchGender}${searchSortName}${searchCreatedDate}${searchPage}${searchSize}`
  );

  return response.data;
};

export const createUser = async (data) => {
  const response = await api.post(`/api/user`, data, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
  return response.data;
};

export const getUserById = async (id) => {
  const response = await api.get(`/api/user/userId?Id=${id}`);
  return response.data;
};

export const getUserByEmail = async (email) => {
  const response = await api.get(`/api/user/email?email=${email}`);
  return response.data;
};

export const changeUserStatus = async (id) => {
  const response = await api.patch(`/api/user/status?id=${id}`);
  return response.data;
};

export const updateUserInformation = async (data) => {
  const response = await api.put(`/api/user/update?id=${data.Id}`, data);
  return response.data;
};

export const asignUserRole = async (id) => {
  const response = await api.patch(`/api/user/role?id=${id}`);
  return response.data;
};
