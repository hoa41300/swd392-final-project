import api from "./baseAPI";

export const getRevenue = async (pageNumber, pageSize) => {
  const response = await api.get(
    `/api/invoice/revenue?pageNumber=${pageNumber}&pageSize=${pageSize}`
  );
  return response.data;
};

export const getRevenueDetail = async (date, pageNumber, pageSize) => {
  const response = await api.get(
    `/api/invoice/list?date=${date}&pageNumber=${pageNumber}&pageSize=${pageSize}`
  );
  return response.data;
};

export const getBillDetail = async (id) => {
  const response = await api.get(`/api/invoice/invoice-detail?id=${id}`);
  return response.data;
};
