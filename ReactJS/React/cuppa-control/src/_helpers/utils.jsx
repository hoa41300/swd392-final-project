//dd/mm/yyyy => mm-dd-yyyy
export const formatToSQLDate = (dateString) => {
  if (!dateString) {
    return null;
  }
  try {
    const [day, month, year] = dateString.split("/");

    return `${month}-${day}-${year}`;
  } catch (error) {
    return null;
  }
};

//mm-dd-yyyy => yyyy-mm-dd
export const formatStringToDate = (dateString) => {
  if (!dateString) {
    return null;
  }
  try {
    const [month, day, year] = dateString.split("-");

    return `${year}-${month}-${day}`;
  } catch (error) {
    return null;
  }
};

//yyyy-mm-dd => mm-dd-yyyy
export const formatDateToString = (date) => {
  if (!date) {
    return "";
  }
  try {
    const [year, month, day] = date.split("-");
    var dateString = month + "-" + day + "-" + year;

    return dateString;
  } catch (error) {
    return "";
  }
};

//yyyy-mm-dd => dd-mm-yyyy
export const formatDateToStringV2 = (date) => {
  if (!date) {
    return "";
  }
  try {
    const [year, month, day] = date.split("-");
    var dateString = day + "-" + month + "-" + year;

    return dateString;
  } catch (error) {
    return "";
  }
};

// dd-mm-yyyy => dd/mm
export const fortmatDateToStringV3 = (date) => {
  if (!date) {
    return "";
  }
  try {
    const [day, month, year] = date.split("-");
    var dateString = day + "/" + month;

    return dateString;
  } catch (error) {
    return "";
  }
};
