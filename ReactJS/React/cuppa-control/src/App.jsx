import { Routes, Route, Navigate, BrowserRouter } from "react-router-dom";
import { Login, ForgotPassword } from "pages/login/Login";
import { NotFound } from "pages/notFound/NotFound";
import { Menu } from "pages/menu/Menu";
import { ProductDetail } from "pages/menu/productDetail/ProductDetail";
import PrivateRoute from "_components/route/PrivateRoute";
import { Good } from "pages/good/Good";
import { Account } from "pages/account/Account";
import { User } from "pages/user/User";
import { UserDetail } from "pages/user/userDetail/UserDetail";
import { Revenue } from "pages/revenue/Revenue";
import { RevenueDetail } from "pages/revenue/revenueList/revenueItem/revenueDetail/RevenueDetail";
import Worksheet from "pages/worksheet/Worksheet";

export const App = () => {
  return (
    <Routes>
      <Route path="/">
        {/*public page*/}
        <Route index element={<Navigate to="/login" />} />
        <Route
          path="login"
          element={<PrivateRoute page="login" component={Login} />}
        />
        <Route
          path="forgot-password"
          element={
            <PrivateRoute page="forgotPassword" component={ForgotPassword} />
          }
        />

        {/*Authenticated page*/}
        <Route path="menu">
          <Route
            index
            element={<PrivateRoute page="menu" component={Menu} />}
          />
          <Route
            path="product-detail"
            element={
              <PrivateRoute page="productDetail" component={ProductDetail} />
            }
          />
        </Route>

        <Route path="good">
          <Route
            index
            element={<PrivateRoute page="good" component={Good} />}
          />
        </Route>

        <Route path="account">
          <Route
            index
            element={<PrivateRoute page="account" component={Account} />}
          />
        </Route>

        <Route path="user">
          <Route
            index
            element={<PrivateRoute page="user" component={User} />}
          />
          <Route
            path="user-detail"
            element={<PrivateRoute page="userDetail" component={UserDetail} />}
          />
        </Route>

        <Route path="revenue">
          <Route
            index
            element={<PrivateRoute page="revenue" component={Revenue} />}
          />

          <Route
            path="revenue-detail"
            element={
              <PrivateRoute page="revenueDetail" component={RevenueDetail} />
            }
          />
        </Route>

        <Route path="worksheet">
          <Route
            index
            element={<PrivateRoute page="worksheet" component={Worksheet} />}
          />
        </Route>

        {/*Not found page*/}
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
};
