import "./Selection.css";

export const Selection = ({
  options,
  selectedValue,
  handleSelectChange,
  label,
}) => {
  return (
    <div className="Selection_container d-flex justify-content-start align-items-center">
      <p className="Selection_label my-auto">{label}</p>

      <div className="Selection_select px-0">
        <select value={selectedValue} onChange={handleSelectChange}>
          {options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};
