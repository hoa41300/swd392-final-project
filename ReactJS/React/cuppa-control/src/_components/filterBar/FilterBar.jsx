import { Filter } from "_components/filter/Filter";
import "./FilterBar.css";
import filter from "assets/images/icon/Filter/Filter.png";
import searchIcon from "assets/images/icon/Search/Search.png";
import { useState } from "react";

export const FilterBar = ({ name, handleNameChange, filterList }) => {
  const [filterVisible, setFilterVisible] = useState(false);
  const handleFilterVisibleChange = (event) => {
    setFilterVisible(!filterVisible);
  };

  return (
    <div className="d-flex align-items-center justify-content-start">
      <div className="FilterBar_search-container">
        <input
          type="text"
          placeholder="Search"
          name="name"
          value={name || ""}
          onChange={handleNameChange}
          className="FilterBar_search-box px-3"
        />
        <button
          className="FilterBar_search-button"
          // onClick={handleNameChange}
        >
          <img src={searchIcon} alt="Search Icon" />
        </button>
      </div>
      <div className="FilterBar_filter_container d-inline-block mx-5">
        <button
          className="d-flex justify-content-between align-items-center FilterBar_filter-button px-2"
          onClick={handleFilterVisibleChange}
        >
          <img src={filter} alt="Filter icon" />
          <span>Filter</span>
        </button>
        {filterVisible && <Filter filterList={filterList} />}
      </div>
    </div>
  );
};
