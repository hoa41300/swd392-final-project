const { Header } = require("pages/header/Header");
const { Outlet } = require("react-router-dom");

const HeaderRoute = () => {
  const user = JSON.parse(localStorage.getItem("user"));
  if (user == null) {
    return <Outlet />;
  } else {
    return <Header />;
  }
};

export { HeaderRoute };
