// import { useSelector } from "react-redux";
import { useLocation, Outlet, Navigate, useNavigate } from "react-router-dom";
// import { selectAuthUser } from "_store/selectors";

const PrivateRoute = ({ page, component: Component }) => {
  const navigate = useNavigate();

  // console.log(`This is PrivateRoute with page: ${page}`);
  const location = useLocation();
  // const { role, isAuthenticated } = useSelector(selectAuthUser);
  const user = JSON.parse(localStorage.getItem("user"));

  if (page == null) {
    return <Navigate to="/error404" state={{ from: location }} replace />;
  } else {
    if (user == null) {
      if (page !== "forgotPassword" && page !== "login") {
        return <Navigate to="/login" state={{ from: location }} replace />;
      } else {
        //log
        // console.log("This is where return component in guest login");
        return <Component />;
      }
    } else {
      if (page === "forgotPassword" || page === "login") {
        return <Navigate to="/menu" state={{ from: location }} replace />;
      } else {
        //Authorize here
        //log
        // console.log("This is where return component in user login");
        if (user?.role === "Admin" || user?.role === "ShopOwner") {
          return <Component />;
        } else {
          if (
            page === "menu" ||
            page === "productDetail" ||
            page === "good" ||
            page === "user" ||
            page === "userDetail" ||
            page === "revenue" ||
            page === "revenueDetail"
          ) {
            return <Navigate to="/account" state={{ from: location }} replace />;
          } else {
            return <Component />;
          }
        }
      }
    }
  }
};

export default PrivateRoute;
