import { useContext } from "react";
import "./Filter.css";
import SelectionBox from "_components/selectionBox/SelectionBox";

export const Filter = ({ filterList }) => {
  return (
    <div className="Filter_container p-2">
      {filterList.map((filter) => (
        <SelectionBox
          options={filter.options}
          selectedValue={filter.selectedValue}
          handleSelectChange={filter.handleSelectChange}
        />
      ))}
    </div>
  );
};
