import "./PaginationNav.css";

export const PaginationNav = ({ page, totalPage, handlePageClick }) => {
  const pageItems = [];
  for (let index = 1; index <= totalPage; index++) {
    pageItems.push(
      <li
        key={index}
        className={
          index === page
            ? "page-item active PaginationNav-link"
            : "page-item PaginationNav-link"
        }
      >
        <a className="page-link" onClick={() => handlePageClick(index)}>
          {index}
        </a>
      </li>
    );
  }

  return (
    <div className="d-flex justify-content-center align-items-center mt-5 mb-0">
      <nav aria-label="Page navigation example">
        <ul className="pagination">
          <li className="page-item PaginationNav-link">
            <a
              className="page-link"
              aria-label="Previous"
              onClick={() => handlePageClick(page > 1 ? page - 1 : 1)}
            >
              <span aria-hidden="true">&laquo;</span>
            </a>
          </li>

          {pageItems}
          <li className="page-item PaginationNav-link">
            <a
              className="page-link"
              aria-label="Next"
              onClick={() =>
                handlePageClick(page < totalPage ? page + 1 : totalPage)
              }
            >
              <span aria-hidden="true">&raquo;</span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
};
