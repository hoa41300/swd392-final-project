import "./SelectionBox.css";

const SelectionBox = ({ options, selectedValue, handleSelectChange }) => {
  return (
    <div className="SelectionBox_container d-flex align-items-center">
      <select value={selectedValue} onChange={handleSelectChange}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SelectionBox;
