import "./SelectFilter.css";

export const SelectFilter = ({
  options,
  selectedValue,
  handleSelectChange,
  label,
  selectedName,
}) => {
  return (
    <div className="SelectFilter_container d-flex justify-content-start align-items-center">
      <p className="SelectFilter_label my-auto">{label}</p>

      <div className="SelectFilter_select px-0">
        <select
          name={selectedName || ""}
          value={selectedValue}
          onChange={handleSelectChange}
        >
          {options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};
