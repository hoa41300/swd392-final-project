import { useEffect, useRef, useState } from "react";
import "./CreateProduct.css";
import { createProduct, getProductCategories } from "_helpers/api/product";
import { useNavigate } from "react-router-dom";

export const CreateProduct = () => {
  const closeButtonRef = useRef(null);

  const navigate = useNavigate();

  const [input, setInput] = useState({
    name: "",
    price: 1,
    category: 1,
    description: "",
    productImg: "",
    status: "Inactive",
  });
  const [productCategory, setProductCategory] = useState([]);
  //options for category here
  const [categoryOptions, setCategoryOptions] = useState([]);

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInput((values) => ({ ...values, [name]: value }));
  };

  //fetch product category
  useEffect(() => {
    const fetchProductCategory = async () => {
      try {
        const data = await getProductCategories();
        setProductCategory(data.result);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setProductCategory([]);
          } else {
            alert(`Category \n Status: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchProductCategory();
  }, []);

  useEffect(() => {
    setCategoryOptions([
      ...productCategory.map((element) => {
        return { value: element.id, label: element.name };
      }),
    ]);
  }, [productCategory]);

  const handleFormSubmit = (event) => {
    //prevent default action of form
    event.preventDefault();
    const createNewProduct = async () => {
      try {

        //Thia code is call when have API
        const data = await createProduct({
          name: input.name,
          price: input.price,
          categoryId: input.category,
          description: input.description,
          productImg: input.productImg,
          status: input.status,
        });

        var id = data?.result;
        closeButtonRef.current.click();

        navigate(`product-detail?id=${id}`);
        // window.location.reload();
      } catch (error) {
        if (error.response) {
          const { status, message } = error.response.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    createNewProduct();
  };

  return (
    // <!-- Modal -->
    <div
      className="modal fade"
      id="create-product"
      tabIndex="-1"
      aria-labelledby="enrollLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="enrollLabel">
              Add new Product
            </h1>
            <button
              ref={closeButtonRef}
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleFormSubmit} id="createProduct">
              <div className="mb-3">
                <label htmlFor="name" className="col-form-label">
                  Name:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Product name"
                  id="name"
                  name="name"
                  required={true}
                  value={input?.name || ""}
                  onChange={handleInputChange}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="price" className="col-form-label">
                  Price:
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="price"
                  name="price"
                  required={true}
                  value={input?.price || 1}
                  step="0.1"
                  onChange={handleInputChange}
                  min="0"
                />
              </div>

              <div className="mb-3">
                <label htmlFor="status" className="col-form-label">
                  Status:
                </label>
                <select
                  id="status"
                  name="status"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.status || "Inactive"}
                >
                  <option value="Active">Active</option>
                  <option value="Inactive">Inactive</option>
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="category" className="col-form-label">
                  Category:
                </label>
                <select
                  id="category"
                  name="category"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.category || 1}
                >
                  {/* BUG HERE */}
                  {categoryOptions.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="productImg" className="col-form-label">
                  Product image url:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Product image url"
                  id="productImg"
                  name="productImg"
                  required={true}
                  value={input?.productImg || ""}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="description" className="col-form-label">
                  Description:
                </label>
                <textarea
                  id="description"
                  name="description"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.description || ""}
                />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              form="createProduct"
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
