import { useSelector } from "react-redux";
import "./ProductList.css";
import { ProductItem } from "./productItem/ProductItem";
import { productList } from "_store/selectors";

//need to remove productList parameter
export const ProductList = ({ products }) => {
  // const products = useSelector(productList);

  return (
    <div className="container mt-5">
      <div className="row">
        {products?.length > 0 &&
          products.map((item) => (
            <ProductItem
              key={item.id}
              id={item.id}
              name={item.name}
              price={item.price}
              imgUrl={item.productImg}
              status={item.status}
            />
          ))}
      </div>
    </div>
  );
};
