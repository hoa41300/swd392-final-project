import { useNavigate } from "react-router-dom";
import "./ProductItem.css";

export const ProductItem = ({ id, name, imgUrl, price, status }) => {
  const navigate = useNavigate();
  const handleImgOnClick = () => {
    navigate(`product-detail?id=${id}`);
  };

  return (
    <div className="col-md-3 col-sm-6 g-5">
      <div className="card ProductItem_card ProductItem_border">
        <img
          src={imgUrl}
          alt={name}
          className="card-img-top ProductItem_img"
          onClick={() => handleImgOnClick()}
        />
        <div className="card-body ProductItem_body">
          <div>{name}</div>
          <div className="py-2">{price} $</div>
        </div>
      </div>
    </div>
  );
};
