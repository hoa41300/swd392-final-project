import { createContext, useState, useEffect, useRef } from "react";
import "./MenuFilter.css";
import searchIcon from "assets/images/icon/Search/Search.png";
import plus from "assets/images/icon/Plus/Plus.png";
import { getProductByFilter, getProductCategories } from "_helpers/api/product";
import { SelectFilter } from "./selectFilter/SelectFilter";
import { ProductList } from "./productList/ProductList";
import { useDispatch, useSelector } from "react-redux";
import { getProductList } from "_store/apiThunk/productThunk";
import { productListStore } from "_store/selectors";
import { PaginationNav } from "_components/paginationNav/PaginationNav";
import { CreateProduct } from "./creatProduct/CreateProduct";
import { FilterBar } from "_components/filterBar/FilterBar";

const FilterContext = createContext();

const MenuFilter = () => {
  // const products = useSelector(productList);
  const [searchQuery, setSearchQuery] = useState("");
  const [categoryId, setCategoryId] = useState("");
  const [status, setStatus] = useState("");
  const [sortName, setSortName] = useState("asc");
  const [sortPrice, setSortPrice] = useState("");
  const [page, setPage] = useState(1);
  const [productCategory, setProductCategory] = useState([]);
  const [productList, setProductList] = useState([]);
  const [totalPage, setTotalPage] = useState(1);
  const size = 12;

  //options for category here
  const [categoryOptions, setCategoryOptions] = useState([]);

  /*Fake data here*/
  const productSample = {
    id: "1",
    name: "Cà phê sữa",
    imgUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSpdOTkQIGXDUjcSh1LwetQhRffxE-D2x9Kwc55wTQNSDAMkEolH1mnZ57rTiCIbcyP04&usqp=CAU",
    price: "30000",
    category: "Drink",
    status: "Active",
  };

  const productListFake = [
    productSample,
    { ...productSample, id: "2" },
    { ...productSample, id: "3" },
    { ...productSample, id: "4" },
    { ...productSample, id: "5" },
    { ...productSample, id: "6" },
    { ...productSample, id: "7" },
    { ...productSample, id: "8" },
    { ...productSample, id: "9" },
    { ...productSample, id: "10" },
    { ...productSample, id: "11" },
    { ...productSample, id: "12" },
  ];

  /*Fake data here*/

  //Filter object used in useContext
  const FilterContextObject = {
    sortName,
    sortPrice,
    setSortPrice,
    setSortName,
  };

  //options for status here
  const productStatus = [
    { value: "", label: "All" },
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
    { value: "OutOfStock", label: "Out Of Stock" },
  ];

  const handleCategoryChange = (e) => {
    setCategoryId(e.target.value);
    setPage(1);
  };

  const handleStatusChange = (e) => {
    setStatus(e.target.value);
    setPage(1);
  };

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
    setPage(1);
  };

  const handleSortNameChange = (e) => {
    setSortName(e.target.value);
    setSortPrice("");
  };

  const handleSortPriceChange = (e) => {
    setSortPrice(e.target.value);
    setSortName("");
  };

  const handlePageClick = (value) => {
    setPage(value);
  };

  //handle Filter onClick
  const [isFilterVisible, setIsFilterVisible] = useState(false);
  const filterRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (filterRef.current && !filterRef.current.contains(event.target)) {
        setIsFilterVisible(false);
      }
    };

    document.addEventListener("click", handleClickOutside);

    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);

  const handleButtonClick = () => {
    setIsFilterVisible(!isFilterVisible);
  };
  //handle Filter onClick

  // Fetch Data
  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const data = await getProductByFilter(
          searchQuery,
          categoryId,
          sortName,
          status,
          sortPrice,
          page,
          size
        );

        setTotalPage(data?.result?.totalPage || 1);
        setProductList(data?.result?.list);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setProductList([]);
            setTotalPage(1);
          } else {
            alert(`Product \nStatus: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    // setProductList(productListFake);
    fetchProduct();
  }, [searchQuery, categoryId, status, sortName, sortPrice, page, size]);

  //fetch product category
  useEffect(() => {
    const fetchProductCategory = async () => {
      try {
        const data = await getProductCategories();
        setProductCategory(data.result);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setProductCategory([]);
          } else {
            alert(`Category \n Status: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchProductCategory();
  }, []);

  useEffect(() => {
    setCategoryOptions([
      { value: "", label: "All" },
      ...productCategory.map((element) => {
        return { value: element.id, label: element.name };
      }),
    ]);
  }, [productCategory]);

  //Param for FilterBar
  const filterList = [
    {
      options: [
        { value: "asc", label: "Name asc" },
        { value: "desc", label: "Name desc" },
      ],
      selectedValue: sortName,
      handleSelectChange: handleSortNameChange,
    },
    {
      options: [
        { value: "asc", label: "Price asc" },
        { value: "desc", label: "Price desc" },
      ],
      selectedValue: sortPrice,
      handleSelectChange: handleSortPriceChange,
    },
  ];

  return (
    <FilterContext.Provider value={FilterContextObject}>
      <div className="MenuFilter_container p-5 my-5">
        <h3>Menu</h3>
        <div className="mt-4 container">
          <div className="d-flex justify-content-between align-items-center">
            <div className="d-flex align-items-center justify-content-start">
              <FilterBar
                name={searchQuery}
                handleNameChange={handleSearchChange}
                filterList={filterList}
              />
            </div>

            {/* Create Button  */}
            <button
              className="btn btn-primary btn-md"
              data-bs-toggle="modal"
              data-bs-target="#create-product"
            >
              <div className="d-flex justify-content-between align-items-center">
                <p className="my-0 mx-1">Add</p>
                <img src={plus} alt="Plus icon" className="d-block mx-1" />
              </div>
            </button>
            {/* Create Button  */}
          </div>

          <div className="d-flex align-items-center justify-content-start mt-4">
            <SelectFilter
              options={categoryOptions}
              label={"Category"}
              selectedValue={categoryId}
              handleSelectChange={handleCategoryChange}
            />
            <SelectFilter
              options={productStatus}
              label={"Status"}
              selectedValue={status}
              handleSelectChange={handleStatusChange}
            />
          </div>
        </div>
        {/* Need to remove product list parameter, just use useSelector to get local storage */}
        <ProductList products={productList} />
        <PaginationNav
          page={page}
          totalPage={totalPage}
          handlePageClick={handlePageClick}
        />
      </div>

      <CreateProduct />
    </FilterContext.Provider>
  );
};

export { FilterContext, MenuFilter };
