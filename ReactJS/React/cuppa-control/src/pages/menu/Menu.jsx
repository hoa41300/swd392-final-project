import { Header } from "pages/header/Header";
import "./Menu.css";
import { Footer } from "pages/footer/footer";
import { MenuFilter } from "./menuFilter/MenuFilter";

export const Menu = () => {
  document.body.style = "background: #efefef";
  // console.log("This is menu page");
  return (
    <>
      <Header />
      <div className="Menu_container">
        <MenuFilter />
      </div>
      <Footer />
    </>
  );
};
