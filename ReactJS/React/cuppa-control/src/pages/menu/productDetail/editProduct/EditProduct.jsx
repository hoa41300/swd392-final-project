import { useNavigate } from "react-router-dom";
import "./EditProduct.css";
import { useEffect, useRef, useState } from "react";
import { getProductCategories, updateProduct } from "_helpers/api/product";
import { SelectFilter } from "pages/menu/menuFilter/selectFilter/SelectFilter";

export const EditProduct = ({ product, setProduct }) => {
  const closeButtonRef = useRef(null);

  const navigate = useNavigate();
  const [alertMessage, setAlertMessage] = useState(null);

  const [input, setInput] = useState({});

  const [productCategory, setProductCategory] = useState([]);
  //options for category here
  const [categoryOptions, setCategoryOptions] = useState([]);

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInput((values) => ({ ...values, [name]: value }));
  };

  const handleFormSubmit = (event) => {
    //prevent default action of form
    event.preventDefault();
    try {
      //Thia code is call when have API
      const result = updateProduct({
        id: product.id,
        name: input.name,
        price: input.price,
        categoryId: input.category,
        description: input.description,
        productImg: input.productImg,
      });

      console.log(`Input update date: CategoryId: ${input.category}\nName: ${input.name}\nPrice: ${input.price}\ndescription: ${input.description}\nProductImage: ${input.productImg}`);

      // setProduct(result);
      closeButtonRef.current.click();

      // navigate(`/menu/product-detail?id=${product.id}`);
      window.location.reload();
    } catch (error) {
      if (error?.response) {
        const { status, message } = error?.response?.data;
        alert(`Status: ${status}\nMessage: ${message}`);
      } else {
        setAlertMessage("Oops... Something went wrong.");
        alert(`Message: ${alertMessage}`);
      }
    }
  };

  useEffect(() => {
    setInput({
      name: product?.name || "",
      price: product?.price || 1,
      category: 1,
      description: product?.description || "",
      productImg: product?.productImg || "",
    });
  }, [product]);

  //fetch product category
  useEffect(() => {
    const fetchProductCategory = async () => {
      try {
        const data = await getProductCategories();
        setProductCategory(data.result);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setProductCategory([]);
          } else {
            alert(`Category \n Status: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchProductCategory();
  }, []);

  useEffect(() => {
    setCategoryOptions([
      ...productCategory.map((element) => {
        return { value: element.id, label: element.name };
      }),
    ]);
  }, [productCategory]);

  return (
    // <!-- Modal -->
    <div
      className="modal fade"
      id="edit-product"
      tabIndex="-1"
      aria-labelledby="editProductLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="editProductLabel">
               Update Product
            </h1>
            <button
              ref={closeButtonRef}
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form
              onSubmit={handleFormSubmit}
              id="edit-product-form"
              method="post"
            >
              <input
                type="hidden"
                className="form-control"
                name="id"
                value={product?.id || 1}
                disabled
              />
              <div className="mb-3">
                <label htmlFor="name" className="col-form-label">
                  Name:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Product name"
                  id="name"
                  name="name"
                  required={true}
                  value={input?.name || ""}
                  onChange={handleInputChange}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="price" className="col-form-label">
                  Price:
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="price"
                  name="price"
                  required={true}
                  value={input?.price || 1000}
                  onChange={handleInputChange}
                />
              </div>
              {/* <div className="mb-3">
                <label htmlFor="email" className="col-form-label">
                  Category:
                </label>
                <select
                  id="category"
                  name="category"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.category || "Food"}
                >
                  <option value="Food">Food</option>
                  <option value="Drink">Drink</option>
                </select>
              </div> */}

              <div className="mb-3">
                <label htmlFor="category" className="col-form-label">
                  Category:
                </label>
                <select
                  id="category"
                  name="category"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.category || "Fruit"}
                >
                  {/* BUG HERE */}
                  {categoryOptions.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="productImg" className="col-form-label">
                  Product image url:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Product name"
                  id="productImg"
                  name="productImg"
                  required={true}
                  value={input?.productImg || ""}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="description" className="col-form-label">
                  Description:
                </label>
                <textarea
                  id="description"
                  name="description"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.description || ""}
                />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              form="edit-product-form"
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};


