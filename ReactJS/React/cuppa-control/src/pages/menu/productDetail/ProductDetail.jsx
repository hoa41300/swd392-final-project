import { Header } from "pages/header/Header";
import "./ProductDetail.css";
import { Footer } from "pages/footer/footer";
import ArrowLeftIcon from "assets/images/icon/Arrow_Left/Arrow_Left.png";
import ActiveIcon from "assets/images/icon/Active/Active.png";
import { useLocation, useNavigate } from "react-router-dom";
import { getProductById, updateProductStatus } from "_helpers/api/product";
import { useEffect, useState } from "react";
import { EditProduct } from "./editProduct/EditProduct";
import InactiveIcon from "assets/images/icon/Inactive/Inactive.png";

export const ProductDetail = () => {
  document.body.style = "background: #efefef";

  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const id = searchParams.get("id");

  const navigate = useNavigate();

  //create product variable
  const [product, setProduct] = useState({});

  const handleChangeStatus = () => {
    const changeStatus = async () => {
      try {
        const response = await updateProductStatus(id);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    changeStatus();
    //relaod the page
    window.location.reload();
  };

  //fake data
  const productSample = {
    id: "1",
    name: "Cà phê sữa",
    imgUrl:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSpdOTkQIGXDUjcSh1LwetQhRffxE-D2x9Kwc55wTQNSDAMkEolH1mnZ57rTiCIbcyP04&usqp=CAU",
    price: "30000",
    category: "Drink",
    status: "Active",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit.Pariatur cumque ducimus reiciendis vel porro, temporibus veniam dolores ipsa voluptatem reprehenderit recusandae modiad magnam aliquam sunt asperiores, rerum, corporis soluta?",
  };
  //fetch data
  useEffect(() => {
    //fetch fucntion here
    const fetchData = async () => {
      try {
        const data = await getProductById(id);
        setProduct(data);
        // setProduct(productSample);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchData();
  }, []);

  // const handleBackOnClick = () => {
  //   navigate("/menu");
  // };

  return (
    <>
      <Header />

      <div className="ProductDetail_container">
        <div className="ProductDetail_content p-5 my-5">
          <h3>Product detail</h3>
          <div className="mt-4 container">
            <div className="row g-5">
              <div className="col-md-4 col-xs-12 d-block px-3">
                <img
                  src={product?.productImg}
                  alt="product"
                  className="ProductDetail_img"
                />
                {/* <form
                  className="text-center mt-3"
                  method="post"
                  encType="multipart/form-data"
                >
                  <div className="my-2">
                    <input
                      type="file"
                      accept="image/*"
                      style={{ display: "none" }}
                      id="imageInput"
                    />
                    <label
                      htmlFor="imageInput"
                      className="btn btn-secondary ProductDetail_button-content px-2 py-1"
                    >
                      Choose file
                    </label>
                  </div>
                  <button
                    type="submit"
                    className="btn btn-primary ProductDetail_button-content px-2 py-1"
                    disabled
                  >
                    Upload Image
                  </button>
                </form> */}
              </div>
              <div className="col-md-8 col-xs-12">
                <div className="d-flex justify-content-between align-items-center">
                  <h4 className="my-0">{product?.name}</h4>
                  {/* <button className="btn btn-success ProductDetail_button px-2">
                    <div className="d-flex justify-content-between align-items-center w-100 ProductDetail_button-content">
                      <img
                        src={product?.status === "Active" ? Active : Inactive}
                        alt="Active icon"
                        className="ProductDetail_icon"
                        style={{ height: "12px" }}
                      />
                      <p className="my-0">{product?.status}</p>
                    </div>
                  </button> */}

                  <button
                    className={`btn UserDetail_button 
              ${
                product?.status == "Active"
                  ? "btn-success UserDetail_button-active"
                  : "btn-danger UserDetail_button-inactive"
              }`}
                    onClick={() => handleChangeStatus()}
                  >
                    <div className="d-flex justify-content-between align-items-center w-100 UserDetail_button-content">
                      <img
                        src={
                          product?.status === "Active"
                            ? ActiveIcon
                            : InactiveIcon
                        }
                        alt="Status icon"
                        className="UserDetail_icon"
                        style={{ height: "10px" }}
                      />
                      <p className="my-0">{product?.status}</p>
                    </div>
                  </button>
                </div>
                <hr />
                <div className="">
                  <h5>Description: </h5>
                  <p>{product?.description}</p>
                  <h5>
                    Price:{" "}
                    <span style={{ fontSize: "18px" }}>
                      {product?.price} $
                    </span>
                  </h5>
                  <h5>
                    Category:{" "}
                    <span style={{ fontSize: "18px" }}>
                      {product?.categoryName}
                    </span>
                  </h5>
                  {/* Create Button  */}
                  <button
                    className="btn btn-primary btn-md"
                    data-bs-toggle="modal"
                    data-bs-target="#edit-product"
                  >
                    Update
                  </button>
                  {/* Create Button  */}
                </div>
              </div>
            </div>

            {/* <button
              className="btn btn-primary mt-5 ProductDetail_button px-2"
              onClick={() => handleBackOnClick()}
            >
              <div className="d-flex justify-content-between align-items-center w-100 ProductDetail_button-content">
                <img
                  src={ArrowLeftIcon}
                  alt="Arrow left icon"
                  className="ProductDetail_icon"
                />
                <p className="my-0">Back</p>
              </div>
            </button> */}
          </div>
        </div>
      </div>
      <EditProduct product={product} setProduct={setProduct} />

      <Footer />
    </>
  );
};
