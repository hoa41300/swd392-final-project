import { Footer } from "pages/footer/footer";
import { Header } from "pages/header/Header";
import "./Worksheet.css";
import { useEffect, useState } from "react";
import { getWorkSheetByDate } from "_helpers/api/worksheet";
import { formatDateToStringV2, fortmatDateToStringV3 } from "_helpers/utils";
import { ShiftComponent } from "./shiftComponent/ShiftComponent";

const Worksheet = () => {
  document.body.style = "background: #efefef";

  const today = new Date();
  const year = today.getFullYear();
  const month = (today.getMonth() + 1).toString().padStart(2, "0");
  const day = today.getDate().toString().padStart(2, "0");
  const formattedDate = `${year}-${month}-${day}`;
  const [worksheets, setWorkSheets] = useState([]);
  // Initialize the date state to today's date
  const [date, setDate] = useState(formattedDate);

  const handleDateChange = (event) => {
    const dateForm = event.target.value;
    setDate(dateForm);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        //log
        console.log(`data: ${JSON.stringify(worksheets, null, 2)}`);

        const data = await getWorkSheetByDate(formatDateToStringV2(date));
        setWorkSheets(data?.result);

        //log
        // console.log(`data: ${JSON.stringify(data, null, 2)}`);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("OOps... Something went wrong");
        }
      }
    };

    fetchData();
  }, [date]);

  return (
    <>
      <Header />
      <div className=" Worksheet_container">
        <div className="Worksheet_container-content p-0 mt-4 WorkSheet_table">
          <table className="table ">
            <thead>
              <tr style={{ backgroundColor: "#4c6ef8" }}>
                <th style={{ width: "23%" }} rowSpan={2}>
                  <label htmlFor="date" className="text-center">
                    Date
                  </label>
                  <br />
                  <input
                    type="date"
                    value={date || ""}
                    onChange={handleDateChange}
                    className="form-control mx-0 mt-2"
                  />
                </th>
                <th style={{ width: "11%" }}>MON</th>
                <th style={{ width: "11%" }}>TUE</th>
                <th style={{ width: "11%" }}>WED</th>
                <th style={{ width: "11%" }}>THU</th>
                <th style={{ width: "11%" }}>FRI</th>
                <th style={{ width: "11%" }}>SAT</th>
                <th style={{ width: "11%" }}>SUN</th>
              </tr>
              <tr style={{ backgroundColor: "#4c6ef8" }}>
                <td>
                  {fortmatDateToStringV3(`${worksheets?.[0]?.date || ""}`)}
                </td>
                <td>
                  {fortmatDateToStringV3(`${worksheets?.[3]?.date || ""}`)}
                </td>
                <td>
                  {fortmatDateToStringV3(`${worksheets?.[6]?.date || ""}`)}
                </td>
                <td>
                  {fortmatDateToStringV3(`${worksheets?.[9]?.date || ""}`)}
                </td>
                <td>
                  {fortmatDateToStringV3(`${worksheets?.[12]?.date || ""}`)}
                </td>
                <td>
                  {fortmatDateToStringV3(`${worksheets?.[15]?.date || ""}`)}
                </td>
                <td>
                  {fortmatDateToStringV3(`${worksheets?.[18]?.date || ""}`)}
                </td>
              </tr>
            </thead>
            <tbody>
              <tr style={{ height: "120px", color: "black", fontSize: "14px" }}>
                <td style={{ width: "23%" }}>
                  <p>Shift 1</p>
                  <p>(6h:00 - 12h:00)</p>
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[0]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[3]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[6]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[9]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[12]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[15]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[18]?.listUser} />
                </td>
              </tr>
              <tr style={{ height: "120px", color: "black", fontSize: "14px" }}>
                <td style={{ width: "23%" }}>
                  <p>Shift 2</p>
                  <p>(12h:00 - 18h:00)</p>
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[1]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[4]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[7]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[10]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[13]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[16]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[19]?.listUser} />
                </td>
              </tr>
              <tr style={{ height: "120px", color: "black", fontSize: "14px" }}>
                <td style={{ width: "23%" }}>
                  <p>Shift 3</p>
                  <p>(18h:00 - 22h:00)</p>
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[2]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[5]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[8]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[11]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[14]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[17]?.listUser} />
                </td>
                <td style={{ width: "11%" }}>
                  <ShiftComponent users={worksheets?.[20]?.listUser} />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Worksheet;
