import "./ShiftComponent.css";

export const ShiftComponent = ({ users }) => {
  const currentUser = JSON.parse(localStorage.getItem("user"));
  return (
    <>
      {users == undefined || users.length == 0 ? (
        <p>-----</p>
      ) : (
        users?.map((user) => (
          <p className={`${user?.userId === currentUser.id ? "fw-bold" : ""}`}>
            {user?.userName}
          </p>
        ))
      )}
    </>
  );
};
