import { Header } from "pages/header/Header";
import "./Account.css";
import { Footer } from "pages/footer/footer";
import defaultAvatar from "assets/images/avatar/default_avatar.jpg";
import { useEffect, useState } from "react";
import {
  changePassword,
  getCurrentAccount,
  getUserById,
} from "_helpers/api/user";

export const Account = () => {
  document.body.style = "background: #efefef";
  const [currentUser, setCurrentUser] = useState({});

  const [isChangePassword, setIsChangePassword] = useState(false);
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [changePassMessage, setChangePassMessage] = useState({
    color: "",
    message: "",
  });
  const [isMessageVisible, setIsMessageVisible] = useState(false);

  const handleIsChangePasswordChange = () => {
    setIsChangePassword(!isChangePassword);
  };

  const handleOldPasswordChange = (event) => {
    setOldPassword(event.target.value);
  };

  const handleNewPasswordChange = (event) => {
    setNewPassword(event.target.value);
  };

  // const handleChangePassMessageChange = (e) => {

  // }

  const handleChangePasswordFormSubmit = (event) => {
    //prevent default action of form
    event.preventDefault();
    // alert(`Form submitted`);

    const user = JSON.parse(localStorage.getItem("user"));

    //Call change password form here
    const changePasswordFunction = async () => {
      try {
        console.log(`User id: ${user.id}`);
        console.log(`Old password: ${oldPassword}`);
        console.log(`New password: ${newPassword}`);

        const data = await changePassword({
          id: user.id,
          OldPassword: oldPassword,
          NewPassword: newPassword,
          ConfirmPassword: newPassword,
        });

        //log
        console.log(data.message);
        //Check status range here
        setChangePassMessage({ color: "green", message: data.message });
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          // if (status === "400") {
          //   setChangePassMessage({ color: "red", message: message });
          // } else {
          //   alert(`Oops... Something went wrong.`);
          // }
          setChangePassMessage({ color: "red", message: message });
        } else {
          alert(`Oops... Something went wrong.`);
        }
      }
    };
    changePasswordFunction();
  };

  //fetch data here
  useEffect(() => {
    const fetchData = async () => {
      const user = await JSON.parse(localStorage.getItem("user"));
      const data = await getUserById(user.id);

      console.log(data);
      setCurrentUser(data.result);
    };
    fetchData();
  }, []);

  return (
    <>
      <Header />
      <div className="Account_container p-0">
        <div className="Account_container-bar mt-1"></div>
        <div className="Account_container-content ">
          <div className="Account_avatar">
            <div className="Account_avatar-image">
              <img
                src={currentUser?.avatar || defaultAvatar}
                alt="Avatar image"
              />
            </div>
            <p className="text-center">Kim Da Mi</p>
          </div>
          <div className="row">
            <div className="col-md-4 pr-3 Account_change-password">
              <button
                className="btn btn-primary  -button"
                onClick={handleIsChangePasswordChange}
              >
                Change Password
              </button>
              {isChangePassword && (
                <div>
                  <form
                    className="mt-3 px-2"
                    id="change-password-form"
                    onSubmit={handleChangePasswordFormSubmit}
                  >
                    <div className="mb-2">
                      <label for="oldPassword" className="form-label">
                        Old Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="oldPassword"
                        required
                        onChange={handleOldPasswordChange}
                      />
                    </div>
                    <div className="mb-2">
                      <label for="oldPassword" className="form-label">
                        New Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="oldPassword"
                        required
                        onChange={handleNewPasswordChange}
                      />
                    </div>
                  </form>

                  {changePassMessage?.color && (
                    <p style={{ color: changePassMessage.color }}>
                      {changePassMessage.message}
                    </p>
                  )}

                  <div className="d-flex justify-content-start align-items-center mt-3">
                    <button
                      className="btn btn-success mr-3 Account_save-button"
                      type="submit"
                      form="change-password-form"
                    >
                      Save
                    </button>
                    <button
                      className="btn btn-secondary Account_cancel-button"
                      onClick={() => setIsChangePassword(false)}
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              )}
            </div>

            <div className="col-md-8">
              <p>Email: {currentUser?.email}</p>
              <p>Address: {currentUser?.address}</p>
              <p>Phone number: {currentUser?.phone}</p>
              <p>Date of bitrh: {currentUser?.dateOfBirth}</p>
              <p>Start to work: {currentUser?.startToWork}</p>
              <p>Contract expiration date: {currentUser?.contractExpiration}</p>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
