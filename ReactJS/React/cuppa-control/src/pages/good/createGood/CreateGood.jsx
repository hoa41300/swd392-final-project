import { useEffect, useRef, useState } from "react";
import "./CreateGood";
import { createNewGood, getGoodCategories } from "_helpers/api/good";

export const CreateGood = () => {
  const closeButtonRef = useRef(null);
  const [alertMessage, setAlertMessage] = useState("");

  const [goodCategory, setGoodCategory] = useState([]);

  //options for category here
  const [categoryOptions, setCategoryOptions] = useState([]);

  //fetch good category
  useEffect(() => {
    const fetchProductCategory = async () => {
      try {
        const data = await getGoodCategories();
        setGoodCategory(data.result);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setGoodCategory([]);
          } else {
            alert(`Category \n Status: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchProductCategory();
  }, []);

  useEffect(() => {
    setCategoryOptions([
      ...goodCategory.map((element) => {
        return { value: element.id, label: element.name };
      }),
    ]);
  }, [goodCategory]);

  const [input, setInput] = useState({
    name: "",
    category: 1,
    quantity: 1,
  });
  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInput((values) => ({ ...values, [name]: value }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    try {
      console.log(`Call API here`);
      console.log(
        `Data put to api: Name: ${input.name}, Category: ${input.category}, Quantity: ${input.quantity}`
      );

      const createGoodFunction = async () => {
        //Thia code is call when have API
        const result = await createNewGood({
          name: input.name,
          categoryId: input.category,
          quantity: input.quantity,
        });
        //need to check status range here
        alert(`Add new good success.`);
        closeButtonRef.current.click();

        //   navigate("product-detail?id=2");
        window.location.reload();
      };

      createGoodFunction();
    } catch (error) {
      if (error?.response) {
        const { status, message } = error.response.data;
        alert(`Status: ${status}\nMessage: ${message}`);
      } else {
        alert(`Message: Oops... Something went wrong.`);
      }
    }
  };

  return (
    // <!-- Modal -->
    <div
      className="modal fade"
      id="create-good"
      tabIndex="-1"
      aria-labelledby="addLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="addLabel">
              Add new Good
            </h1>
            <button
              ref={closeButtonRef}
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleFormSubmit} id="create-good-form">
              <div className="mb-3">
                <label htmlFor="name" className="col-form-label">
                  Name:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Name"
                  id="name"
                  name="name"
                  required={true}
                  value={input?.name || ""}
                  onChange={handleInputChange}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="email" className="col-form-label">
                  Category:
                </label>
                <select
                  id="category"
                  name="category"
                  required={true}
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.category || 1}
                >
                  {categoryOptions.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="quantity" className="col-form-label">
                  Quantity:
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="quantity"
                  name="quantity"
                  required={true}
                  value={input?.quantity || 1}
                  onChange={handleInputChange}
                  min="0"
                />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              form="create-good-form"
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
