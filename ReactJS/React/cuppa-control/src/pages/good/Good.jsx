import "./Good.css";
import plusIcon from "assets/images/icon/Plus/Plus.png";
import { Header } from "pages/header/Header";
import { Footer } from "pages/footer/footer";
import { createContext, useEffect, useState } from "react";
import { getGoodByFilter, getGoodCategories } from "_helpers/api/good";
import { Selection } from "_components/selection/Selection";
import { GoodList } from "./goodList/GoodList";
import { PaginationNav } from "_components/paginationNav/PaginationNav";
import { CreateGood } from "./createGood/CreateGood";
import { FilterBar } from "_components/filterBar/FilterBar";

export const Good = () => {
  document.body.style = "background: #efefef";

  const size = 12;
  const [goodList, setGoodList] = useState([]);
  const [name, setName] = useState("");

  const [page, setPage] = useState(1);
  const [sortName, setSortName] = useState("asc");
  const [sortQuantity, setSortQuantity] = useState("asc");
  const [isInUse, setIsInUse] = useState("");
  const [category, setCategory] = useState("");

  const [goodCategory, setGoodCategory] = useState([]);
  const [totalPage, setTotalPage] = useState(1);

  //options for category here
  const [categoryOptions, setCategoryOptions] = useState([]);

  // const handelInputsChange = (event) => {
  //   const name = event.target.name;
  //   const value = event.target.value;
  //   setInputs((values) => ({ ...values, [name]: value }));
  // };
  const handleNameChange = (event) => {
    setName(event.target.value);
    setPage(1);
  };
  const handleSortNameChange = (event) => {
    setSortName(event.target.value);
    setSortQuantity("");
  };
  const handleSortQuantityChange = (event) => {
    setSortQuantity(event.target.value);
    setSortName("");
  };
  const handleIsInUseChange = (event) => {
    setIsInUse(event.target.value);
    setPage(1);
  };
  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
    setPage(1);
  };
  const handlePageClick = (pageNumber) => {
    setPage(pageNumber);
  };

  //use context
  const context = {
    sortName,
    handleSortNameChange,
    sortQuantity,
    handleSortQuantityChange,
  };

  //Fake data
  const goodSample = {
    name: "Cà phê hạt",
    quantity: 10,
    isInUse: true,
    categoryId: 1,
  };

  const sampleGoodList = [
    { ...goodSample, id: 1 },
    { ...goodSample, id: 2 },
    { ...goodSample, id: 3 },
    { ...goodSample, id: 4 },
    { ...goodSample, id: 5 },
    { ...goodSample, id: 6 },
    { ...goodSample, id: 7 },
    { ...goodSample, id: 8 },
    { ...goodSample, id: 9 },
    { ...goodSample, id: 10 },
    { ...goodSample, id: 11 },
    { ...goodSample, id: 12 },
  ];
  //Fake data

  //need to fetch category here
  // const categoryOtions = [
  //   { value: 1, label: "Drink" },
  //   { value: 2, label: "Food" },
  // ];
  const statusOptions = [
    { value: "", label: "All" },
    { value: "true", label: "In use" },
    { value: "false", label: "Not in use" },
  ];

  useEffect(() => {
    //fetch data through API
    const fetchData = async () => {
      try {
        const data = await getGoodByFilter(
          name,
          category,
          isInUse,
          sortName,
          sortQuantity,
          page,
          size
        );

        setTotalPage(data?.result?.totalPage);
        setGoodList(data?.result?.list);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setGoodList([]);
          } else {
            alert(`Product \nStatus: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };
    fetchData();
    //Fetch data through API

    // setGoodList(sampleGoodList);
  }, [name, page, category, isInUse, sortName, sortQuantity]);

  //fetch good category
  useEffect(() => {
    const fetchProductCategory = async () => {
      try {
        const data = await getGoodCategories();
        setGoodCategory(data.result);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setGoodCategory([]);
          } else {
            alert(`Category \n Status: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchProductCategory();
  }, []);

  useEffect(() => {
    setCategoryOptions([
      { value: "", label: "All" },
      ...goodCategory.map((element) => {
        return { value: element.name, label: element.name };
      }),
    ]);
  }, [goodCategory]);

  //Param for FilterBar
  const filterList = [
    {
      options: [
        { value: "asc", label: "Name asc" },
        { value: "desc", label: "Name desc" },
      ],
      selectedValue: sortName,
      handleSelectChange: handleSortNameChange,
    },
    {
      options: [
        { value: "asc", label: "Quantity asc" },
        { value: "desc", label: "Quantity desc" },
      ],
      selectedValue: sortQuantity,
      handleSelectChange: handleSortQuantityChange,
    },
  ];

  return (
    <FilterContext.Provider value={context}>
      <Header />
      <div className="Good_container">
        <div className="Good_container-content p-5 my-5">
          <h3>Good</h3>
          <div className="mt-4 container">
            <div className="d-flex justify-content-between align-items-center">
              <FilterBar
                name={name}
                handleNameChange={handleNameChange}
                filterList={filterList}
              />
              {/* Create Button  */}
              <button
                className="btn btn-primary btn-md"
                data-bs-toggle="modal"
                data-bs-target="#create-good"
              >
                <div className="d-flex justify-content-between align-items-center">
                  <p className="my-0 mx-1">Add</p>
                  <img
                    src={plusIcon}
                    alt="Plus icon"
                    className="d-block mx-1"
                  />
                </div>
              </button>
              {/* Create Button  */}
            </div>
            <div className="d-flex align-items-center justify-content-start mt-4 mb-5">
              <Selection
                options={categoryOptions}
                label={"Category"}
                selectedValue={category}
                handleSelectChange={handleCategoryChange}
              />
              <Selection
                options={statusOptions}
                label={"Status"}
                selectedValue={isInUse}
                handleSelectChange={handleIsInUseChange}
              />
            </div>

            <GoodList goodList={goodList} />
          </div>
          <PaginationNav
            page={page}
            totalPage={totalPage}
            handlePageClick={handlePageClick}
          />
        </div>
      </div>
      <CreateGood />
      <Footer />
    </FilterContext.Provider>
  );
};

export const FilterContext = createContext();
