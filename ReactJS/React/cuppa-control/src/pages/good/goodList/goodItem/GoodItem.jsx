import "./GoodItem.css";
import Edit from "assets/images/icon/Edit/Edit.png";
import Delete from "assets/images/icon/Delete/Delete.png";
import { EditGood } from "./editGood/EditGood";

export const GoodItem = ({ good, id }) => {
  return (
    <>
      <div className="GoodItem_container mb-2 d-flex justify-content-between align-items-center px-3">
        <div
          className="d-flex justify-content-between align-items-center "
          style={{ width: "80%", color: "black" }}
        >
          <div className="d-flex justify-content-start align-item-center">
            <p className="my-0 ">{good.name}</p>
            <p className="my-0 mx-5">Quantity: {good.quantity}</p>
          </div>
          <div className="d-flex align-items-center justify-content-center GoodItem_container-category">
            <p className="my-0">{good.categoryId}</p>
          </div>
        </div>
        <div className="d-flex justify-content-end align-items-center">
          <button
            type="button"
            className="GoodItem_button mx-2 GoodItem_button-edit"
            data-bs-toggle="modal"
            data-bs-target={`#edit-good-${good.id}`}
          >
            <img src={Edit} alt="Edit button" />
          </button>
          <button
            type="button"
            className="GoodItem_button GoodItem_button-delete"
          >
            <img src={Delete} alt="Delete button" />
          </button>
        </div>
      </div>
      <EditGood id={id} />
    </>
  );
};
