import { useEffect, useRef, useState } from "react";
import "./EditGood.css";
import {
  getGoodById,
  getGoodCategories,
  updateGoodInformation,
} from "_helpers/api/good";

export const EditGood = ({ id }) => {
  const closeButtonRef = useRef(null);

  const [input, setInput] = useState({});
  const [good, setGood] = useState({});

  const [goodCategory, setGoodCategory] = useState([]);
  //options for category here
  const [goodOptions, setGoodOptions] = useState([]);

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInput((values) => ({ ...values, [name]: value }));
  };

  const handleFormSubmit = (event) => {
    //prevent default action of form
    event.preventDefault();

    const updateGoodInformationFunction = async () => {
      try {
        //log
        console.log(
          `Input update date: CategoryId: ${input.categoryId}\nName: ${input.name}\nIsInUse: ${input.isInUse}`
        );

        //Thia code is call when have API
        const result = await updateGoodInformation({
          id: good?.id,
          Name: input.name,
          CategoryId: input.categoryId,
          isInUser: input.isInUse,
        });

        alert(`Update Successfully`);
        closeButtonRef.current.click();

        window.location.reload();
      } catch (error) {
        console.log(`Result: ${JSON.stringify(error, null, 2)}`);
        if (error?.response) {
          const { status, message } = error?.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert(`Message: Oops... Something went wrong.`);
        }
      }
    };

    updateGoodInformationFunction();
  };

  useEffect(() => {
    const fetchGoodData = async () => {
      try {
        const data = await getGoodById(id);
        setGood(data?.result);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          alert(`Status: ${status}, Message: ${message}`);
        } else {
          alert(`Oops... Something went wrong.`);
        }
      }
    };

    fetchGoodData();
  }, [id]);

  useEffect(() => {
    //log
    // console.log(`Good status: ${good?.isInUser}`);
    setInput({
      name: good?.name || "",
      // categoryId: good?.categoryId || 1,

      isInUse: good?.isInUser || false,
    });
  }, [good]);

  //fetch good category
  useEffect(() => {
    const fetchGoodCategory = async () => {
      try {
        const data = await getGoodCategories();
        setGoodCategory(data.result);
        //log
        // console.log( `Fetch good by category: ${data.result[0].id}`)
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status === "404") {
            setGoodCategory([]);
          } else {
            alert(`Category \n Status: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchGoodCategory();
  }, []);

  useEffect(() => {
    //log
    // console.log(`Good Category log:\n member 0 name: ${goodCategory[0].name} `)
    setGoodOptions([
      ...goodCategory?.map((element) => {
        //log
        // console.log(`Map good category options: Element name: ${element?.name}`)
        return { value: element?.id, label: element?.name };
      }),
    ]);
  }, [goodCategory]);

  useEffect(() => {
    const mapData = async () => {
      // //log
      // console.log(`goodOptions size: ${goodOptions.length}`);
      // console.log(`good?.categoryId: ${good?.categoryId}`);
      var value = await goodOptions?.find((option) => {
        //log
        // console.log(`Option.label: ${option?.label}`);
        // console.log(`Option.value: ${option?.value}`);
        // console.log(`CategoryId: ${good?.categoryId}`);

        return option.label === good.categoryId;
      });
      //log
      // console.log( `Value: ${JSON.stringify(value)}`);
      // console.log(`Value: id ${value?.value} name: ${value?.label}`);

      setInput((values) => ({ ...values, ["categoryId"]: value?.value || 3 }));
    };
    mapData();
  }, [goodOptions]);

  return (
    // <!-- Modal -->
    <div
      className="modal fade"
      id={`edit-good-${id}`}
      tabIndex="-1"
      aria-labelledby="editGoodLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="editGoodLabel">
              Update good information {id}
            </h1>
            <button
              ref={closeButtonRef}
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form
              onSubmit={handleFormSubmit}
              id={`edit-good-form-${id}`}
              method="post"
            >
              <div className="mb-3">
                <label htmlFor="name" className="col-form-label">
                  Name:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Name"
                  id="name"
                  name="name"
                  required={true}
                  value={input?.name || ""}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="categoryId" className="col-form-label">
                  Category:
                </label>
                <select
                  id="categoryId"
                  name="categoryId"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.categoryId || 1}
                >
                  {goodOptions?.map((option) => (
                    <option key={option.value} value={option.value}>
                      {option.label}
                    </option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="status" className="col-form-label">
                  Status:
                </label>
                <select
                  id="status"
                  name="isInUse"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.isInUse || "False"}
                >
                  <option value="True">In use</option>
                  <option value="False">Not in use</option>
                </select>
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              form={`edit-good-form-${id}`}
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
