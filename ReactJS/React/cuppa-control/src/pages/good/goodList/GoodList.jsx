import "./GoodList.css";
import { GoodItem } from "./goodItem/GoodItem";

export const GoodList = ({ goodList }) => {
  return (
    <div className="mt-5 pt-4 GoodList_container">
      {goodList?.map((good) => (
        
        <GoodItem key={good.id} good={good} id={good.id} />
      ))}
    </div>
  );
};
