import "./Login.css";
import groupImg from "assets/images/Group.png";
import userImg from "assets/images/user.png";
import lockImg from "assets/images/lock.png";
import emailImg from "assets/images/email.png";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import { Link, useNavigate } from "react-router-dom";
import { useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { useFormik } from "formik";
import jwtDecode from "jwt-decode";
import * as Yup from "yup";
import { setAuthentication, setRole } from "_store/slices/auth.slice";
import { login } from "_helpers/api/login";

const Login = () => {
  document.body.style = "background: #4C6EF8";

  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const [inputs, setInputs] = useState({});

  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState(null);

  const formik = useFormik({
    initialValues: {
      email: "",
      // username: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Invalid email address").required("Required"),
      // username: Yup.string().required("Required"),
      password: Yup.string().required("Required"),
    }),
    onSubmit: async (values) => {
      try {
        const data = await login({
          Email: values.email,
          // username: values.username,
          Password: values.password,
        });

        const token = data?.result.token;
        const fullName = data?.result?.user?.fullName;
        const avatarUrl = data?.result?.user?.avatar;
        const id = data?.result?.user?.id;
        const auth = jwtDecode(token);
        const role = auth.Role;

        dispatch(setRole({ role }));
        dispatch(setAuthentication(true));
        const user = {
          token,
          id,
          fullName,
          avatarUrl,
          role,
        };
        localStorage.setItem("user", JSON.stringify(user));
        if (user?.role === "Admin" || user?.role === "ShopOwner") {
          navigate("/menu");
        } else {
          navigate("/worksheet");
        }
      } catch (error) {
        // if (error.response) {
        //   setShowAlert(true);
        //   if (error.response.status === 401) {
        //     setAlertMessage("Incorrect email or password");
        //   } else {
        //     setAlertMessage("Oops... Something went wrong.");
        //   }
        // }

        //Need to fix here
        console.log(error.response.data);
        if (error.response) {
          setShowAlert(true);
          const { status, message } = error.response.data;
          setAlertMessage(message);
        } else {
          setAlertMessage("Oops... Something went wrong.");
        }
      }
    },
  });

  const handleOverlayClick = () => {
    setShowAlert(false);
  };

  return (
    <>
      <div className="login">
        <div className="Login_container">
          <img
            src={groupImg}
            alt="Decoration Icon"
            className="Login_decorateIcon"
          />

          <form onSubmit={formik.handleSubmit} className="Login_form-login">
            <div className="Login_inputBox">
              <img src={userImg} alt="user icon" className="Login_inputIcon" />
              <input
                type="email"
                placeholder="Email"
                className="Login_loginInput"
                required={true}
                name="email"
                // name="username"
                value={formik.values.email}
                // value={formik.values.username}
                onChange={formik.handleChange}
              />
            </div>
            <div className="Login_inputBox">
              <img
                src={lockImg}
                alt="password icon"
                className="Login_inputIcon"
              />
              <input
                type="password"
                placeholder="Password"
                className="Login_loginInput"
                required={true}
                name="password"
                value={formik.values.password}
                onChange={formik.handleChange}
              />
            </div>

            <button className="Login_loginButton" type="submit">
              LOGIN
            </button>

            <div className="Login_alignRight">
              <Link to="/forgot-password" className="Login_forgotLink">
                {" "}
                Forgot Password?
              </Link>
            </div>
          </form>

          {showAlert && (
            <div className="Login_alert-overlay" onClick={handleOverlayClick}>
              <Alert severity="error" className="Login_alert">
                <AlertTitle>{alertMessage}</AlertTitle>
              </Alert>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

const ForgotPassword = () => {
  //log
  console.log("This is forgot password page");
  document.body.style = "background: #4C6EF8";

  const inputs = useRef({});

  // const [inputs, setInputs] = useState({});

  //Handle input change
  // const handleChange = (event) => {
  //   const name = event.target.name;
  //   const value = event.target.value;
  //   setInputs((values) => ({ ...values, [name]: value }));
  // };
  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    inputs.current = { ...inputs.current, [name]: value };
  };

  //Handle form submit
  const handleSubmit = (event) => {
    event.preventDefault();
    alert(`Email: ${inputs.current.email}`);
  };

  return (
    <div className="login">
      <div className="Login_container" style={{ height: "387px" }}>
        <img
          src={groupImg}
          alt="Decoration Icon"
          className="Login_decorateIcon"
        />

        <form action="#" className="Login_form-login" onSubmit={handleSubmit}>
          <div className="Login_inputBox">
            <img
              src={emailImg}
              alt="Email icon"
              style={{ width: "25px", height: "18px" }}
            />
            <input
              type="email"
              placeholder="Enter email"
              className="Login_loginInput"
              required={true}
              name="email"
              onChange={handleChange}
            />
          </div>

          <button className="Login_loginButton">SEND PASSWORD</button>
        </form>

        <div className="Login_alignRight">
          <Link to="/login" className="Login_forgotLink">
            Login
          </Link>
        </div>
      </div>
    </div>
  );
};

export { Login, ForgotPassword };
