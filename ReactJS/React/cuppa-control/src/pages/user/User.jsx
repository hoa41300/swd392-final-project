import { Header } from "pages/header/Header";
import "./User.css";
import { Footer } from "pages/footer/footer";
import plusIcon from "assets/images/icon/Plus/Plus.png";
import { Selection } from "_components/selection/Selection";
import { PaginationNav } from "_components/paginationNav/PaginationNav";
import { FilterBar } from "_components/filterBar/FilterBar";
import { useEffect, useState } from "react";
import { UserList } from "./userList/UserList";
import { filterUserList } from "_helpers/api/user";
import Avatar from "assets/images/avatar/avatar.jpg";
import { CreateUser } from "./createUser/CreateUser";

export const User = () => {
  document.body.style = "background: #efefef";

  const size = 10;
  const [name, setName] = useState("");
  const [page, setPage] = useState(1);
  const [role, setRole] = useState("");
  const [isActive, setIsACtive] = useState("");
  const [gender, setGender] = useState("");
  const [sortCreatedDate, setSortCreatedDate] = useState("");
  const [sortName, setSortName] = useState("asc");
  const [users, setUsers] = useState([]);
  const [totalPage, setTotalPage] = useState(0);

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handlePageClick = (pageNumber) => {
    setPage(pageNumber);
  };

  const handleRoleChange = (e) => {
    setRole(e.target.value);
  };

  const handleIsActiveChange = (e) => {
    setIsACtive(e.target.value);
  };

  const handleGenderChange = (e) => {
    setGender(e.target.value);
  };

  const handleSortCreatedDate = (e) => {
    setSortCreatedDate(e.target.value);
    setSortName("");
  };

  const handleSortNameChange = (e) => {
    setSortName(e.target.value);
    setSortCreatedDate("");
  };

  //Fake data
  // const fakeUser = {
  //   Id: 1,
  //   FullName: "Kim Da Mi",
  //   Avatar: "https://manga-reader-clone.s3.amazonaws.com/avatar_Kim_Da_Mi.jpg",
  //   ContractExpiration: "14/07/2025",
  //   Role: "User",
  //   IsActive: "true",
  //   Gender: "Female",
  //   DateOfBirth: "14/07/2002",
  //   CreatedDate: "30/02/2023",
  //   Salary: "",
  // };
  // const fakeUserList = [
  //   fakeUser,
  //   { ...fakeUser, Id: 2 },
  //   { ...fakeUser, Id: 3 },
  //   { ...fakeUser, Id: 4 },
  //   { ...fakeUser, Id: 5 },
  //   { ...fakeUser, Id: 6 },
  //   { ...fakeUser, Id: 7 },
  //   { ...fakeUser, Id: 8 },
  //   { ...fakeUser, Id: 9 },
  //   { ...fakeUser, Id: 10 },
  // ];
  //Fake data

  //Fetch Data
  useEffect(() => {
    //fetch data function
    const fetchData = async () => {
      try {
        const data = await filterUserList(
          name,
          role,
          isActive,
          gender,
          sortCreatedDate,
          sortName,
          page,
          size
        );
        setUsers(data?.result?.list);
        setTotalPage(data?.result?.totalPage);
      } catch (error) {
        setUsers([]);
      }
    };

    fetchData();
    //fetch data function

    //fake data
    // setUsers(fakeUserList);
  }, [name, role, isActive, gender, sortCreatedDate, sortName, page]);
  //Fetch Data

  //param for Filterbar
  const filterList = [
    {
      options: [
        {
          value: "asc",
          label: "Name asc",
        },
        {
          value: "desc",
          label: "Name desc",
        },
      ],
      selectedValue: sortName,
      handleSelectChange: handleSortNameChange,
    },

    {
      options: [
        {
          value: "asc",
          label: "Create date asc",
        },
        {
          value: "desc",
          label: "Create date desc",
        },
      ],
      selectedValue: sortCreatedDate,
      handleSelectChange: handleSortCreatedDate,
    },
  ];

  //Other param
  const roleOptions = [
    { value: "", label: "All" },
    { value: "Staff", label: "Staff" },
    { value: "ShopOwner", label: "Shop Owner" },
    { value: "Admin", label: "Admin" },
  ];
  const genderOptions = [
    { value: "", label: "All" },
    { value: "Male", label: "Male" },
    { value: "Female", label: "Female" },
  ];
  const statusOptions = [
    { value: "", label: "All" },
    { value: "Active", label: "Active" },
    { value: "Inactive", label: "Inactive" },
  ];

  return (
    <>
      <Header />
      <div className="Good_container">
        <div className="Good_container-content p-5 my-5">
          <h3>User</h3>
          <div className="mt-4 container">
            <div className="d-flex justify-content-between align-items-center">
              <FilterBar
                name={name}
                handleNameChange={handleNameChange}
                filterList={filterList}
              />
              {/* Create Button  */}
              <button
                className="btn btn-primary btn-md"
                data-bs-toggle="modal"
                data-bs-target="#create-user"
              >
                <div className="d-flex justify-content-between align-items-center">
                  <p className="my-0 mx-1">Create</p>
                  <img
                    src={plusIcon}
                    alt="Plus icon"
                    className="d-block mx-1"
                  />
                </div>
              </button>
              {/* Create Button  */}
            </div>
            <div className="d-flex align-items-center justify-content-start mt-4 mb-5">
              <Selection
                options={roleOptions}
                label={"Role"}
                selectedValue={role}
                handleSelectChange={handleRoleChange}
              />
              <Selection
                options={genderOptions}
                label={"Gender"}
                selectedValue={gender}
                handleSelectChange={handleGenderChange}
              />
              <Selection
                options={statusOptions}
                label={"Status"}
                selectedValue={isActive}
                handleSelectChange={handleIsActiveChange}
              />
            </div>

            {/* List user here */}
            <UserList userList={users} />
          </div>
          <PaginationNav
            page={page}
            totalPage={totalPage}
            handlePageClick={handlePageClick}
          />
        </div>
      </div>

      <CreateUser />
      <Footer />
    </>
  );
};
