import { useEffect, useRef, useState } from "react";
import "./UpdateUser.css";
import { updateUserInformation } from "_helpers/api/user";
import {
  formatDateToString,
  formatStringToDate,
  formatToSQLDate,
} from "_helpers/utils";

export const UpdateUser = ({ user, setUser }) => {
  //log
  console.log(`user fullname: ${user.fullName}`);

  const closeButtonRef = useRef(null);

  const [input, setInput] = useState({});

  useEffect(() => {
    setInput({
      FullName: user?.fullName || "",
      StartToWork: formatToSQLDate(user?.startToWork),
      Phone: user?.phone || "",
      Address: user?.address || "",
      Note: user?.note || "",
      Email: user?.email || "",
      ContractExpiration: formatToSQLDate(user?.contractExpiration),
      Avatar: user?.avatar || "",
      Gender: user?.gender || "",
      DateOfBirth: formatToSQLDate(user?.dateOfBirth),
      Salary: user?.cofficientsSalary || "",
    });
  }, [user]);

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    const type = event.target.type;

    if (type === "date") {
      var dateString = formatDateToString(value);
      setInput((values) => ({ ...values, [name]: dateString }));
    } else {
      setInput((values) => ({ ...values, [name]: value }));
    }
  };

  const handleFormSubmit = (event) => {
    //prevent default action of form
    event.preventDefault();
    const fetchData = async () => {
      try {
        console.log(`FullName: ${input.FullName}`);
    console.log(`DateOfBirth: ${formatDateToString(input.DateOfBirth)}`);
        //Thia code is call when have API
        const response = await updateUserInformation({
          Id: user?.id,
          FullName: input.FullName,
          StartToWork: input.StartToWork,
          Phone: input.Phone,
          Address: input.Address,
          Note: input.Note,
          Email: input.Email,
          ContractExpiration: input.ContractExpiration,
          Avatar: "", // need to remove
          Gender: input.Gender,
          DateOfBirth: input.DateOfBirth,
          Salary: input.Salary,
        });

        // setUser(response?.result);
        closeButtonRef.current.click();

        // navigate(`/menu/product-detail?id=${product.id}`);
        window.location.reload();
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchData();
  };

  const today = new Date();
  const maxDateOfBirth = new Date(today.getFullYear() - 15, 1, 1)
    .toISOString()
    .substr(0, 10);

  return (
    // <!-- Modal -->
    //log

    <div
      className="modal fade"
      id="edit-user"
      tabIndex="-1"
      aria-labelledby="editUserLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="editUserLabel">
              Update user information
            </h1>
            <button
              ref={closeButtonRef}
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form
              onSubmit={handleFormSubmit}
              id="edit-user-update-form"
              method="post"
            >
              <input
                type="hidden"
                className="form-control"
                name="id"
                value={user?.id || 1}
                disabled
              />
              <div className="mb-3">
                <label htmlFor="email" className="col-form-label">
                  Email:
                </label>
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email"
                  id="email"
                  name="Email"
                  required={true}
                  value={input?.Email || ""}
                  onChange={handleInputChange}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="phone" className="col-form-label">
                  Phone number:
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="phone"
                  name="Phone"
                  required={true}
                  value={input?.Phone || "0123456777"}
                  onChange={handleInputChange}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="address" className="col-form-label">
                  Address:
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="address"
                  name="Address"
                  required={true}
                  value={input?.Address || ""}
                  onChange={handleInputChange}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="dateOfBirth" className="col-form-label">
                  Date of birth:
                </label>
                <input
                  type="date"
                  className="form-control"
                  id="dateOfBirth"
                  name="DateOfBirth"
                  required={true}
                  // value={formatStringToDate(
                  //   input?.DateOfBirth || user?.dateOfBirth
                  // )}
                  value={formatStringToDate(input?.DateOfBirth)}
                  //   value="2013-01-08"
                  onChange={handleInputChange}
                  max={maxDateOfBirth}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="gender" className="col-form-label">
                  Gender:
                </label>
                <select
                  id="gender"
                  name="Gender"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.Gender || "Male"}
                >
                  <option value="Male">Male</option>
                  <option value="Female">Female</option>
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="startToWork" className="col-form-label">
                  Start to work:
                </label>
                <input
                  type="date"
                  className="form-control"
                  id="startToWork"
                  name="StartToWork"
                  required={true}
                  value={formatStringToDate(input?.StartToWork)}
                  max={formatStringToDate(input?.ContractExpiration)}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="contractExpiration" className="col-form-label">
                  Contract expiration date:
                </label>
                <input
                  type="date"
                  className="form-control"
                  id="contractExpiration"
                  name="ContractExpiration"
                  required={true}
                  value={formatStringToDate(input?.ContractExpiration)}
                  onChange={handleInputChange}
                  min={formatStringToDate(input?.StartToWork)}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="salary" className="col-form-label">
                  Cofficient Salary
                </label>
                <input
                  type="number"
                  step="0.1"
                  className="form-control"
                  id="salary"
                  name="Salary"
                  required={true}
                  value={input?.Salary || user?.cofficientsSalary || "0"}
                  onChange={handleInputChange}
                  min="0"
                />
              </div>

              <div className="mb-3">
                <label htmlFor="note" className="col-form-label">
                  Note:
                </label>
                <textarea
                  id="note"
                  name="Note"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.Note || ""}
                />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              form="edit-user-update-form"
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
