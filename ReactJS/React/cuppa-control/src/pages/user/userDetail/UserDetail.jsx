import "./UserDetail.css";
import { Header } from "pages/header/Header";
import { Footer } from "pages/footer/footer";
import { getUserById } from "_helpers/api/user";
import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import ActiveIcon from "assets/images/icon/Active/Active.png";
import InactiveIcon from "assets/images/icon/Inactive/Inactive.png";
import { changeUserStatus, asignUserRole } from "_helpers/api/user";
import { UpdateUser } from "./updateUser/UpdateUser";
import defaultAvatar from "assets/images/avatar/default_avatar.jpg";

export const UserDetail = () => {
  document.body.style = "background: #efefef";
  const currentUser = JSON.parse(localStorage.getItem("user"));

  const navigate = useNavigate();
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const id = searchParams.get("id");

  const [user, setUser] = useState({});
  const [role, setRole] = useState("");

  const handleChangeStatus = () => {
    const changeStatus = async () => {
      try {
        const response = await changeUserStatus(id);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    changeStatus();
    //relaod the page
    window.location.reload();
  };

  const handleUpdateRole = () => {
    const updateRole = async () => {
      try {
        const response = await asignUserRole(id);
      } catch (error) {
        if (error?.response) {
          const { status, message } = error.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    updateRole();
    window.location.reload();
  };

  //fake data
  // const fakeUser = {
  //   Id: 1,
  //   FullName: "Kim Da Mi",
  //   Avatar: "https://manga-reader-clone.s3.amazonaws.com/avatar_Kim_Da_Mi.jpg",
  //   StartToWork: "14/07/2022",
  //   ContractExpiration: "14/07/2025",
  //   Role: "User",
  //   IsActive: "true",
  //   Gender: "Female",
  //   Phone: "0123456789",
  //   Address: "Ho Chi Minh, Viet Nam",
  //   DateOfBirth: "14/07/2002",
  //   CreatedDate: "30/02/2023",
  //   Salary: "",
  // };

  //fetch data
  useEffect(() => {
    //fetch fucntion here
    const fetchData = async () => {
      const data = await getUserById(id);

      setUser(data.result);
      setRole(user.role);

      // setUser(fakeUser);
    };

    fetchData();
  }, []);

  return (
    <>
      <Header />
      <div className="UserDetail_container p-0">
        <div className="UserDetail_container-bar mt-1"></div>
        <div className="UserDetail_container-content ">
          <div className="UserDetail_avatar">
            <div className="UserDetail_avatar-image">
              <img
                // src="https://manga-reader-clone.s3.amazonaws.com/avatar_Kim_Da_Mi.jpg"
                // alt="Avatar image"
                src={user?.avatar || defaultAvatar}
                alt="Avatar image"
              />
            </div>
            <p className="text-center">{user.fullName}</p>
          </div>
          <div className="row">
            <div className="col-md-4 pr-3 UserDetail_change-password">
              {/* <button
                className="btn btn-primary  -button"
                // onClick={handleIsChangePasswordChange}
              >
                Change Password
              </button> */}
            </div>

            <div className="col-md-8 ">
              <div className="d-flex justify-content-between align-items-start">
                <div>
                  <p>Email: {user.email}</p>
                  <p>Phone number: {user.phone}</p>
                  <p>Address: {user.address}</p>
                  <p>Date of bitrh: {user.dateOfBirth}</p>
                  <p>Gender: {user.gender}</p>
                  <p>Role: {user.role}</p>
                  <p>Create date: {user.createDate}</p>
                  <p>Start to work: {user.startToWork}</p>
                  <p>Contract expiration date: {user.contractExpiration}</p>
                  <p>Coefficient salary: {user.cofficientsSalary}</p>
                  <p>Note: {user.note}</p>
                </div>

                {user.role === "Staff" && currentUser?.role === "Admin" && (
                  <button
                    className={`btn btn-danger UserDetail_button-changeRole UserDetail_button`}
                    onClick={() => handleUpdateRole()}
                  >
                    <div className="d-flex justify-content-between align-items-center w-100 UserDetail_button-content">
                      <p className="my-0">Assign to owner</p>
                    </div>
                  </button>
                )}

                <button
                  className={`btn UserDetail_button 
              ${
                user?.status === "Active"
                  ? "btn-success UserDetail_button-active"
                  : "btn-danger UserDetail_button-inactive"
              }`}
                  onClick={() => handleChangeStatus()}
                  disabled={
                    currentUser?.role !== "Admin" &&
                    (user?.role === "Admin" || user?.role === "ShopOwner")
                  }
                >
                  <div className="d-flex justify-content-between align-items-center w-100 UserDetail_button-content">
                    <img
                      src={
                        user?.status === "Active" ? ActiveIcon : InactiveIcon
                      }
                      alt="Status icon"
                      className="UserDetail_icon"
                      style={{ height: "12px" }}
                    />
                    <p className="my-0">{user?.status}</p>
                  </div>
                </button>
              </div>

              <button
                className="btn btn-primary btn-md"
                data-bs-toggle="modal"
                data-bs-target="#edit-user"
                disabled={
                  currentUser?.role !== "Admin" &&
                  (user?.role === "Admin" ||
                    (user?.role === "ShopOwner" &&
                      user?.id !== currentUser?.id))
                }
              >
                Update
              </button>
            </div>
          </div>
        </div>
      </div>

      <UpdateUser user={user} setUser={setUser} />
      <Footer />
    </>
  );
};
