import { UserItem } from "../userItem/UserItem";
import "./UserList.css";

export const UserList = ({ userList }) => {
  return (
    <div className="mt-5 pt-4 UserList_container">
      {userList.map((user) => (
        <UserItem user={user} />
      ))}
    </div>
  );
};
