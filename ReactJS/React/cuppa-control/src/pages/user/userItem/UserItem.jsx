import "./UserItem.css";
import Edit from "assets/images/icon/Edit/Edit.png";
import Delete from "assets/images/icon/Delete/Delete.png";
import { useNavigate } from "react-router-dom";
import defaultAvatar from "assets/images/avatar/default_avatar.jpg";

export const UserItem = ({ user }) => {
  const navigate = useNavigate();

  const handleEditButtonOnClick = () => {
    navigate(`user-detail?id=${user.id}`);
  };

  return (
    <div className="row UserItem_container mb-2 px-2">
      <div
        className="col-md-6 d-flex justify-content-start align-items-center"
        style={{ color: "black" }}
      >
        <div className="UserItem_img-container">
          <img
            src={user?.avatar || defaultAvatar}
            className="rounded-circle"
            alt="avatar image"
          />
        </div>
        <p className="my-0 mx-3">{user.fullName}</p>
        <p className="my-0">Create date: {user.createDate}</p>
      </div>
      <div className="col-md-6 d-flex justify-content-end align-items-center">
        <div
          className={
            `d-flex align-items-center justify-content-center UserItem_container-gender` +
            `${user?.gender === "Male" ? "_male" : "_female"}`
          }
        >
          <p className="my-0">{user.gender}</p>
        </div>
        <div className="d-flex align-items-center justify-content-center UserItem_container-role mx-3">
          <p className="my-0">
            {user?.role === "ShopOwner" ? "Shop Owner" : user?.role}
          </p>
        </div>
        <div
          className={`d-flex align-items-center justify-content-center 
        ${
          user?.status === "Active"
            ? "UserItem_container-status_active"
            : "UserItem_container-status_inactive"
        }`}
        >
          <p className="my-0">{user.status}</p>
        </div>
        <button
          className="UserItem_button mx-2 UserItem_button-edit"
          onClick={() => handleEditButtonOnClick()}
        >
          <img src={Edit} alt="Edit button" title="Edit user" />
        </button>
      </div>
    </div>
  );
};
