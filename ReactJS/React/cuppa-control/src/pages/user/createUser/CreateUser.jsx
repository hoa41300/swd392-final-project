import { useRef, useState } from "react";
import "./CreateUser.css";
import { useNavigate } from "react-router-dom";
import { formatDateToString, formatStringToDate } from "_helpers/utils";
import { createUser } from "_helpers/api/user";

export const CreateUser = () => {
  const closeButtonRef = useRef(null);

  const navigate = useNavigate();

  const [input, setInput] = useState({});

  const handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    const type = event.target.type;

    setInput((values) => ({ ...values, [name]: value }));
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    console.log(`FullName: ${input.FullName}`);
    console.log(`DateOfBirth: ${formatDateToString(input.DateOfBirth)}`);
    console.log(`Start to Work: ${formatDateToString(input.StartToWork)}`);
    console.log(
      `Contract expiration date : ${formatDateToString(
        input.ContractExpiration
      )}`
    );
    const creatNewUser = async () => {
      try {
        // console.log(`Call API here`);
        // console.log(
        //   `Data put to api: \n
        //   FullName: ${input.FullName}\n
        //   Phone: ${input.Phone}\n
        //   Address: ${input.Address}\n
        //   Gender: ${input.Gender}\n
        //   Email: ${input.Email}\n
        //   DateOfBirth: ${input.DateOfBirth}\n
        //   StartToWork: ${input.StartToWork}\n
        //   ContractExpiration: ${input.ContractExpiration}\n
        //   Note: ${input.Note}\n        `
        // );

        //This code is call when have API

        const data = await createUser({
          FullName: input.FullName,
          Phone: input.Phone,
          Address: input.Address,
          Gender: input.Gender,
          Email: input.Email,
          DateOfBirth: input.DateOfBirth,
          StartToWork: input.StartToWork,
          ContractExpiration: input.ContractExpiration,
          Note: input.Note,
        });

        const id = await data?.result?.id;
        //need to check status range here
        closeButtonRef.current.click();
        console.log(`id: ${id}`);

        // navigate(`user-detail?id=${id}`);
        // window.location.reload();
      } catch (error) {
        if (error?.response) {
          const { status, message } = error?.response?.data;
          alert(`Status: ${status}\nMessage: ${message}`);
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };
    creatNewUser();
  };

  const today = new Date();
  const maxDateOfBirth = new Date(today.getFullYear() - 15, 1, 1)
    .toISOString()
    .substr(0, 10);

  return (
    // <!-- Modal -->
    <div
      className="modal fade"
      id="create-user"
      tabIndex="-1"
      aria-labelledby="createUserLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h1 className="modal-title fs-5" id="createUserLabel">
              Create User
            </h1>
            <button
              ref={closeButtonRef}
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form onSubmit={handleFormSubmit} id="create-user-form">
              <div className="mb-3">
                <label htmlFor="fullName" className="col-form-label">
                  FullName:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Staff name"
                  id="fullName"
                  name="FullName"
                  required={true}
                  value={input?.FullName || ""}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="phone" className="col-form-label">
                  Phone number:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="0123456789"
                  id="phone"
                  name="Phone"
                  required={true}
                  value={input?.Phone || ""}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="address" className="col-form-label">
                  Address:
                </label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Address"
                  id="address"
                  name="Address"
                  required={true}
                  value={input?.Address || ""}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="gender" className="col-form-label">
                  Gender:
                </label>
                <select
                  id="gender"
                  name="Gender"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.Gender || "Male"}
                >
                  <option value="true">Male</option>
                  <option value="false">Female</option>
                </select>
              </div>

              <div className="mb-3">
                <label htmlFor="email" className="col-form-label">
                  Email:
                </label>
                <input
                  type="eamail"
                  className="form-control"
                  placeholder="abc@gmail.com"
                  id="email"
                  name="Email"
                  required={true}
                  value={input?.Email || ""}
                  onChange={handleInputChange}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="dateOfBirth" className="col-form-label">
                  Date of birth:
                </label>
                <input
                  type="date"
                  className="form-control"
                  id="dateOfBirth"
                  name="DateOfBirth"
                  required={true}
                  value={input?.DateOfBirth || ""}
                  onChange={handleInputChange}
                  max={maxDateOfBirth}
                />
              </div>

              <div className="mb-3">
                <label htmlFor="startToWork" className="col-form-label">
                  Start to work:
                </label>
                <input
                  type="date"
                  className="form-control"
                  id="startToWork"
                  name="StartToWork"
                  required={true}
                  value={
                    input?.StartToWork || new Date().toISOString().substr(0, 10)
                  }
                  onChange={handleInputChange}
                  max={
                    input?.ContractExpiration ||
                    new Date().toISOString().substr(0, 10)
                  }
                />
              </div>

              <div className="mb-3">
                <label htmlFor="contractExpiration" className="col-form-label">
                  Contract expiration date:
                </label>
                <input
                  type="date"
                  className="form-control"
                  id="contractExpiration"
                  name="ContractExpiration"
                  required={true}
                  value={
                    input?.ContractExpiration ||
                    new Date().toISOString().substr(0, 10)
                  }
                  onChange={handleInputChange}
                  min={
                    input?.StartToWork || new Date().toISOString().substr(0, 10)
                  }
                />
              </div>

              <div className="mb-3">
                <label htmlFor="note" className="col-form-label">
                  Note:
                </label>
                <textarea
                  id="note"
                  name="Note"
                  className="form-control"
                  onChange={handleInputChange}
                  value={input?.Note || ""}
                />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-bs-dismiss="modal"
            >
              Close
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              form="create-user-form"
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
