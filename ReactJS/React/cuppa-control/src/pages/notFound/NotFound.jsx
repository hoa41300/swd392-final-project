import "./NotFound.css";

export const NotFound = () => {
  document.body.style = "background: #4C6EF8";
  return (
    <>
      <div className="NotFound_container">
        <h1>404 NOT FOUND</h1>
      </div>
    </>
  );
};
