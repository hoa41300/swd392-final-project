import "./RevenueList.css";
import { getRevenue } from "_helpers/api/revenue";
import { RevenueItem } from "./revenueItem/RevenueItem";
import { useEffect, useState } from "react";
import { getInvoiceByFilter } from "_helpers/api/invoice";

export const RevenueList = ({ revenues }) => {
//   document.body.style = "background: #efefef";

//   const pageSize = 10;
//   const [pageNumber, setPageNumber] = useState(1);
//   const [revenueList, setRevenueList] = useState([]);
//   const [totalPage, setTotalPage] = useState(0);

//   const handlePageNumberClick = (pageNumber) => {
//     setPageNumber(pageNumber);
//   };

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         //call api here
//         const data = await getInvoiceByFilter(pageNumber, pageSize, "");

//         //log
//         console.log(`Revenue list: ${JSON.stringify(data, null, 2)}`);

//         setRevenueList(data?.result?.list);
//         setTotalPage(data?.result?.totalPage);
//       } catch (error) {
//         //log
//         console.log(`Error: ${JSON.stringify(error, null, 2)}`);

//         if (error?.response) {
//           const { status, message } = error?.response?.data;
//           if (status === "404") {
//             setRevenueList([]);
//           } else {
//             alert(`Status: ${status}\nMessage: ${message}`);
//           }
//         } else {
//           alert("Oops... Something went wrong.");
//         }
//       }
//     };

//     fetchData();
//   }, [pageNumber]);

  return (
    <div className="mt-4 pt-4 GoodList_container">
      {revenues?.map((revenue) => (
        <RevenueItem key={revenue.id} revenue={revenue} id={revenue.id} />
      ))}
    </div>
  );
};
