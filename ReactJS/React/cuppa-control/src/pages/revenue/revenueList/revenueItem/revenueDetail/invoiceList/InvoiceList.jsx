import "./InvoiceList.css";
import { InvoiceDetail } from "./invoiceDetail/InvoiceDetail";

export const InvoiceList = ({ invoices }) => {
  console.log(`Invoice: ${JSON.stringify(invoices, null, 2)}`)
  return (
    <div className="mt-4 pt-4 GoodList_container">
      {invoices?.map((invoice) => (
        <InvoiceDetail key={invoice.id} invoice={invoice} id={invoice.id} />
      ))}
    </div>
  );
};
