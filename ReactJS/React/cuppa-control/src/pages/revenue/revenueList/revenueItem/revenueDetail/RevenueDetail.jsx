import { Header } from "pages/header/Header";
import "./RevenueDetail.css";
import { Footer } from "pages/footer/footer";
import { useLocation } from "react-router-dom";
import { PaginationNav } from "_components/paginationNav/PaginationNav";
import { useEffect, useState } from "react";
import { getInvoiceById } from "_helpers/api/invoice";
import { InvoiceList } from "./invoiceList/InvoiceList";

export const RevenueDetail = () => {
  document.body.style = "background: #efefef";

  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);

  const [id, setId] = useState(searchParams.get("id"));
  const [productList, setProductList] = useState([]);
  const [invoice, setInvoice] = useState({});

  //fetch data
  useEffect(() => {
    const fetchInvoice = async () => {
      try {
        //log
        console.log(`id: ${id}`);

        //Call fetch API here
        console.log(`Call fetch API here.`);
        const data = await getInvoiceById(id);
        // setInvoice(data?.result);
        setProductList(data?.result?.invoiceDetails);

        console.log(`Data: ${JSON.stringify(data, null, 2)}`);
        // console.log(`Invoice: ${JSON.stringify(invoice, null, 2)}`);
        console.log(
          `Product list: ${JSON.stringify(
            data?.result?.invoiceDetails,
            null,
            2
          )}`
        );
      } catch (error) {
        if (error?.response) {
          const { status, message } = error.response?.data;
          alert(`Status: ${status}\n Message: ${message}`);
          setInvoice({});
          setProductList({});
        } else {
          alert(`Oops... Something went wrong.`);
          setInvoice({});
          setProductList({});
        }
      }
    };

    fetchInvoice();
  }, [id]);

  return (
    <>
      <Header />
      <div className="Good_container">
        <div className="Good_container-content p-5 my-5">
          <h3>Invoice Detail</h3>
          <div className="mt-4 container">
            <InvoiceList invoices={productList} />
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
