import { useNavigate } from "react-router-dom";
import "./RevenueItem.css";

export const RevenueItem = ({ revenue, id }) => {
  const navigate = useNavigate();

  const handleButtonClick = () => {
    navigate(`revenue-detail?id=${id}`);
  };

  return (
    <>
      <button
        type="button"
        className="RevenueItem_container mb-2 px-4 d-md-flex align-items-center"
        onClick={handleButtonClick}
      >
        <div className="row flex-grow-1">
          <div className="col-md-3 col-sm-6 d-flex justify-content-start ">
            Staff: {revenue?.username}
          </div>
          <div className="col-md-3 col-sm-6 d-flex justify-content-start ">
            Total: {revenue?.total} $
          </div>
          <div className="col-md-3 col-sm-6 my-0 d-flex justify-content-md-end justify-content-sm-start">
            Date: {revenue?.date}
          </div>
          <div className="col-md-3 col-sm-6 my-0 d-flex justify-content-md-end justify-content-sm-end">
            Status: {revenue?.status}
          </div>
        </div>
      </button>
    </>
  );
};
