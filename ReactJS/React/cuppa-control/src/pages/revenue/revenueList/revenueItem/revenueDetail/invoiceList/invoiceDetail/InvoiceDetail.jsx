import "./InvoiceDetail.css";

export const InvoiceDetail = ({ invoice, id }) => {
  return (
    <>
      <div className="InvoiceDetail_container  mb-2  px-1">
        <div className="row px-0">
          <div className="col-md-2 col-sm-6  py-0 ">
            <div className="InvoiceDetail-img-container">
              <img src={invoice?.product?.productImg} className="rounded" />
            </div>
          </div>

          <div className="col-md-10 d-flex justify-content-between align-items-center">
            <div className="col-md-3 col-sm-6 ">{invoice?.product?.name}</div>
            <div className="col-md-3 col-sm-6 my-0 ">
              Quantity: {invoice?.quantity}
            </div>
            <div className="col-md-2 col-sm-6 my-0 ">
              Price: {invoice?.price} $
            </div>
            <div className="col-md-2 col-sm-6 my-0 ">
              Total: {invoice?.total} $
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
