import { Header } from "pages/header/Header";
import "./Revenue.css";
import { Footer } from "pages/footer/footer";
import { useEffect, useState } from "react";
import { PaginationNav } from "_components/paginationNav/PaginationNav";
import { getInvoiceByFilter } from "_helpers/api/invoice";
import { RevenueList } from "./revenueList/RevenueList";
import { formatDateToString } from "_helpers/utils";

export const Revenue = () => {
  document.body.style = "background: #efefef";

  const pageSize = 10;
  const [pageNumber, setPageNumber] = useState(1);
  const [revenueList, setRevenueList] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const [date, setDate] = useState("");
  const [formSubmit, setFormSubmit] = useState(true);

  const handlePageNumberClick = (pageNumber) => {
    setPageNumber(pageNumber);
  };

  const handleDateChange = (event) => {
    var value = event.target.value;
    setDate(value);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();

    setFormSubmit(!formSubmit);
    setPageNumber(1);
  };

  useEffect(() => {
    const fetchData = async () => {
      //log
      console.log(`Call Api fetch data with date: ${date}`);
      try {
        //call api here
        const data = await getInvoiceByFilter(pageNumber, pageSize, date);

        //log
        // console.log(`Revenue list: ${JSON.stringify(data, null, 2)}`);

        setRevenueList(data?.result?.list);
        setTotalPage(data?.result?.totalPage || 1);
        // window.location.reload();
      } catch (error) {
        //log
        console.log(`Error: ${JSON.stringify(error, null, 2)}`);

        if (error?.response) {
          const { status, message } = error?.response?.data;
          if (status == "404") {
            setRevenueList([]);
            setTotalPage(1);
          } else if (status == "400") {
            setRevenueList([]);
            setTotalPage(1);
          } else {
            alert(`Status: ${status}\nMessage: ${message}`);
          }
        } else {
          alert("Oops... Something went wrong.");
        }
      }
    };

    fetchData();
  }, [pageNumber, formSubmit]);

  const today = new Date();

  const maxDate = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate()
  )
    .toISOString()
    .substr(0, 10);

  return (
    <>
      <Header />
      <div className="Good_container">
        <div className="Good_container-content p-5 my-5">
          <h3>Revenue</h3>
          <div className="mt-4 container">
            <form
              onSubmit={handleFormSubmit}
              id="edit-user-update-form"
              method="post"
            >
              <div className="mb-3 d-flex justify-content-start align-items-center">
                <label htmlFor="date" className="col-form-label">
                  Date :
                </label>
                <input
                  type="date"
                  className="form-control mx-4"
                  id="date"
                  name="date"
                  value={date || ""}
                  onChange={handleDateChange}
                  max={maxDate}
                  style={{ width: "250px" }}
                />
                <button type="submit" className="btn btn-primary">
                  Filter
                </button>
              </div>
            </form>
            <RevenueList revenues={revenueList} />
          </div>
          <PaginationNav
            page={pageNumber}
            totalPage={totalPage}
            handlePageClick={handlePageNumberClick}
          />
        </div>
      </div>
      <Footer />
    </>
  );
};
