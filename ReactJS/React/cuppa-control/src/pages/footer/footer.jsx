import "./footer.css";

export const Footer = () => {
  return (
    <footer className="Footer_bg-custom text-light mt-2 d-flex justify-content-center align-items-center">
      <p className="m-0">Copyright By CuppaControl Team</p>
    </footer>
  );
};
