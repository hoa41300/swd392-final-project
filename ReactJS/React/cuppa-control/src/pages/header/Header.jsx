import "./Header.css";
import defaultAvatar from "assets/images/avatar/default_avatar.jpg";
import menu from "assets/images/icon/Menu/Menu.png";
import good from "assets/images/icon/Good/Good.png";
import arrowRight from "assets/images/icon/Arrow_Right/Arrow_Right.png";
import leaveRequest from "assets/images/icon/Leave_Request/Text_Documet.png";
import importReport from "assets/images/icon/Import_Report/Archive.png";
import account from "assets/images/icon/Account/User.png";
import logoutImg from "assets/images/icon/Logout/Logout.png";
import { NavLink, Outlet, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setAuthentication } from "_store/slices/auth.slice";
// import { selectAuthUser } from "_store/selectors";

export const Header = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const { isAuthenticated } = useSelector(selectAuthUser);
  const user = JSON.parse(localStorage.getItem("user"));

  if (user == null) {
    return <Outlet />;
  } else {
    const user = JSON.parse(localStorage.getItem("user"));
    // console.log("Header component:");
    // console.log(JSON.stringify(user, null, 2));
    const name = user?.fullName || "Anonymous User";
    const avatarUrl = user?.avatarUrl || account;

    const handleLogout = () => {
      localStorage.clear();
      dispatch(setAuthentication(false));
      navigate("/login");
    };

    return (
      <>
        <nav className="navbar navbar-expand-lg  navbar-dark py-2 fixed-top Header_bg-custom">
          <div className="Header_container">
            <NavLink className="navbar-brand Header_brand">
              Cuppa Control
            </NavLink>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navmenu"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navmenu">
              <ul
                className="navbar-nav mx-auto d-flex"
                style={{ alignItems: "center" }}
              >
                {(user?.role === "Admin" || user?.role === "ShopOwner") && (
                  <li className="nav-item dropdown d-block mx-3 ">
                    <NavLink
                      className="nav-link dropdown-toggle px-0"
                      data-bs-toggle="dropdown"
                      role="button"
                      data-bs-target="#dropdownProduct"
                      aria-expanded="false"
                    >
                      Product
                    </NavLink>
                    <ul
                      className="dropdown-menu Header_dropdown"
                      id="dropdownProduct"
                    >
                      <HeaderDropdown
                        name={"Menu"}
                        url="/menu"
                        mainIconSrc={menu}
                        mainIconAlt={"Menu icon"}
                        arrowSrc={arrowRight}
                        arrowIconAlt={"Arrow right icon"}
                      />

                      <HeaderDropdown
                        name={"Good"}
                        url="/good"
                        mainIconSrc={good}
                        mainIconAlt={"Good icon"}
                        arrowSrc={arrowRight}
                        arrowIconAlt={"Arrow right icon"}
                      />
                    </ul>
                  </li>
                )}

                {/* <li className="nav-item dropdown d-block mx-3">
                  <NavLink
                    className="nav-link dropdown-toggle Header_normal px-0"
                    data-bs-toggle="dropdown"
                    role="button"
                    data-bs-target="#dropdownRequest"
                    aria-expanded="false"
                  >
                    Request
                  </NavLink>
                  <ul
                    className="dropdown-menu Header_dropdown"
                    id="dropdownRequest"
                  >
                    <HeaderDropdown
                      name={"Leave Request"}
                      url="#"
                      mainIconSrc={leaveRequest}
                      mainIconAlt={"Leave request icon"}
                      arrowSrc={arrowRight}
                      arrowIconAlt={"Arrow right icon"}
                    />

                    <HeaderDropdown
                      name={"Import Report"}
                      url="#"
                      mainIconSrc={importReport}
                      mainIconAlt={"Import report icon"}
                      arrowSrc={arrowRight}
                      arrowIconAlt={"Arrow right icon"}
                    />
                  </ul>
                </li> */}

                <li className="nav-item d-block mx-3">
                  <NavLink className="nav-link Header_normal px-0"
                  to="/worksheet">
                    Worksheet
                  </NavLink>
                </li>

                <li className="nav-item d-block mx-3">
                  <NavLink className="nav-link Header_normal px-0">
                    Salary
                  </NavLink>
                </li>
                {(user?.role === "Admin" || user?.role === "ShopOwner") && (
                  <li className="nav-item d-block mx-3">
                    <NavLink
                      className="nav-link Header_normal px-0"
                      to="/revenue"
                    >
                      Revenue
                    </NavLink>
                  </li>
                )}

                {(user?.role === "Admin" || user?.role === "ShopOwner") && (
                  <li className="nav-item d-block mx-3">
                    <NavLink className="nav-link Header_normal px-0" to="/user">
                      User
                    </NavLink>
                  </li>
                )}
              </ul>

              <ul
                className="navbar-nav d-flex"
                style={{ alignItems: "center" }}
              >
                <li className="nav-item d-block mx-3">
                  <img
                    //avatar url here
                    src={avatarUrl}
                    // src="https://manga-reader-clone.s3.amazonaws.com/avatar_Kim_Da_Mi.jpg"
                    alt="Avatar"
                    className="rounded-circle"
                    style={{ width: 50, height: 50 }}
                  />
                </li>

                <li className="nav-item dropdown d-block mx-1">
                  <NavLink
                    className="nav-link dropdown-toggle Header_normal px-0"
                    data-bs-toggle="dropdown"
                    role="button"
                    data-bs-target="#dropdownAccount"
                    aria-expanded="false"
                  >
                    {name}
                  </NavLink>
                  <ul
                    className="dropdown-menu Header_dropdown dropdown-menu-end mt-3"
                    id="dropdownAccount"
                  >
                    <HeaderDropdown
                      name={"Account"}
                      url="/account"
                      mainIconSrc={account || defaultAvatar}
                      mainIconAlt={"User icon"}
                      arrowSrc={arrowRight}
                      arrowIconAlt={"Arrow right icon"}
                    />

                    <li className="dropdown-item Header_dropdown-item mb-1 ">
                      <button
                        className="Header_link Header_button"
                        onClick={handleLogout}
                      >
                        <div className="Header_link">
                          <img
                            src={logoutImg}
                            alt="Log out icon"
                            className="Header_icon"
                          />
                          Log out
                        </div>
                        <img
                          src={arrowRight}
                          alt="Arrow icon"
                          className="Header_icon"
                        />
                      </button>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <Outlet />
      </>
    );
  }
};

export const HeaderDropdown = (props) => {
  const url = props.url;
  const name = props.name;
  const mainIconSrc = props.mainIconSrc;
  const mainIconAlt = props.mainIconAlt;

  const arrowSrc = props.arrowSrc;
  const arrowIconAlt = props.arrowIconAlt;

  return (
    <li className="dropdown-item Header_dropdown-item mb-1 ">
      <NavLink className="Header_link" to={url}>
        <div className="Header_link">
          <img src={mainIconSrc} alt={mainIconAlt} className="Header_icon" />
          {name}
        </div>
        <img src={arrowSrc} alt={arrowIconAlt} className="Header_icon" />
      </NavLink>
    </li>
  );
};
