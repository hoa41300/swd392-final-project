use master
go

USE [7Store]
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [FullName], [Password], [StartToWork], [Phone], [Role], [Address], [Note], [Email], [isActive], [ContractExpiration], [Avatar], [Gender], [DateOfBirth], [CreateDate], [CofficientsSalary]) VALUES (1, N'Admin', N'96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'0123456789', N'1', N'HCM', N'admin', N'admin@7Store.com', 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'https://randomuser.me/api/portraits/men/66.jpg', 1, CAST(N'2001-12-02T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1.2)
INSERT [dbo].[Users] ([Id], [FullName], [Password], [StartToWork], [Phone], [Role], [Address], [Note], [Email], [isActive], [ContractExpiration], [Avatar], [Gender], [DateOfBirth], [CreateDate], [CofficientsSalary]) VALUES (2, N'Manager', N'96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'0123456789', N'2', N'HCM', N'manager', N'manager1@7Store.com', 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'https://randomuser.me/api/portraits/men/1.jpg', 0, CAST(N'2001-12-02T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 2)
INSERT [dbo].[Users] ([Id], [FullName], [Password], [StartToWork], [Phone], [Role], [Address], [Note], [Email], [isActive], [ContractExpiration], [Avatar], [Gender], [DateOfBirth], [CreateDate], [CofficientsSalary]) VALUES (3, N'Tran Nhut Hao', N'96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'0123456789', N'2', N'HCM', N'manager', N'manager2@7Store.com', 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'https://randomuser.me/api/portraits/men/3.jpg', 1, CAST(N'2001-12-02T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 2)
INSERT [dbo].[Users] ([Id], [FullName], [Password], [StartToWork], [Phone], [Role], [Address], [Note], [Email], [isActive], [ContractExpiration], [Avatar], [Gender], [DateOfBirth], [CreateDate], [CofficientsSalary]) VALUES (4, N'Emloyee', N'96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'0123456789', N'3', N'BT', N'employee', N'employees1@7Store.com', 0, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'https://randomuser.me/api/portraits/men/1.jpg', 1, CAST(N'2001-12-02T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1.3)
INSERT [dbo].[Users] ([Id], [FullName], [Password], [StartToWork], [Phone], [Role], [Address], [Note], [Email], [isActive], [ContractExpiration], [Avatar], [Gender], [DateOfBirth], [CreateDate], [CofficientsSalary]) VALUES (5, N'Tan Thu Hao', N'96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'0123456789', N'3', N'CT', N'employee', N'HaoTT300009@7Store.com', 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'https://randomuser.me/api/portraits/men/93.jpg', 0, CAST(N'2001-12-02T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1.8)
INSERT [dbo].[Users] ([Id], [FullName], [Password], [StartToWork], [Phone], [Role], [Address], [Note], [Email], [isActive], [ContractExpiration], [Avatar], [Gender], [DateOfBirth], [CreateDate], [CofficientsSalary]) VALUES (6, N'Tra Nhu Thao', N'96cae35ce8a9b0244178bf28e4966c2ce1b8385723a96a6b838858cdd6ca0a1e', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'0123456789', N'3', N'CM', N'employee', N'ThaoTN300010@7Store.com', 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'https://randomuser.me/api/portraits/women/48.jpg', 0, CAST(N'2001-12-02T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1.5)
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET IDENTITY_INSERT [dbo].[Punishment] ON 
INSERT [dbo].[Punishment] ([Id], [UserId], [Reason], [Fine], [Note], [IsDeleted], [Date]) VALUES (1, 5, N'Go to work late', 200, N'First', 0, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Punishment] ([Id], [UserId], [Reason], [Fine], [Note], [IsDeleted], [Date]) VALUES (2, 4, N'End WorkSheet early', 100, N'First', 0, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Punishment] ([Id], [UserId], [Reason], [Fine], [Note], [IsDeleted], [Date]) VALUES (3, 4, N'Break the glass', 50, N'First', 1, CAST(N'2023-07-15T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Punishment] ([Id], [UserId], [Reason], [Fine], [Note], [IsDeleted], [Date]) VALUES (4, 6, N'Break the glass', 50, N'Second', 0, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Punishment] ([Id], [UserId], [Reason], [Fine], [Note], [IsDeleted], [Date]) VALUES (5, 5, N'Ruin the food', 50, N'Second', 0, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Punishment] ([Id], [UserId], [Reason], [Fine], [Note], [IsDeleted], [Date]) VALUES (6, 5, N'Ruin the drink', 50, N'Second', 0, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Punishment] OFF
GO

SET IDENTITY_INSERT [dbo].[RequestType] ON 
INSERT [dbo].[RequestType] ([Id], [Name]) VALUES (1, N'Take a day off')
INSERT [dbo].[RequestType] ([Id], [Name]) VALUES (2, N'Change Shifts')
INSERT [dbo].[RequestType] ([Id], [Name]) VALUES (3, N'Salary Advance')
SET IDENTITY_INSERT [dbo].[RequestType] OFF
GO

SET IDENTITY_INSERT [dbo].[Request] ON 
INSERT INTO [dbo].[Request] ([Id], [UserId], [ApprovedDate], [AppovedBy], [Title], [Reason], [DateOff], [RequestDate], [Status], [Note], [RequestTypeID])
VALUES (6, 4, CAST(N'2023-07-24T00:00:00.0000000' AS DateTime2), 3, N'Morning leave', N'Busy with family matters', CAST(N'2023-07-24T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1, N'', 1);

INSERT INTO [dbo].[Request] ([Id], [UserId], [ApprovedDate], [AppovedBy], [Title], [Reason], [DateOff], [RequestDate], [Status], [Note], [RequestTypeID])
VALUES (7, 6, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 9, N'Annual leave day', N'Visiting relatives', CAST(N'2023-07-26T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-22T00:00:00.0000000' AS DateTime2), 1, N'no', 1);

INSERT INTO [dbo].[Request] ([Id], [UserId], [ApprovedDate], [AppovedBy], [Title], [Reason], [DateOff], [RequestDate], [Status], [Note], [RequestTypeID])
VALUES (8, 5, CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), 6, N'Early leave', N'Medical check-up', CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1, N'', 2);
INSERT INTO [dbo].[Request] ([Id], [UserId], [ApprovedDate], [AppovedBy], [Title], [Reason], [DateOff], [RequestDate], [Status], [Note], [RequestTypeID])
VALUES (2, 5, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 2, N'Shift 1 leave', N'Sick', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1, N'', 1);

INSERT INTO [dbo].[Request] ([Id], [UserId], [ApprovedDate], [AppovedBy], [Title], [Reason], [DateOff], [RequestDate], [Status], [Note], [RequestTypeID])
VALUES (4, 6, CAST(N'2023-07-22T00:00:00.0000000' AS DateTime2), 7, N'Full day leave', N'Accompany wife for medical check-up', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-21T00:00:00.0000000' AS DateTime2), 1, N'no', 1);

INSERT INTO [dbo].[Request] ([Id], [UserId], [ApprovedDate], [AppovedBy], [Title], [Reason], [DateOff], [RequestDate], [Status], [Note], [RequestTypeID])
VALUES (5, 4, CAST(N'2023-07-21T00:00:00.0000000' AS DateTime2), 5, N'Lateness', N'Requesting permission for late arrival on 12/7/23 shift 2', CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1, N'Deduct money for late arrival at a rate of 10k/1h', 2);
SET IDENTITY_INSERT [dbo].[Request] OFF
GO


SET IDENTITY_INSERT [dbo].[InventoryCategory] ON 
INSERT [dbo].[InventoryCategory] ([Id], [Name]) VALUES (1, N'Vegetable')
INSERT [dbo].[InventoryCategory] ([Id], [Name]) VALUES (2, N'Fruit')
INSERT [dbo].[InventoryCategory] ([Id], [Name]) VALUES (3, N'Ingredient')
INSERT [dbo].[InventoryCategory] ([Id], [Name]) VALUES (4, N'Bot Drink')

SET IDENTITY_INSERT [dbo].[InventoryCategory] OFF 
Go


SET IDENTITY_INSERT [dbo].[Inventory] ON 
INSERT INTO [dbo].[Inventory] ([ID], [Name], [isInUser], [CategoryId], [Quantity])
VALUES
  (1, 'Apple', 1, 2, 50),
  (2, 'Vera', 1, 1, 75),
  (3, 'Orange', 1, 2, 25),
  (4, 'Black Tea', 1, 3, 20),
  (5, 'Milk', 1, 2, 30),
  (6, 'Sting', 0, 4, 80),
  (7, 'Wine', 1, 4, 10),
  (8, 'Coffee powder', 1, 3, 15),
  (9, 'Package Coffee', 1, 3, 5),
  (10, 'Dried jasmine flowers', 1, 3, 40),
  (11, 'Grape', 1, 2, 15),
  (12, 'Pennywort', 1, 1, 20),
  (13, 'Soy bean', 1, 1, 30),
  (14, 'Watermelon', 0, 1, 5),
  (15, 'Pineapple', 0, 1, 8),
  (16, 'Avocado', 0, 4, 12),
  (17, 'Eggs', 1, 4, 33),
  (18, 'Tapioca pearls', 0, 3, 15),
  (19, 'Sugar', 0, 3, 20),
  (20, 'Bot Water', 0, 4, 18),
  (21, 'Jelly fruit', 1, 3, 10),
  (22, 'Green Apple', 1, 2, 6),
  (23, 'Ginger', 1, 1, 12),
  (24, 'Lotus seeds', 1, 3, 20),
  (25, 'Red Bull', 1, 4, 30),
  (26, 'Grapefruit', 1, 2, 24),
  (27, 'Lemon', 1, 2, 50),
  (28, 'Matcha tea powder', 0, 3, 40),
  (29, 'Carrot', 0, 1, 45),
  (30, 'Wakeup 247', 0, 4, 30);
SET IDENTITY_INSERT [dbo].[Inventory] OFF 
GO

SET IDENTITY_INSERT [dbo].[Profit] ON 
INSERT [dbo].[Profit] ([Id], [TotalSalary], [TotalImport], [ToTalRevenue], [Order], [Month], [Year], [Note]) VALUES (1, 100, 10, 4, 6, 2, 2023, N'None')
INSERT [dbo].[Profit] ([Id], [TotalSalary], [TotalImport], [ToTalRevenue], [Order], [Month], [Year], [Note]) VALUES (2, 25, 12, 10, 2, 7, 2023, N'None')
INSERT [dbo].[Profit] ([Id], [TotalSalary], [TotalImport], [ToTalRevenue], [Order], [Month], [Year], [Note]) VALUES (3, 50, 9, 1, 3, 3, 2023, N'None')
INSERT [dbo].[Profit] ([Id], [TotalSalary], [TotalImport], [ToTalRevenue], [Order], [Month], [Year], [Note]) VALUES (4, 76, 9, 1, 3, 1, 2023, N'None')
INSERT [dbo].[Profit] ([Id], [TotalSalary], [TotalImport], [ToTalRevenue], [Order], [Month], [Year], [Note]) VALUES (5, 123, 9, 1, 3, 5, 2023, N'None')


SET IDENTITY_INSERT [dbo].[Profit] OFF
GO

SET IDENTITY_INSERT [dbo].[Salary] ON 
INSERT [dbo].[Salary] ([Id], [Month], [Year], [CreateDate], [UserId], [ProfitId], [ProfitTotalImport]) VALUES (1, 2, 2023, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 4, 1, 10);
INSERT [dbo].[Salary] ([Id], [Month], [Year], [CreateDate], [UserId], [ProfitId], [ProfitTotalImport]) VALUES (2, 7, 2023, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 5, 2, 50);
INSERT [dbo].[Salary] ([Id], [Month], [Year], [CreateDate], [UserId], [ProfitId], [ProfitTotalImport]) VALUES (3, 3, 2023, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 6, 3, 80);
SET IDENTITY_INSERT [dbo].[Salary] OFF 
GO

SET IDENTITY_INSERT [dbo].[Import] ON 
INSERT [dbo].[Import] ([Id], [UserId], [Date], [Price], [Status], [ProfitID], [ProfitTotalImport], [ImportIMG]) VALUES (1, 2, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1000000, 1, 1, 5, N'abcxyz')
INSERT [dbo].[Import] ([Id], [UserId], [Date], [Price], [Status], [ProfitID], [ProfitTotalImport], [ImportIMG]) VALUES (2, 3, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 20000000, 1, 2, 4, N'abcxyz')
INSERT [dbo].[Import] ([Id], [UserId], [Date], [Price], [Status], [ProfitID], [ProfitTotalImport], [ImportIMG]) VALUES (3, 2, CAST(N'2023-07-24T00:00:00.0000000' AS DateTime2), 1000000, 2, 3, 9, N'abcxyz')
SET IDENTITY_INSERT [dbo].[Import] OFF
GO

SET IDENTITY_INSERT [dbo].[ImportDetails] ON 
INSERT [dbo].[ImportDetails] ([Id], [InventoryId], [ImportId], [Quantity], [Price]) VALUES (1, 1, 1, 100, 10000)
INSERT [dbo].[ImportDetails] ([Id], [InventoryId], [ImportId], [Quantity], [Price]) VALUES (2, 5, 2, 10, 100000)
INSERT [dbo].[ImportDetails] ([Id], [InventoryId], [ImportId], [Quantity], [Price]) VALUES (3, 5, 2, 5, 5000)
INSERT [dbo].[ImportDetails] ([Id], [InventoryId], [ImportId], [Quantity], [Price]) VALUES (4, 5, 2, 4, 500)
SET IDENTITY_INSERT [dbo].[ImportDetails] OFF
GO

SET IDENTITY_INSERT [dbo].[ProductCategory] ON 
INSERT INTO [dbo].[ProductCategory] ([Id], [Name])
VALUES
  (1, 'Fruit'),
  (2, 'Vegetable'),
  (3, 'Coffee'),
  (4, 'Tea'),
  (5, 'Smoothies'),
  (6, 'Alcoholic beverages'),
  (7, 'Soft drinks.');
SET IDENTITY_INSERT [dbo].[ProductCategory] OFF
GO

SET IDENTITY_INSERT [dbo].[Product] ON 
INSERT INTO [dbo].[Product] ([Id], [Name], [Description], [CategoryId], [Status], [OutOfStock], [ProductImg], [Unit], [Price])
VALUES
  (1, 'Apple juice M', 'This is the description for Product 1.', 1, 1, 0, 'https://assets.epicurious.com/photos/62aa36999bb63010f3c51a34/1:1/w_960,c_limit/Homemade_apple-cider-vinegar_CarmenTroesser.jpg', 1, 10.99),
  (2, 'Pennywort juice M', 'This is the description for Product 2.', 2, '0', 0, 'https://runawayrice.com/wp-content/uploads/2018/05/Pennywort-Juice-Nuoc-Rau-Ma-792x1024.jpg', 1, 14.99),
  (3, 'Coffee M', 'This is the description for Product 3.', 3, 1, 0, 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/A_small_cup_of_coffee.JPG/220px-A_small_cup_of_coffee.JPG', 1, 9.99),
  (4, 'Black tea ', 'This is the description for Product 4.', 4, 1, 0, 'https://www.12taste.com/in/wp-content/uploads/2022/01/middle-east-black-tea-2021-08-27-12-00-46-utc-scaled.jpg', 1, 12.99),
  (5, 'Matcha', 'This is the description for Product 5.', 4, 1, 0, 'https://hips.hearstapps.com/hmg-prod/images/matcha-tea-health-benefits-nutritionists-1656538803.jpg?crop=0.670xw:1.00xh;0.146xw,0&resize=980:*', 1, 8.99),
  (6, 'Coffee L', 'This is the description for Product 6.', 3, 1, 0, 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/A_small_cup_of_coffee.JPG/220px-A_small_cup_of_coffee.JPG', 1, 6.99),
  (7, 'Red bull', 'This is the description for Product 7.', 7, 1, 0, 'https://kingfoodmart.com/_next/image?url=https%3A%2F%2Fstorage.googleapis.com%2Fsc_pcm_product%2Fprod%2F2023%2F4%2F28%2F4154-96660.jpg&w=1080&q=75', 1, 11.99),
  (8, 'Milk Tea', 'This is the description for Product 8.', 4, 1, 0, 'https://images.immediate.co.uk/production/volatile/sites/30/2022/06/Bubble-Tea-81ba83b.png?quality=90&webp=true&resize=300,272', 1, 13.99),
  (9, 'Fruit Smoothies', 'This is the description for Product 9.', 5, 1, 0, 'https://www.acouplecooks.com/wp-content/uploads/2020/11/Chia-Seed-Smoothie-004.jpg', 1, 15.99),
  (10, 'Pennywort juice L', 'This is the description for Product 10.', 2, 1, 0, 'https://runawayrice.com/wp-content/uploads/2018/05/Pennywort-Juice-Nuoc-Rau-Ma-792x1024.jpg', 1, 19.99),
  (11, 'Apple juice L', 'This is the description for Product 11.', 1, 1, 0, 'https://assets.epicurious.com/photos/62aa36999bb63010f3c51a34/1:1/w_960,c_limit/Homemade_apple-cider-vinegar_CarmenTroesser.jpg', 1, 9.99),
  (12, 'Pennywort juice XL', 'This is the description for Product 12.', 2, 1, 0, 'https://runawayrice.com/wp-content/uploads/2018/05/Pennywort-Juice-Nuoc-Rau-Ma-792x1024.jpg', 1, 14.99),
  (13, 'Milk coffee M', 'This is the description for Product 13.', 3, 1, 0, 'https://www.thirstmag.com/uploadfiles/images/contents/drinks/article/History-of-milk-coffee-and-diet-with-cappuccino.jpg', 1, 11.99),
  (14, 'Olong Tea', 'This is the description for Product 14.', 4, 1, 0, 'https://cdn.shopify.com/s/files/1/0368/5017/files/yang-gaiwan-set02.jpg?16919735735197567377', 1, 16.99),
  (15, 'Fruit Smoothies 2', 'This is the description for Product 15.', 5, 1, 0, 'https://lilluna.com/wp-content/uploads/2014/02/fruit-smoothie-resize-8-768x1075.jpg', 1, 7.99),
  (16, 'Milk coffee L', 'This is the description for Product 16.', 3, 1, 0, 'https://www.thirstmag.com/uploadfiles/images/contents/drinks/article/History-of-milk-coffee-and-diet-with-cappuccino.jpg', 1, 19.99),
  (17, 'Number One', 'This is the description for Product 17.', 7, 1, 0, 'https://d1v5l30g8wuyvb.cloudfront.net/thp.com.vn/uploads/2017/01/slider-n3.png', 1, 8.99),
  (18, 'Wine', 'This is the description for Product 18.', 6, 1, 0, 'https://images.pexels.com/photos/2702805/pexels-photo-2702805.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', 1, 12.99),
  (19, 'Green vegetable smoothies', 'This is the description for Product 19.', 5, 1, 0, 'https://www.jessicagavin.com/wp-content/uploads/2020/01/green-smoothie-12-600x900.jpg', 1, 14.99),
  (20, 'Milk Tea L', 'This is the description for Product 20.', 4, 1, 0, 'https://images.immediate.co.uk/production/volatile/sites/30/2022/06/Bubble-Tea-81ba83b.png?quality=90&webp=true&resize=300,272', 1, 10.99),
  (21, 'Apple juice XL', 'This is the description for Product 21.', 1, 1, 0, 'https://assets.epicurious.com/photos/62aa36999bb63010f3c51a34/1:1/w_960,c_limit/Homemade_apple-cider-vinegar_CarmenTroesser.jpg', 1, 9.99),
  (22, 'Soymilk M', 'This is the description for Product 22.', 2, 1, 1, 'https://www.wandercooks.com/wp-content/uploads/2023/01/how-to-make-soy-milk-1-683x1024.jpg', 1, 14.99),
  (23, 'Packed coffee 1', 'This is the description for Product 23.', 3, 1, 0, 'https://res.cloudinary.com/pactcoffee/image/fetch/f_auto/q_auto/https://media.pactcoffee.com/images/uploads/legacyproduct/1830/23_06_losnogales_limitededition_image1_1280x1500.jpg?_a=ATFGlAA0', 1, 11.99),
  (24, 'Chrysanthemum tea', 'This is the description for Product 24.', 4, 1, 0, 'https://draxe.com/wp-content/uploads/2020/05/ChrysanthemumTea_header-768x350.jpg', 1, 16.99),
  (25, 'Protein Smoothies', 'This is the description for Product 25.', 5, 1, 0, 'https://joyfoodsunshine.com/wp-content/uploads/2022/08/protein-smoothie-recipe-7.jpg', 1, 7.99),
  (26, 'Packed coffee 2', 'This is the description for Product 26.', 3, 1, 0, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBYVFRgWFRUZGBgYHBgYHBoZGhwcHhoaGBocGR4YGhwcIS4lHB4rHxgaJjgnKy8xNTU1HCQ7QDs0Py40NTEBDAwMEA8QHxISHzEsJCs0NDUxNDQ0NDY0NDQ0NDE0NDQ0PTQ0NDQ0NDQ0NDE0NDQ0NDQ0NDQxNDQ0NDQ0NDQ0NP/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABAECAwUGBwj/xABCEAACAQIEAgYGCAMHBQEAAAABAgADEQQSITFBUQUGImFxkRMycoGhsTNCUmKCssHRBxSSIzRDosLh8BUkU3Px0v/EABkBAQADAQEAAAAAAAAAAAAAAAABAgMEBf/EACQRAQEAAgICAgIDAQEAAAAAAAABAhEDMSEyElEEQRMUcSJh/9oADAMBAAIRAxEAPwD2aIiAiIgIiICIiAiIgIiICIiAiIgUiUJkX/qFE/4qf1r+8i2ROkyJgXFIdnU/iEvFVTsw8xG4hkiW5hK3kisSkQKxEQEREBERAREQEREBERAREQEREBERAREQEREDUdZSRh3t90e4uoPwnCCd71j/ALu/4fzicIqTj/I9p/jp4OlGNpVFlVoDe58pmpIL7nynO3ZFFhKPVYbMR75V18fKY3T/AJaBRcVUG1Rx+Jv3l46RrA6VX/rY/rMATv8AgYyd/wA4+V+0fGNngOlqyuuaoWXMAQ2uhNjO6nmwSw3nolCpmVW5gHzF51/j5W7lc3NjJqxmiInSxIiICIiBSLzTdZcU1OjdCQWYLcbgEEm3LacguPqjaq4/G37zHPlmF1ppjx3KbekRPPP+q1x/it/UZkTpev8A+Vvh+0r/AGcfpb+DL7d/E4odL1wPpD5L+0o3TtcfX/yr+0f2Mfqn8OTtonDp1kxHNT+H9pkHWmt9lD+Fv/1J/sYo/hydpE5norrC71FR1UBtAVvoeGhJnTTTHKZTcUyxuN1VYiJdUiIgc910r5MOB9upSTzcH9JyQM2P8U8aETDKTvXWofZp7/nEjLRE4vyL/wBOrg9WNXA4y8VFmZMGnEDyhsKn2ROdssaqvOWCsvOZxQXlLGoLwAgYDWEqtQcxL2pDkPKUFO/ARsUzCdt0HUzUE7hl/pJHyE4sJ3Tpuq9bssh3BzDwOh+I+M3/AB8tZaY803i6CIidzlIiICIiBoutq3ofiX5GcWqGdZ10r5adJf8AyVcvuFOo/wA1E5cCcX5Hs6uHpjFFjvbzEkUk527tRC98vVRwnO2HvMbqZIa3dMenG0COqGUCHlMxy90qqiBfhLq6E/VZT5EGehzzoidlT6ZohVzVFDFVJF9iQNDbYzp/Hyk3K5+fG3Wm0iQh0rRP+Kn9QHzlf+p0f/NT/rX951fLH7Yav0mRI64ymdqiHwYfvMgrKdmB94k7hp5B/FbGZ8SEB0pIF/E/bPwKeU2nV3G+loIx1ZRkb2l0+IsffOP601S+JrueNR7eAYhfgBLuqfSXoauVj2Hsp7m+q36Hx7pxck+W66sP+dR6IDLWaWlpidpz7as2aWVG05zGrzHiMUiLndlRebEAeZkjKvC57vHvlyrNLU6xUVvbORfcIwUn2mABmTD9PUGNi5QnQZ1ZLnkGYAE+BjV+k6bZmtMnRuM9HVVuF7N7J0P7+6RHeYi0TLV3EWbmnpkTW9A4nPRUncXU/hNvlabKenjdyVwWaulYlIlkKxEQOD/iFigK+CS+71GP9GRfi5kEUe+aH+JnSJbG9n/ACKOWYf2h+LAe6dLhKyuiuuzKGHgRecHP5y26+LxisXBX+sZX+U+8ZMBlrTFqi/yo4sZacKBxPnJV5jqtb/nHYQIzUAOfnApcr+ckWvKqsDAKfj5mRb6zYmQQgN4gtuAJVNZX0IJ4/CZFQX4yRlprxlHMvtyB+EsNPx+ECO+pljoDuAfdMzU/HylCnj5SBat+ZmcPz3lqIJirOF1JsAL37oEbpPpH0YCoueo+iJ82bkouPMCR6HRyg+lxL+kfm3qrf6qLsOVhv3nWROiXzF8S/wBfRPuoNFAHPXbe5bnMuKxAAzVEvoWUEggZRmZRycKGN+NjrpLa14iWDGV0xFVKRR1RWci6st2Vd8pA0HaHidrWvfV6Ppep6HKxNkZCbEAHVzfUasbHex00muxNdFxjdlzUbMLi9reipsCAAe1mXgPHS0kfy9XOWVyGRSWG6F2FxStqxAFrtqdRa2olhKzHCsNScOxtY6mkTxH3OY2A1Fra7dnkBaq4ikDbsut7Hhfge8H5TB0LXJpAHdCUOt/VNlueeXLKXzB6V1R+gPtt8hN7NB1P+gPtt8lm/nocXpHBn7VWIiaKkoZWYq5srHuPygfPXTWINSq7nd3d/wCpif1nQdSektDQc6i7J4bsvuOvvPKc2KRYydhcK1NlddGUgj/nwnDlqx1zxXoZeYneY1rZgCOIB8xeCZztWQPBM1/SHSCUVBa7MxsiL6ztyH6k6ASCuBqV+1iHKruKSEqoH3zu3v07hJk+0trUxtNCA1RAdR2nUHw1Ph5TNTqhhdSCOYII8xORxPR1KpUWnh2VFVXLZVPaIZQbHZstztfUgGE6LQZWo1WRy2U6BWOu4y2vZbmzZh2TcXBlvjPtOnVV6lgTyBPlIOAqZqVM80Q+aiayv0m6o9OtYPkbK40V7gjbg3G3GxItYgbHq2Q+GpnkMh/ASvyAPvka1Fb2m5wOMuGIXukmnhxxEyPRX7IkCMK623lhxK33ksUl5SjIOUCG2IECuO6Z2SWhOEbFnpB3TUdZmth3tpmXJ/WQv+ozd5JreslAthqlhcqucDmUOe3wk49waquMtGm9rrTs7KPrLkYMO+xfNb7sgdKVKZpPVpOoZR202BJBTtL9V7OQDx210ttMIweiutwyZb+7Lec7g0qCpWpilmzqisTYLdFysCx3UsGGgvubaTTFLNh8calWk+gTO7EixdWNIixFvVA0va2ncJMxeNUWREzb61HFIMSSSVV9XYnjltyMg9G4NEdxdmdWb1SikKwHZZy3fstiDyuJZiqj1Caa06NJD6xNRGZwd7NqQTzsT3y2psbfq659EUIysjuGU7qSxYA+5txpyleiD9NyNVyPDKk1vQ7tQSsXI7GUKb3uoBKm430IH4e6bToykUpKG9Y3Yg8CxzEe69vdKZTtD03qZ9Afbb5CdBOc6kH+wb22/Ks6OdvF6Rw5+1ViImiqkw4v1H9lvkZmmDHfRv7LflMijwbCrYgzbNsJrcONptittZ59drZYd+yo7h8plaqACSbAC5PIDjIdJ9B4CQesFa1Bh9sqh9l2Ab/LmlJN3S50Y3pGbE1NNwgP1EGt/E7nv8BJNWsrntsQAydi32jZfSA3uGOnIX5i4wYghBTVjlQaMdhnsMtzwBObXnl42kPpctSs7NmTMFzaZkDMps1vWXMq2O/jLa3UsGGKUq9RldQctSyInaVUe1jlF20Nx7I4CSv52qlQu6KVRQHIuXQOQQLi4Nh2io2BvmN5CwTI+KBVO2Vd2ZvVclkYFdDcrfhM/SOKpoj0zUzEglwqZrsdSXOVgvha4FpazyJPWWopRRubkg91tfmI6j42xeiePbT4Bh8j5zTV62ajR30pqNd9Li59wEhYSq1N1dNGQ3H7HuI098fHxpnb5esgwzSLg8WtRFddmF7cuYPeDcS92mS7JmgtMAaUq11VSzsFUC5LEADvJO0C5TvfvOmugtsPDlz5y+wmlqdYKepRKlQAXLItltvfM5UEd4uJgo9aqR3V1/ob4I7MfcJPxv0nTomMws8w0MYlRcyOGG2h2I4Ebg9xgmVNOQrVzg2emULJq9K3EfY/CNDxsoNt5oquJrVSzZwqvsEIIzMAclyQRzJ2uT4TvelMMlRcrrceRBGxBGoI5icpjOrBJJUq/tEq3vZQQ3jlB75vhlL32IeGSmvquCbMTdwbstiL5jYgk3JA3AtbWRKlejtkVjoAEBNr3vYnfne17kaaSenVup9hB3lw3zQzZ4ToBF1chvuKMq+B4t4aA8pa5Sfsazq90Vc5zfJe4B+sVN1GmhA3JG50Gl79QWlxAHcNuXcBLRqLjaZ3L5XaHoHUX+7t7bflWdLOa6i/3dv/AGN+VZ0s7eL1jhz9qrERNFVJHx/0b+w35TJEj9IfRVPYf8pkUjw7DjabCseyJDwqyZW9WcDtSaB7I8BIHWG/ogRwdPi2X/VJmGPYHhK4vDCojI2zqVvyvx928pLrJeMPSa56D5Re6ZlHMjtL8QJzlF0dK2HLMf7W6AZmCpmAXKo3UX1HLXgJtMF0stOmRXbK9M5GG5J4ZRxvuO63fbSJ0rUBcUkVO0oJewYgmy3XcnYm19zzmmMs8JSuiMNlQekdbAOoVu3YZ1BZEXUeoRc+Utq41WsmFNYsv1xmVFtw9Gi2N9tVG8YUVED3diWGZrXzE7C5GpNzqDrbLta0j4jEMBf+Yqqdhmc2JsDdgLm+uo0tLa8iuDfOt7Zbs112AbQsAOAzZjbhebbD9Hgi813RuZkzObsSSSd+A177Wm+w2iyuVZ3ts+gewrpwuCPfoflNp6SafAaZu+36yaHmOXa06XY7GpSQu2w0AG7MdAq8yTpNbQwjVSKuJO3aSnfsIOZ5t94/AaTA7emxFjqlAe4uwBJPgCB/XzmfEYwNdtGVNct+0QupfL9a3AabX10ltaWR+nOlV9EURCyOp7YsFZTZbJY3NyQL2A7Q11kh8JTpjs0kZCpuN215XvoNOOgHcL6fp/Er6NWZM5U1rZdArqwtU7hbXj60kYrCmpYdpC50CtbIgsWdrbnXKouQLjcbW14gkv0aUAqUexUW5K3JDKWLZG5gA2HK2lpsMHjRVQONL3BU7qw0KnwPnvIfR2KN3psSzUyAGO7IwurHQa6EHnlmLCnJiKiDZ1FQDvBCsffmUfhErfPYndIYgIoJDG7AdhWY6gm9lG2m8hVcaq1VpEG7DfgpObKD3tka3hK9OKHpBS2QOwQsVZrAgi/ZIym9tW05zUH0LZ3KlmVkCkuoKkMqIBlJKKGXUkXN204SccZoXP0w5FTKE7C1zazEr6MsFZidCCQNBzmOviXZWC1DUUNT7aAg3YtnS9IXIACnTUX3md3ysFVUuLkDKzZ/Su3pFVj6qi2u9ieAGtyUKhCZnyjMxIuRYa5VygqPdqOd5fxBFrYZ7g5woY4cZXJUMFZW7Ic5gwI46m54zobCaXDdHsMrZ1FmZuzc5g1juuTlxzd95tbyMvKHf9Qz/YP3VD+RJ085jqH9A/8A7D+RJ087OL1jhz9qrERNFVJH6Q+iqew/5TJEwY/6N/Zb8pijxPAH5CSauxmkLPe6HRV143v922trfGbSjiQ6EgEWNiDzFr2PEd88/X7dqdhT2F8P1maYMJ6g/wCcTMwEzva8a7pbolawuCFe1rkXDDfK44jkRqOE5vE9G4hNDnAGaxANQdrchlGbX7yidm7gWBIF9rkC/hMf8wuYpmGcLny8ct7X85fHOxLiKODqnsj0xBuDYVFFjqRYpbfmZs8B1bJOZxkG+93PxITxuTytNqemwVuqPqodc1lDqXVLggkjVxuJhxXSdQEJkCuHyNbti2QuCtygv42tL7yvQuxNJUYKoAAAAAk3CDszXFyztc3INuH2QeHjNjhhoZSs6kYZ9T7pJDSBg27Te6TLyl7WnTU9CJnpVCSQXeoCeIDM1vLNK08VTqEUagyVFJVbfaXQsjeHA8DqCDKdBHKatM7o7n3MxYf5SvnIPSlF0ro9NMxzs+4AAdFQgk7eoT/8mneViWubpBilSmHVWAqLc6B0GYdkHZjYC3dN/isaiDKEuzAA+lJVeyPV7Vy4FzooI3mrr4RBWUvYs4YFUYG+ZsqsxYXXQ2LAakXGpMyYvpFlJWk2GQnd/S527iSy3PvBlrNpZuhq59OSfRgVEsppaoxQ3IvYEMAdjJrn/uk7qT397rb/AJ3TR9HUQuIRhWRw2Znyvclwj9ojhcMfKbno5s71KvBiEX2UuCfedPwyMpq7/wDCtjiKQdACTYMCQPrW1ynuvbbl4yOKFlKALk7W/avm11B09YnTlaSUew1lj1GtoNbeF+WsziFi0rC17CwFh2QLEnS229vcJZ6NRwHE7cyW3PeSffKZXNsxA5gC/LS59/DlCYcjUsW8T8gNpKV5YShf3Shpd8Kmthckwh3n8Pvoqnt/6R/tOtnLdR8LVSnUzoVVmUqCLG9rMbctF+M6md3F6xw5+1ViImipI+N+jf2W+RkiR8b9G/st8jA+earqGGZiptoeGx7Jtrqbd3OZ1xTKCXDA3JDrpmNgO0pABuAvC/hDUQ1jx211Fu8fqCDD03RbC1j9VtUO2x0F/GxJPGcfh2t9gCSik7ka/wCwkoNIuAP9mnhJEwy7XnTS4nDVDUeyXdnRkqEIVVFC9ntdpbEMeyNzKLTBf+Y9LclyAqAMCrDIEBAuSVVTvlBBPC8kt0ezVCzBPXDh7kuAAAKYFuyL3vY2IJ0uYo4FVpqgDkpls+YqcyDISpYmwtfQaWNpfYgUsLTQFQrkEU6bm4XtizqbasGFxcg2F+NtLqKFgg9EtmDVDmXMb2SzZnJLHtEXtc2G02iYRQc2Vc3Z7RuxIACm5O7WuM29pX0OgBZjYW3yg6W2W0fJLW4csQue+awvcWPDhYW8hNogtIOMcK9ydAAST3ScrAi4NxzEis6tww7beH6yaJCw3rN4frJl5XLtbHpq+k0am4xCi4tlqAfZGz6b22PdY/VnPY/pN6zg51pBb2UHtFNDmzeqxPC3f3mdteaXHdX0e5U5CdctsyXO5C3BQ+yRL4ZT9rOcorRXVmBf1xmIOhOgZgDZsp+rfUEm1hM2Jx6BiLq4B4Ln0ANrsTY62tbQG97nWTR1Ze+hp29/yKE/GTMN1eA1d7j7KXUeFySR+HLL3LH7Gj6P6ONd8wTIB61vq8b7aNa1hw9Y8j2CIFUKosqgADkBpLqdNUUKqhQNgNB5SiOG9UgjmDf4zPLL5IXprLws2PVzo5a9XIzEDKW7Nr6EC2vjO5wvV3DptTDHm3a+B0+Evhw3KbZZ8sx8PPcPhHfREZ/ZBPmRtNvheqNd/Wy0x3m58l0+M75UAFgAB3aS+b48GM78srzZXpzGE6mUV1dmc8vVXyGvxm8wnR9Kl9HTVe8AX895LiazDGdRlcre6rERLIIiICR8b9G/sN+UyRLHW4IPEEecD54z6gc5ssPU0seM3nTPUo4UlkUvT+3uyjk3Lx28JpjTE4c5cbquyWZTcT8IoCKFGgFgANgNhaSLSNh2sgHd+suzEzKzy0jMSJhqVgBe3kCT5CAl5eqwnSMaztshAtftWHOwtv8A/YRX+tbuy3/WSmUSZheia9X1KbEcyLDzawkyW9RFsnbl+lNyDfUC+Xe3d3zHgHKEZGDUySCeIIUm+UDQ6G/Dsyb1qwL0HKvlzZFbe47VxY3sOHh3zTUTd1P0ZAHcDwAzDYEjwM1mNk1VN76dBhXBYkEEFbgjUHUbScBNN0MHzuXABIvoAAdu1pob8/2m4mWU8rTpixOJCAE3JJyqqi7Mx4Ae492kh4vpLJTDhDmJyhGKhri5OoJHqqTvy5y/pVL5GDFXVjksufMSrArl0uMt9bi1prcFTpZkQLnADP2lAVWdmBsgUgMDTK6kAczeTjJrazL0h0o6sBTZbMlN1JQsGLsVAZ7gIug1POWpiHZyFqOzCpUVksCq0wGtfTQ3y21ubylGsuTsBQrFUYIhd6adtgHBzXObS2Wy3Ohlz0arhs75exlAJIuzZxchWyg2y37J12tLakEOhhDkTOfRspzlmsjAimwDauWazEXva+ulpvOjUK0kViCQNSL2OvC4BtICdHAk5X7BULotjxa4C2S3aH1T5zY0UCqF3sO75DSRldodN1JP/c/gb5rPQ5531H/vP4G+az0SdXB6uTl9lYiJsyIiICIiAiIgIiIFCJx3WHqcrgvh7I+5TZT4fZPw8J2MSuWMymqnHK43cePDDMgCupVhoQRYgiZaGGZjZFZzyUFvlPU8RgadSxemrEbFlBmWnTCiyqFA4AADyEw/r+fNbfz+OnAYTqtiH9YKg+8dfJb/ABtN1hup1MWNSozdyjKP1M6mJpOHGfpS8uV/aBhOiaNP1Kag8yLnzOsnxE0k0z3t5L/EU/8Aen2E/wBU5inhlYWFhbgfV15W1U96kd950X8THtjbDUmmhsBfiw15bTnsOrDW05c/GVdeHrEnoegUdgQdri+o1toCN9RyG+029ryHg3uT4ScLTDK+WkYMRhQ+U3ZSpuGU2IuLEagixBli4YJ6gC9kKSbsezcruddWYm+95INUCWmoTtIlqy0UzuSd78hx079/gJZkUbAX8+JO57yT75ayOSbsAOFhc+JvptKDCgkElja27G2ncNJIuLCPScoNMcZQIL6AkwhvupWuKXuV7+X+89Lnn/VHoqutdahQogDXLC1wVIsAddyD7p6BOzhlmPlyctly8KxETZkREQEREBERAREQEREBERAREQEREDUdN9BUsSvaFmAsGG47jzHdPOOmehKuHbtLdT6rgdk/se4z12Yq1JXUqyhgdCCLg+4zPPjmX+r453F4vhfWPhJBBnV9KdTyjM+H7Snemd1P3SdxrsdfGa/C9XsQ+1MqOb9kfHX4Tky48plrTpxzx1vbTBJflnYYXqZxqVfcg/1N+03WF6u4dNqYY827XwOnwlseDK9+EXnxnTzzD4Zn0RGY/dBPnbabjC9Uq7+tlQfeNz5L+875UAFgAByGkvm2PBjO/LLLmyvTmMJ1Norq7M55eqvkNfjN3hOjqVL1Kar3ga+e8mRNZhjOoyuVvdViIlkEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERED//Z', 1, 19.99),
  (27, 'Sting gold', 'This is the description for Product 27.', 7, 1, 0, 'https://cdn.tgdd.vn/Products/Images/3226/91595/bhx/nuoc-tang-luc-sting-vang-lon-cao-330ml-1511201815591.jpg', 1, 8.99),
  (28, 'Lotus seed tea', 'This is the description for Product 28.', 4, 1, 0, 'https://delightfulplate.com/wp-content/uploads/2020/05/Lotus-Seed-dessert-soup-with-longan-Che-hat-sen-nhan-nhuc-768x1152.jpg', 1, 12.99),
  (29, 'Filtered coffee XL', 'This is the description for Product 29.', 3, 1, 0, 'https://mojonow.blob.core.windows.net/cmr/p13647/opt/f992b86f556d4c2ba432e1a836c3287b_1.jpg', 1, 14.99),
  (30, 'Soymilk L', 'This is the description for Product 30.', 2, 1, 0, 'https://www.wandercooks.com/wp-content/uploads/2023/01/how-to-make-soy-milk-1-683x1024.jpg', 1, 10.99),
  (31, 'Grape juice L', 'This is the description for Product 31.', 1, 1, 0, 'https://simplelivingrecipes.com/wp-content/uploads/2023/02/Grape-juice-recipe-720x1080.jpg', 1, 9.99),
  (32, 'Soymilk XL', 'This is the description for Product 32.', 2, 1, 0, 'https://www.wandercooks.com/wp-content/uploads/2023/01/how-to-make-soy-milk-1-683x1024.jpg', 1, 14.99),
  (33, 'Filtered coffee M', 'This is the description for Product 33.', 3, 1, 0, 'https://mojonow.blob.core.windows.net/cmr/p13647/opt/f992b86f556d4c2ba432e1a836c3287b_1.jpg', 1, 11.99),
  (34, 'Lychee tea', 'This is the description for Product 34.', 4, 1, 0, 'https://cdn.foodaciously.com/static/recipes/6a8cad49-6315-416d-995f-9c6861996228/lychee-iced-tea-b5557cf2e58ecc5f0615a1812e98bc0b-720-q80.webp', 1, 16.99),
  (35, 'Smoothies for weight loss', 'This is the description for Product 35.', 5, 1, 0, 'https://simplegreensmoothies.com/wp-content/uploads/vanilla-matcha-smoothie-fat-burning-recipe-400x267.jpg', 1, 7.99),
  (36, 'Filtered coffee L', 'This is the description for Product 36.', 3, 1, 0, 'https://mojonow.blob.core.windows.net/cmr/p13647/opt/f992b86f556d4c2ba432e1a836c3287b_1.jpg', 1, 19.99),
  (37, 'Worrior Red', 'This is the description for Product 37.', 7, 1, 0, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSERUTEhMVFhUWFx0bGBUYGBcZGBcbGhgXFxkbFxkgHSgiGholGxUYITEhJSkrLi4uHR8zODMtNygvLisBCgoKDg0OGxAQGy4mICItLS0vMi0tLS0uMjcwLS0uLTAtLS0wNSstLS0vLS8tLy0tLS0tLS0tLzItLS0tLS8tLf/AABEIANIA8AMBIgACEQEDEQH/xAAcAAEAAwEBAQEBAAAAAAAAAAAABQYHBAMBCAL/xABOEAABAwIDBAYFBgoGCgMAAAABAAIDBBEFEiEGEzFBByJRYXGRFDJCUoEjYqGxwdEVM0Nyc4Kys9LwJTSToqPhJFNldISSlMPi8TVEY//EABoBAQADAQEBAAAAAAAAAAAAAAADBAUBAgb/xAAzEQACAQMCAwUHBAIDAAAAAAAAAQIDESEEMRJBUQUTYXGBFCJCkaHB8DKx0eFSkhVTsv/aAAwDAQACEQMRAD8A3FERAEREAREQBERAEREBC7VYwykg3shcG5g3qi5u7QWCgqfaWjmZd88jTfk6Vnhw1UB0s4ud42DgxgDj3ucCPoH7Sz6j9ILrsb1e/wDnioJVXxNLZG1Q7NpvTxqVeLil+lRtt1d1+L6a8dpaKAnJU6H/AFhlf+0DZecu3tKB/WIfJ33LMcQnIbZ7W/8AMq76OJTxsPHqtXFWe72PU+yqaahFtyfLH8Gs4jt/BI0sZNA+/LM9v0tsfpXRs5tJTFjt+9jLHq7t8sgI7yb6rJnxQRN1dYO79XL7QVEj9IwCPMNXFWlLKWD3U7Jo0UoTm3Ufwxtjzw3+1/JXNsG1WH8BNIf1Jv4F44xt1TU8Ej43uLw05A5ji0ut1c17aXWbQyvYLub9hd4KExozygjJ1e7WwXY1JSeNiLUdn0dPC0uKU3slbHnh/wB+WSx0HS7VSS/KPhDeyKKQHzc/xWrbEbQCtZI5ubqENOa3Ei+mp0X5ma9kZtzWndDWKmKrEQ1ZO3UdhaCWu+gj4qRyz4FOOmjKm0sySu84sjdERFIUQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIDC+liQnEjGPcjJ8l/DDlYPBe3SoQ3EnnmWR/UvtBh0ksT5GtLmsALvBVZfqPqNLK1KLb95pei2Xz/d9Sm4s1pk67so/vO/y715VVU1jbNyk+y1vq/nOV72abThtZUVLM0UbWtOmrQ4tbmA7Rx87Km7TbPilqCHn5B/WjkHquB1b1uTXDmo3G6TlsWKeo7uc6VG3Hi7fR5undppJpvCtdO0slXkpXTuzE6e0/wCwK47OMaxuVv8APznLjwzB56wndWihZxlIA07ifVb9KnKTZ2SnaJWvFRA/hK0hw8Mw59xR8Uld7Ci6GnqOEHxVJbydrvfKV7259WveV1k9pYCXcbjt5fBf2HNAsP8A2uzC6F9VI2KIaH1jyA5k9ymNrG05pA+naLQTboyAC8pDGuLr8xcm30aFd4XJNnO/p6epGN7yldt+Sbfpiy3u/BGG41GPSn9mZaR0Xjd19MHes9zv3byqbieESxSMnkbZsjjYH2b8DfmrX0bkuxSlfZ2XePDT7JtC8kDv6wJ8QpMy4VyVjP4oUYVp/FNzSXS97v0yuqeHZ3S/RKIismAEREAREQBERAEREAREQBERAEREAREQBERAYf0owF2K/N3bL+Sn8JlmipIfRwDLLO5wYeEjI4nEsPjw8SFDdK8uWuNuJYy32lWalYI/RAPyEUD/APqJwx5Hg0fSoIXc3c29W6cNFCMd5KN/9b2+l/XxIz8FNmpqw0lzHVQF8dvWjlYb7p/Y8OOncPiYLAa91RQhzKcVcDfx1K05ailfzdTe9E/Vwj5agaCw0XDoo4sQrI2ANMjYZy0aZnP3sb3AeMQvbnx4qj7ZbK1FFUHEcLdlc4newCxzk9Z2Rn5QGxJYNQbluvCVQSVuRl1NTUnUVVv3sZWHhWv52WevqZ/t3tY6UCkgjkp6eO143jLLI6wu6YfU34nkBptBRx0NJDUx23U7Imz0huWzucxvXhb7M3E6aOtrY6rG9tdp5MRlbNLHExzWBnyYd1gCSCbk9ui02Ha9hp6MsDPSKfJkizCYOJjMR3mSwYdQWgOLr8QF22LETlJvibzvfnfr1LfSQvfGQIXUVHa8j5DapmaBct43jba9zxtey8RBG6jbPON1T7x9Q8WtZgyxwRtHa5gaNPrIX9YO2pxQh1VljpwA4xA9aYXu244thJHi63EjUWbHIWSSUsTg0gSmTIbWtHFJlNuwPdGfgFxxTVuRJHUVI1O9v72cvfKt884MG2jqZp/TWztyPaIpo4R+Rj1ysI5ENewnvJVp6H8CcaoTuJ3cV8gvpvHssbD83j+qo3F4N7idRb/7D6iC/wCiipwCe4Pjd5rUtiMObBFHGzg0cebjbVx7yVyzcr8j1GpBUHFr3r48Fz+d3YuCIi9lcIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAxXpSZfED2ZI79/WB+y6nauoLquSla3KBS7qE3uS5rBPG6/M5hooTpXf/p9u2Nl/C6ttbLCx++FM1xio21DJC97XHd2aGEAW4Djr4KGnmUrmvropaajw9E352x8lhepSemqYiagrYXFpdGS2RpsdCyRuvZaQ+OqzrHNoqmqk3k0ric4eA0lrWODWtzMbfqmzRw7+1a/jVRSVZpqKelIi9D9Ma5kzrwtDXjK0ZesRwsTbyVNwalwypnhhbhlSzfhzo5JKqQBzWhxLh2jqEaX1UxkGXVS3GlFBhdINwxprJYhYk53sztF33P4samwFrn4qlRx4VOWM9Bq6dsr92yo3xewSaC2osbEi44i6sEVLh9NPPB+DqiQwZTJI2oeeq5rXNcRaw0e3jpdARVJXSRODmPc0jLwJ1DC0tB7QC0adyvnRzO+oramrmNyI7E62GdwNm3OgAjOi/p+CYdG+kYKd8rKppc2Z0z2lrbZjdrRyaR4/SvTAMagax8EVCY4qiKUxOdI4+kbpjswebXbdocLgmyAodLiB/ClGwMBMrJJXg8WGqc+c2Pa1mT6e1bZgujmgfzoVkGCYj6XXbz0OKCd8QkilDi8OjzNjJI7Q1thoLWK17Ax179+nkgLEiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgMY6Uh/SJ/RN+pymseltBGffw0t/vxfxKw1+zkc1bJPM0ObuWxtae/Nnd42IA7NVDbbULYvQYmXyaxam5sXxcTz4KKEGpNmjqNVCenhSW8beWxVcPOeoJ/1eCOb4HO5v2Fcmy39awP8A3Ob/ALq7tmzmmxW41hppIv8AFqn/AOXwUHsLVvkrsKa6JzGxQTNY8/lRaUuc3TgD1VKZxFQj+j6Mf7Wd9TFcWf13G/8AdGfuYbKk4dRNhjpa1l96cRdEQbOZl6p0aRo7U6jXyVtrcLZNX4o52bNDTxyMsfabDF6w9oaICwuFm4N+gk+mELjwkdXCf0Vb+7kUlXVTpXYTI62Z0MhNgAL7ocAOHgo7CPVwr9DW/sSICtbFEPq6QAg5cNs62uUmdxsew9y2TCvxjQOX3LHNgKcRVVPuxYT4eJZBfR0gmLQ7XgbaLY8EbZwJ4n7kBYEREAREQBERAEREAREQBERAEREAREQBERAEREBHVr/lAFB7aUjnuoy1j35Khpdla52Vtxcmw0GgU7VD5QHuC8MYxqOmY1zw45jYBtr6cTqRojdj3TpyqSUIK7ZnOE0hjkx5xY9ofvC0uY5ocP8ASTdpIAcLu5do7VXtlJfl8GeGvyR08zXvyuytLjKG5nWsL6eYWg4ltNBVQyR7qqLHjdvLGDg6wLQQ/QkG3xXpT7VwCNojhqAwBrWhsXVAsMoFnW4EW8QvHew6ll9n6pb02YzvrYdTNcC1wxQvyuBByljCDYjhqroJbV2Mkhwa6lDWmxsS2OJpAPAnMbfAqX2oxulqoJYJW1FjYOO7s6N1wWO9bRwNiL8VN4JtbA5uTJM9xGoEfrWu11m34XBH0J3kep59h1P/AFv5fn55kHE0v/BAaHG0LwSASG52ZG5iBYXIXHgsUzjTsNPM00UFVvCWGznSMkDWstq5xLhpz1tdXLC8bgp4WxsiqN20uDPk9ALudlvm1y6jwC95trIwD8jUdW9+o0Wtmve7tPUd5FO8j1PX/H6n/B/QzDY+B7K6jZJHIwsw5zHCRjmatqDcagX0IPgR2rWcMN5W9w+xUSn2jp6qtYAx7ZGh+RzsuoLes02J0IAPi0K+YU2z29p+5djJSV0yCtQqUZcNSNmTqIi9EQREQBERAEREAREQBERAEREAREQBERAEREBHVgvKB3Lg2owQ1UbA1wa5hNs17EEa8PAKSqzZ4XS0aLjV1Yko1ZUpqpDdGdxbKVMQDWuhdleJA7M+4Iyf/mfcHAhehwer6pyQ3a5rhZ9gS1sYOhjJAO7vYEceauNW+xXOGXUfcxLku06sneSi/R/Z/jM/rsBqiwteyInKQCH2AJjZGTYNt+TaQBa2vIrtwbZ6rMjpX7sh0e7LRIQfVAJBymxLgXcOLirDis2UacfqUhgQJbx+K53EfH89BHtOrG9lHps+fT3sbLYjJMIrHZriDW/tP5mY8clyPlnaE8r818rcGq5WPa51OA8WdrIb6yOHs8QZAb/MHerWTZeZbdeu6XVnP+RqbqMfl/fh87PkZVgWxrqas30kjHZM1g0nsIubtGliVfcDqmSvDo3tc0XF2kEX7NFn3SFtFlldTxHXXO4ctDdg+3y7V0dHOJNjnEDXl4lkFiQQcrYnucbHvDQo4TjGXBHb7luvpq+ooPVVn73JWsrX32xvdW3Wc8tcREVgxQiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiICOqxeUeC7SdFy1Zs8LpaNEBwTM1uV4TydiqWNdIMbKpsMTRJHnDZJMwGpNjux7QHM8PAWJisQ6UadlYyCOMyx5wx8wdYAk5fk2264B4m4vyuuJ32PU4SgryVi21cN9TwXDiG1bKdhjis6bn7sfj2u+b599d272zy5oKZ2Z3B0o4DtEZ7fneXaqFh2JcWtN3+073fDtKglWcnww+Zsabs+FGn7TrMR5R5y9Pt0y7Le9QbWy0+8Odz3vbpmJLWuuOuR2gX08FxYLjdTEZpM7iZW2zON+tcHOB7wFwPHuUTR0ebrO4fX/AJLukINwOPurkp8C4YsuabR+1Vvaa8Uk7cMevS/VfvyxvUKp/wAq5zj2kknTtJJVj6N6nNitN84v8t09ULHa4OkMbfVB6x94/crv0NRZsRikdyzAf2bl4o0+BcUh2jr+/nLT0cpJtvyTb9Le7fm3jlf9FIiK4fMhERAEREAREQBERAEREAREQBERAEREAREQGedIm1rqN5Y228exu609UkuD3HtAAFu8qqUmMzMpZIWvNpXZnuubnTrC/wA7medvFfelnD3S4k0g2DYGan8568qDCJJiyFhbmdoSSbcLknTgACVFJ/Dc09PQlb2h03wpY6NpWbfyb/m2c/xqcl5a29jyHP4L2osNDDmf6/Icmf8AktErOjN8JDxURuc57WZrFmTO4NFhc31I71yYr0W1r+q2eAM7PlLu8erwUc1OXuRwubLelq6WjH2uvJTn8MVy874TW/hhq8rMzHG8V9mM6e0/3vD71O7GYblGaQa8mfefsU9Q9Fc7Bnc+Jz7kAXfplcW6dXjov7lwSelkbE9uknqOBzZ9QLDmCCRoe0Ly1KK4YKy5smo1KOpqe0aqalP4YLZefJ9frK726Jp+Q81E19RYENPLU/crVtFsdUU1O6Zzo3NbbMGFxdYkDmALXIXhV7CStdTM3rHekkhrg11mWZvLnt6tz8FLTp23Kev7SlVdoP1+y+7MeEJfPlHM+XetP6NXBuJ0sLPVGcn+yfx+Ovkud2xIpTWGSXNuIxIXBlg/qucGgE6C4tzXr0OsPp8cr+Mme39m7+H6FxtuV3snZeL2FGEKenlTg7zlHik7YjBLiSvteTcU0utnbhR+g0RFOY4REQBERAEREAREQBERAEREAREQBERAEREBjPTHXFlbC1p6xiF+4ZnfWvPZeqndUwiHIH30LgcujSTmtrYi40Xv0r0TXV7Hv4bhoHzevJqvTZ3ZF9RTNmZO2M5iBcEWI4WcDxUcVHidupo1a2qp0abcrRlFpJdFjK8c/wBWsSO3WBel0M8k9OIKiO7wGShzHubazjawcHHq3c0OCjobw7OtvJq2/XuR+XPq8+5f3WbNYhKZIJKgvEYaflJX5ZA7NYjQ82njwsoXFdksY3LaYyR7h7wBEJBlvrILksB4tJ48VHdyukrLKLSp0tPGFSVRSqKUZWu3aKXFa/jjyW3NueNLFV4RTNmmfE2TJZ7SA4uzkNbc31cVKvj3+I0lPkfkpo3Tue4dV+rWRtDuZD23PgFnm0GyeMtpWQMkEkMdiI2OjzAtJcMpsHGx4a3U5sfHj88TJt+zK9t25jEHWvzG7uOHBTRWEZVeT7yTve7bw8Zy7WxzNOmpDO2pikkY5kosxrT1oxkDTm784zX71w4GA2hhfNbNSteHE8t218btfAKqYJsxWQSvyOayYtu52b12k65TY36wF+fDtUY6nr6imqZd+YoWue2RjnOBkcdHiwFiSTl42uVziz4E8tMoU0+JOcrWSd3m907Y5rnurH97ZVzJaHe5rOrIYOrzLQd4+w8HkH4KD6N5r4vTxt9Vgf5mJw+hU/Fa4wtDMznECzA4k2HaOwe6Bp5Kz9ElK6PEqYuHrB/7tx171FB8cuLly/kvamk9NQlp45liVRrZL4Y+V/y2D9FIiKwYgREQBERAEREAREQBERAEREAREQBERAEREBi3TM9z8QgibziaT39d/HuCuWy0JGHNDIRMRLfIXAcwcwJ0uL3Ve6U4x6bE7mYQD4Z5Fz0FFXuY51LvbA26smQXt2Zhc2sq/FeTikbq0iho4aicvedrN3xFXslbN3vjn9bt6ORWVLnPLw+CGzBoWgOqRlFu03IPG5PYqpFT7utpd1BVxQkuBEu+LS/K8t1c5wHVHaFVG/hylklkENW50ltdZPVvxPW0AvovGnxTHZC3eOnaGOzAupWA3sRoN3ro48dF7nbeWEnf85/sUtPJpqNK0pTi45urYcXfaPVp32s3a9nor6MMq6ip9Ke8ZA11MOs2IgNdcNBJzkC4FhfMeK7sIjbNRMtI6HePzNcCA4F8jnBg1Gpvayyqtx/EKd75skjnyWuNz62UANLupYWHNfcDxLFKqPcyNkbE17XgCnyZS12duWzLmx4a8guRmpK7Vl4nurpJaerGEJRlUvFqzvbGU7q1r2y3srtK5pW1eNuhr6RrQ47sEyE8HMks2wPMjKT4gLw6TsQbBA2JrQA9zpCBpmN76+LnFx8AobFZ62YNMkU0hb6toSCb2vq1g7FXsZgqZpC6s3geQPXGU25AN9kceXanEpJt7Eq0stPVpxpNSq58Uv8AF7ct887O1rFApKcyTGaTUX0+c77gtF6N3f0lTDmd47/CePtVQm0eeIA+LWtBU90T1Bfi8J5Wkt4bt6hpN1KnFyRpa9U9Do3Rvec93zb3cnztyX83P0SiIrh8qEREAREQBERAEREAREQBERAEREAREQBERAZF0w1gjqoebjELN/Xfqvmze0McdM6GoEmUvztfHYkEtsQQSNPvKjem3/5KD9C3949eVNTWaC4ajkeXio7RhdvmadJ6jWuFGGFBei8X4vZL9ssla/bChe4iPEJoL36phls25k4EaX+Vdr3M90L3k2rp8kjW4vHd2XIXGRpZZwLtcpNi0W81n+K4tA6Q9bza4j7VxMmpiRfdEfmH+FR9818L/PQtLs2jNY1NN/63/wDTNPrca3kD2x18IcX5mSiQ+qQRY9XkQHcxrbgNZDCtomNjbHJiFPvGvDnOEhu5pD9C22mpabdml9LnDMdxgAlsJJ7X9n5oUpsdhxIzyc+DTxPeV2NSVrzwuXUhnoacqio6VubX6nZKK67X+ja5LiZttZtY2NrjFMJ3OFm2vlbqes7QC+o0HGyotbM55c95LnO1JPElfAozFqolpYznoSOfcFG3Ko7G1GFDs2lxPLfzk+SS5fbnnelYpOZZtzHwzau97XT4BX/oowsR4jES4ucA/W1h+LcqPkFMC46vJ8zyA7grV0QSOfi0JcSTlk4/o3L1TziOIr6mXrpRheWpjxVpp4viCe3m93s8875l+i0RFZMIIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAy7pQwV7qmKqylzGR5S4AnIQ5xvYa8HcVTqqcyNyt4dnb4rf5GBwIPNVTEMI1O8gY8H2w2xPi5tnea8cHvOXMvQ1j7mOnfuw+Lh/U/O7t/Vr3WDDKnDI2hxkNz718uXX2RzVXcttxbYSkqDe8zD2MkaQPg8XULUdFMXs1E48Ymu/ZIXKcZK7k73Gur0KrjGhT4YxXq/Py5ZfnyWXYU1pnZmseNgeBdY5QfitKwjd7tmR7nPLXb0O0DbHSx/nl22XLP0St5Vjx/wzv4lJ0WxpjblfWyvHZ6O/+LVealJylcs6HtGGmpODi273VrZxbN03jdW526Z4KqrLurH59vgvrIAxpc7jby8O9WGmwKFmvy7j27vL5XXNiMdM0Wc1/wCvKweTWi6Spu3CtiSlr6PH7RWfFU+FWfDH52z+XbyZJi9QJJL31ubDk0dnitJ6E8ImdWtqN24RMY67yLNJLcoDSePHl2Lqw/Yl8rgaeljbc6yua4tA7Q9+pPgCtkwihEELIwbloALvePMqVJJJIyalaVScpyy3f8/NuR3IiLpEEREAREQBERAEREAREQBERAEREAREQBERAEREB4zwNd6zWnxAKgcSha0nK0DwAC+IgK7JM73j5lRs07ved5lEXQSWEU7Hnrta7h6wB+tXuiw6GP8AFxRs/NY1v1BfEXAdqIiAIiIAiIgCIiAIiIAiIgP/2Q==', 1, 8.99),
  (38, 'Grape juice', 'This is the description for Product 38.', 1, 1, 0, 'https://simplelivingrecipes.com/wp-content/uploads/2023/02/Grape-juice-recipe-720x1080.jpg', 1, 12.99),
  (39, 'Aloe vera juice M', 'This is the description for Product 39.', 2, 1, 0, 'https://blog-images-1.pharmeasy.in/blog/production/wp-content/uploads/2022/06/10110613/19.jpg', 1, 14.99),
  (40, 'Milky coffee M', 'This is the description for Product 40.', 3, 1, 0, 'https://www.thirstmag.com/uploadfiles/images/contents/drinks/article/History-of-milk-coffee-and-diet-with-cappuccino.jpg', 1, 10.99),
  (41, 'Orange Juice', 'This is the description for Product 41.', 1, 1, 0, 'https://yellowchilis.com/wp-content/uploads/2022/02/orange-juice-recipe-720x1080.jpg', 1, 9.99),
  (42, 'Aloe vera juice L', 'This is the description for Product 42.', 2, 1, 0, 'https://blog-images-1.pharmeasy.in/blog/production/wp-content/uploads/2022/06/10110613/19.jpg', 1, 14.99),
  (43, 'Milky coffee L', 'This is the description for Product 43.', 3, 1, 0, 'https://www.thirstmag.com/uploadfiles/images/contents/drinks/article/History-of-milk-coffee-and-diet-with-cappuccino.jpg', 1, 11.99),
  (44, 'Jasmine tea', 'This is the description for Product 44.', 4, 1, 0, 'https://sainthonore.com.vn/media/product/2023/05/75_75/pexels-photo-7138780.jpeg', 1, 16.99),
  (45, 'Functional Smoothies', 'This is the description for Product 45.', 5, 1, 0, 'https://mindbodygreen-res.cloudinary.com/image/upload/c_fill,g_auto,w_480,h_322,q_auto,f_auto,fl_lossy/org/wq7xliybbl1nuuxag.jpg', 1, 7.99),
  (46, 'Milky coffee XL', 'This is the description for Product 46.', 3, 1, 0, 'https://www.thirstmag.com/uploadfiles/images/contents/drinks/article/History-of-milk-coffee-and-diet-with-cappuccino.jpg', 1, 19.99),
  (47, 'Wakeup 247', 'This is the description for Product 47.', 7, 1, 0, 'https://www.vietsanmart.vn/web/image/product.template/1380/image?unique=f98810d', 1, 8.99),
  (48, 'Green tea', 'This is the description for Product 48.', 4, 1, 0, 'https://sainthonore.com.vn/media/product/2021/05/75_75/33072_1620976904.jpg', 1, 12.99)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO

SET IDENTITY_INSERT [dbo].[Voucher] ON 
INSERT [dbo].[Voucher] ([Id], [TypeDiscount], [Value], [isActive], [CreateDate]) VALUES (1, 0, 10, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Voucher] ([Id], [TypeDiscount], [Value], [isActive], [CreateDate]) VALUES (2, 0, 50, 0, CAST(N'2023-07-07T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Voucher] ([Id], [TypeDiscount], [Value], [isActive], [CreateDate]) VALUES (3, 1, 20000, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Voucher] ([Id], [TypeDiscount], [Value], [isActive], [CreateDate]) VALUES (4, 1, 100000, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Voucher] ([Id], [TypeDiscount], [Value], [isActive], [CreateDate]) VALUES (5, 1, 100000, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2))

SET IDENTITY_INSERT [dbo].[Voucher] OFF
GO

SET IDENTITY_INSERT [dbo].[Invoice] ON 

INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (1, '2022-01-01', 1, 96.75, 2, 4, 1, 1, 20.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (2, '2022-01-02', 2, 81.00, 2, 5, 2, 2, 30.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (3, '2022-01-03', 3, 30.00, 2, 6, 3, 3, 40.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (4, '2022-01-04', 1, 29.25, 2, 4, 4, 4, 50.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (5, '2022-01-05', 2, 42.5, 2, 5, 5, 5, 60.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (6, '2022-01-06', 3, 44.0, 2, 6, 5, 1, 70.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (7, '2022-01-07', 1, 102.75, 2, 4, 1, 2, 80.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (8, '2022-01-08', 2, 33.00, 2, 5, 2, 3, 90.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (9, '2022-01-09', 3, 94.00, 2, 6, 3, 4, 100.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (10, '2022-01-10', 1, 25.00, 2, 4, 4, 5, 110.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (11, '2022-01-11', 2, 34.50, 2, 5, 5, 1, 120.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (12, '2022-01-12', 3, 55.50, 2, 6, 5, 2, 130.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (13, '2022-01-13', 1, 46.00, 2, 4, 1, 3, 140.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (14, '2022-01-14', 2, 39.75, 2, 5, 2, 4, 150.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (15, '2022-01-15', 3, 81.00, 2, 6, 3, 5, 160.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (16, '2022-01-16', 1, 29.00, 2, 4, 4, 1, 170.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (17, '2022-01-17', 2, 9.00, 2, 5, 5, 2, 180.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (18, '2022-01-18', 3, 57.75, 2, 6, 5, 3, 190.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (19, '2022-01-19', 1, 52.00, 2, 4, 1, 4, 200.00)
INSERT INTO [dbo].[Invoice] ([Id], [Date], [Payment], [Total], [Status], [UserId], [VoucherId], [ProfitID], [ProfitTatolImport])  VALUES  (20, '2022-01-20', 2, 32.00, 2, 5, 2, 5, 210.00)
SET IDENTITY_INSERT [dbo].[Invoice] OFF
GO

SET IDENTITY_INSERT [dbo].[InvoiceDetails] ON 
INSERT INTO [dbo].[InvoiceDetails] ([Id], [ProductId], [Quantity], [Price], [Note], [Total], [InvoiceId])
VALUES
(1, 1, 5, 10.50, N'Note 1', 52.50, 1),/**/
(2, 2, 3, 12.75, N'Note 2', 38.25, 1),/**/
(3, 3, 2, 8.00, N'Note 3', 16.00, 1),/**/
(4, 4, 1, 20.00, N'Note 4', 20.00, 2), /*1*/
(5, 5, 4, 6.25, N'Note 5', 25.00, 2),/**/
(6, 6, 2, 15.00, N'Note 6', 30.00, 3),/*3*/
(7, 7, 1, 18.50, N'Note 7', 18.50, 5),/**/
(8, 8, 3, 9.75, N'Note 8', 29.25, 4),/**/
(9, 9, 5, 7.20, N'Note 9', 36.00, 2),/**/
(10, 10, 2, 12.00, N'Note 10', 24.00, 5),/**/
(11, 11, 4, 5.50, N'Note 11', 22.00, 6),/**/
(12, 12, 3, 14.25, N'Note 12', 42.75, 7),/**/
(13, 13, 1, 22.00, N'Note 13', 22.00, 6),/**/
(14, 14, 2, 16.50, N'Note 14', 33.00, 8),/**/
(15, 15, 4, 8.75, N'Note 15', 35.00, 9),/**/
(16, 16, 5, 5.00, N'Note 16', 25.00, 10),/**/
(17, 17, 3, 11.50, N'Note 17', 34.50, 11),/**/
(18, 18, 2, 17.00, N'Note 18', 34.00, 12),/**/
(19, 19, 1, 19.00, N'Note 19', 19.00, 13),/**/
(20, 20, 3, 13.25, N'Note 20', 39.75, 14),/**/
(21, 1, 4, 11.75, N'Note 21', 47.00, 15),/**/
(22, 2, 2, 14.50, N'Note 22', 29.00, 16),/**/
(23, 3, 1, 9.00, N'Note 23', 9.00, 17),/**/
(24, 4, 3, 19.25, N'Note 24', 57.75, 18),/**/
(25, 5, 5, 5.75, N'Note 25', 28.75, 19),/**/
(26, 6, 2, 16.00, N'Note 26', 32.00, 20),/**/
(27, 7, 1, 21.50, N'Note 27', 21.50, 12),/**/
(28, 8, 4, 8.50, N'Note 28', 34.00, 15),/**/
(29, 9, 3, 7.75, N'Note 29', 23.25, 19),/**/
(30, 10, 2, 13.50, N'Note 30', 27.00, 13),/**/
(31, 11, 1, 24.00, N'Note 31', 24.00, 15),/**/
(32, 12, 4, 14.75, N'Note 32', 59.00, 9),/**/
(33, 13, 3, 20.00, N'Note 33', 60.00, 7)/**/
SET IDENTITY_INSERT [dbo].[InvoiceDetails] OFF
GO

SET IDENTITY_INSERT [dbo].[WorkSheet] ON 
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1, 3)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (2, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 2, 3)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (3, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 3, 3)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (4, CAST(N'2023-07-24T00:00:00.0000000' AS DateTime2), 1, 2)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (5, CAST(N'2023-07-24T00:00:00.0000000' AS DateTime2), 2, 2)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (6, CAST(N'2023-07-24T00:00:00.0000000' AS DateTime2), 3, 2)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (7, CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (8, CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (9, CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES (10, CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (11, CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (12, CAST(N'2023-07-25T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (13, CAST(N'2023-07-26T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (14, CAST(N'2023-07-26T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (15, CAST(N'2023-07-26T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (16, CAST(N'2023-07-27T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (17, CAST(N'2023-07-27T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (18, CAST(N'2023-07-27T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (19, CAST(N'2023-07-28T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (20, CAST(N'2023-07-28T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (21, CAST(N'2023-07-28T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (22, CAST(N'2023-07-29T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (23, CAST(N'2023-07-29T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (24, CAST(N'2023-07-29T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (25, CAST(N'2023-07-30T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (26, CAST(N'2023-07-30T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (27, CAST(N'2023-07-30T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (28, CAST(N'2023-07-31T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (29, CAST(N'2023-07-31T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (30, CAST(N'2023-07-31T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (31, CAST(N'2023-08-01T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (32, CAST(N'2023-08-01T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (33, CAST(N'2023-08-01T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (34, CAST(N'2023-08-02T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (35, CAST(N'2023-08-02T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (36, CAST(N'2023-08-02T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (37, CAST(N'2023-08-03T00.00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (38, CAST(N'2023-08-03T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (39, CAST(N'2023-08-03T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (40, CAST(N'2023-08-04T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (41, CAST(N'2023-08-04T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (42, CAST(N'2023-08-04T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (43, CAST(N'2023-08-05T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (44, CAST(N'2023-08-05T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (45, CAST(N'2023-08-05T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (46, CAST(N'2023-08-06T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (47, CAST(N'2023-08-06T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (48, CAST(N'2023-08-06T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (49, CAST(N'2023-08-07T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (50, CAST(N'2023-08-07T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (51, CAST(N'2023-08-07T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (52, CAST(N'2023-08-08T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (53, CAST(N'2023-08-08T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (54, CAST(N'2023-08-08T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (55, CAST(N'2023-08-09T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (56, CAST(N'2023-08-09T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (57, CAST(N'2023-08-09T00:00:00.0000000' AS DateTime2), 3, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (58, CAST(N'2023-08-10T00:00:00.0000000' AS DateTime2), 1, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (59, CAST(N'2023-08-10T00:00:00.0000000' AS DateTime2), 2, 1)
INSERT [dbo].[WorkSheet] ([Id], [Date], [Shift], [Status]) VALUES  (60, CAST(N'2023-08-10T00:00:00.0000000' AS DateTime2), 3, 1)
/* chạy lại 2 dòng này*/
SET IDENTITY_INSERT [dbo].[WorkSheet] OFF
GO

SET IDENTITY_INSERT [dbo].[UserWorkSheet] ON 
INSERT [dbo].[UserWorkSheet] ([Id], [UserId], [WorkSheetId], [Attend], [CheckIn], [CheckInImg], [CheckOut], [SalaryID]) VALUES (3, 5, 1, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'bacyes', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[UserWorkSheet] ([Id], [UserId], [WorkSheetId], [Attend], [CheckIn], [CheckInImg], [CheckOut], [SalaryID]) VALUES (4, 6, 2, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'bacyes', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[UserWorkSheet] ([Id], [UserId], [WorkSheetId], [Attend], [CheckIn], [CheckInImg], [CheckOut], [SalaryID]) VALUES (5, 4, 3, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'bacyes', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[UserWorkSheet] ([Id], [UserId], [WorkSheetId], [Attend], [CheckIn], [CheckInImg], [CheckOut], [SalaryID]) VALUES (6, 6, 4, 0, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'bacno', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[UserWorkSheet] ([Id], [UserId], [WorkSheetId], [Attend], [CheckIn], [CheckInImg], [CheckOut], [SalaryID]) VALUES (7, 5, 4, 1, CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), N'bacno', CAST(N'2023-07-23T00:00:00.0000000' AS DateTime2), 1)
INSERT INTO [dbo].[UserWorkSheet] ([Id], [UserId], [WorkSheetId], [Attend], [CheckIn], [CheckInImg], [CheckOut], [SalaryID])
VALUES
  (16, 4, 9, 1, CAST(N'2023-07-25T08:00:00.0000000' AS DateTime2), 'checkin_img_1.jpg', CAST(N'2023-07-25T17:00:00.0000000' AS DateTime2), 1),
  (17, 5, 10, 1, CAST(N'2023-07-25T08:00:00.0000000' AS DateTime2), 'checkin_img_2.jpg', CAST(N'2023-07-25T17:00:00.0000000' AS DateTime2), 2),
  (18, 6, 11, 1, CAST(N'2023-07-25T08:00:00.0000000' AS DateTime2), 'checkin_img_3.jpg', CAST(N'2023-07-25T17:00:00.0000000' AS DateTime2), 3),
  (19, 4, 12, 1, CAST(N'2023-07-26T08:00:00.0000000' AS DateTime2), 'checkin_img_4.jpg', CAST(N'2023-07-26T17:00:00.0000000' AS DateTime2), 1),
  (20, 5, 13, 1, CAST(N'2023-07-26T08:00:00.0000000' AS DateTime2), 'checkin_img_5.jpg', CAST(N'2023-07-26T17:00:00.0000000' AS DateTime2), 2),
  (21, 6, 14, 1, CAST(N'2023-07-26T08:00:00.0000000' AS DateTime2), 'checkin_img_6.jpg', CAST(N'2023-07-26T17:00:00.0000000' AS DateTime2), 3),
  (22, 4, 15, 1, CAST(N'2023-07-27T08:00:00.0000000' AS DateTime2), 'checkin_img_7.jpg', CAST(N'2023-07-27T17:00:00.0000000' AS DateTime2), 1),
  (8, 5, 16, 1, CAST(N'2023-07-27T08:00:00.0000000' AS DateTime2), 'checkin_img_8.jpg', CAST(N'2023-07-27T17:00:00.0000000' AS DateTime2), 2),
  (9, 6, 17, 1, CAST(N'2023-07-27T08:00:00.0000000' AS DateTime2), 'checkin_img_9.jpg', CAST(N'2023-07-27T17:00:00.0000000' AS DateTime2), 3),
  (10, 4, 18, 1, CAST(N'2023-07-28T08:00:00.0000000' AS DateTime2), 'checkin_img_10.jpg', CAST(N'2023-07-28T17:00:00.0000000' AS DateTime2), 1),
  (11, 5, 19, 1, CAST(N'2023-07-28T08:00:00.0000000' AS DateTime2), 'checkin_img_11.jpg', CAST(N'2023-07-28T17:00:00.0000000' AS DateTime2), 2),
  (12, 6, 20, 1, CAST(N'2023-07-28T08:00:00.0000000' AS DateTime2), 'checkin_img_12.jpg', CAST(N'2023-07-28T17:00:00.0000000' AS DateTime2), 3),
  (13, 4, 21, 1, CAST(N'2023-07-29T08:00:00.0000000' AS DateTime2), 'checkin_img_13.jpg', CAST(N'2023-07-29T17:00:00.0000000' AS DateTime2), 1),
  (14, 5, 22, 1, CAST(N'2023-07-29T08:00:00.0000000' AS DateTime2), 'checkin_img_14.jpg', CAST(N'2023-07-29T17:00:00.0000000' AS DateTime2), 2),
  (15, 6, 23, 1, CAST(N'2023-07-29T08:00:00.0000000' AS DateTime2), 'checkin_img_15.jpg', CAST(N'2023-07-29T17:00:00.0000000' AS DateTime2), 3);
SET IDENTITY_INSERT [dbo].[UserWorkSheet] OFF
GO


SET IDENTITY_INSERT [dbo].[OverTimeWorkSheet] ON 
INSERT [dbo].[OverTimeWorkSheet] ([Id], [UserId], [StartTime], [EndTime], [Total]) VALUES (1, 4, 0, 10, 10)
INSERT [dbo].[OverTimeWorkSheet] ([Id], [UserId], [StartTime], [EndTime], [Total]) VALUES (2, 5, 11, 21, 10)
INSERT [dbo].[OverTimeWorkSheet] ([Id], [UserId], [StartTime], [EndTime], [Total]) VALUES (3, 6, 0, 22, 22)
INSERT [dbo].[OverTimeWorkSheet] ([Id], [UserId], [StartTime], [EndTime], [Total]) VALUES (4, 4, 0, 24, 24)
INSERT [dbo].[OverTimeWorkSheet] ([Id], [UserId], [StartTime], [EndTime], [Total]) VALUES (5, 4, 23, 32, 9)
SET IDENTITY_INSERT [dbo].[OverTimeWorkSheet] OFF
GO

