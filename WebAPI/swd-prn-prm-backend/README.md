# SWD-PRN-PRM-BackEnd

# User  --  Done
# Product  -- Done(BE)
# Iventory -- Done(BE)
# Invoice  -- Done(BE)     
# Salary
# WorkSheet
# Import
# Request
# Voucher

##Note:
- Các requestParam có dạng sortABC thì có giá trị là: asc, desc
- Các request param trong các api filter là không bắt buộc và có thể null. Nếu null thì mặc định bỏ qua điều kiện liên quan đến param đó. VD nếu param status null thì mặc định sẽ lấy tất vả status.
- <b>Viết doc API thì sẽ dựa vào chức năng để quy định api</b>, đéo phải dựa vào entity mà cái gì cũng CRUD. VD bảng InvoiceDetail sẽ không có api do không có chức năng nào liên quan đến nó, chỉ có chức năng liên quan đến Invoice (tao cần api update InvoiceDetail làm cái gì). Bảng InvoiceDetail là do backend tự định nghĩa, không liên quan đến project chung nên chỉ tương tác qau API của Invoice.
- Các bảng có liên quan đến nhiều bảng khác như là Invoice, thì không được trả về Entity thuần mà phải tạo DTO để custom lại data. VD, bảng Invoice có liên quan đến InvoiceDetail, UserId, VoucherId. Không được trả về UserId, VoucherId mà phải query cụ thể cái user là thằng nào, voucher là thằng nào để còn hiển thị, chả lẽ hiển thị cái Id (Ở đây nếu gọi thêm api ở phía frontend thì 1 trang sẽ load n cái API, chưa kể nếu load cái list thì số lượng api cần gọi sẽ rất lớn)? Ở đây phải tạo InvoiceDTO, trả thêm (UserId => StaffId, StaffName, StaffAvatar (cần Id để click vào thì nó ra chi tiết)) 
- Frontend chỉ hiển thị dữ liệu, xử lý dữ liệu, custome dữ liệu thì backend lo.
- Viết doc API thì hãy tư duy như frontend, Hình dung ra cái màn hình là sẽ biết cần trả về cái gì.


# API
## 1. User APIs:
### [x] 1.1 Login:
- URL: /api/user/login
- Method: POST
- Request body: {email, password}
- Response body: result: {Id, FullName, Avatar, IsActive, Role, Email}
- Description: User login using email and passwords.
### [x] 1.2 Get account information:
- URL: /api/user/account
- Method: GET
- Request body: none
- Response body: result: {Id, FullName, StartToWork, Avatar, ContractExpiration, Phone, Role, Address, IsActive, Email, Gender, DateOfBirth, Salary}
- Description: Get current login user account.
### [x] 1.3 Create user:
- URL: /api/user
- Method: POST
- Request body: {FullName, StartToWork, Avatar, ContractExpiration, Phone, Address, Note, IsActive, Email, Gender, DateOfBirth, Salary}
- Response body: result: {Id}
- Description: Create a new user.
### [x] 1.4 Get User by id
- URL: /api/user?id
- Method: GET
- Request body: none
- Response body: result: {Id, FullName, StartToWork, Avatar, ContractExpiration, Phone, Role, Address, IsActive, Email, Gender, DateOfBirth, CreatedDate, Salary}
- Description: Get user by id.
### [x] 1.5 Get User by email
- URL: /api/user/email?email
- Method: GET
- Request body: none
- Response body: result: {Id, FullName, StartToWork, Avatar, ContractExpiration, Phone, Role, Address, IsActive, Email, Gender, DateOfBirth, CreatedDate, Salary}
- Description: Get user by email.
### [x] 1.6 Filter user list
- URL: /api/user/list?fullName&role&status&gender&sortCreatedDate&sortName&page&size
- Method: GET
- Request body: none
- Response body: result: {List{Id, FullName, Avatar, ContractExpiration, Role, IsActive, Gender, DateOfBirth, CreatedDate, Salary}, TotalPage}
- Description: Filter user list with pagination.
### [x] 1.7 Activate/Deactivate user:
- URL: /api/user/status?id&isActive
- Method: PATCH
- Request body: none
- Response body: null
- Description: Activate/deactivate user
### [x] 1.8 Assign role for user:
- URL: /api/user/role?id&role
- Method: PATCH
- Request body: none
- Response body: null
- Description: Assign role for user
### [x] 1.9 Update employee information:
- URL: /api/user
- Method: PUT
- Request body: {Id, FullName, StartToWork, Avatar, ContractExpiration, Phone, Address, Email, Gender, DateOfBirth, Salary}
- Response body: null
- Description: shop owner Update user information
### [x] 1.10 Change account password:
- URL: /api/user/account/password
- Method: PATCH
- Request body: {OldPassword, NewPassword}
- Response body: none
-	 Description: Change account password
# 2. Inventory APIs:
### [x] 2.1 Create new good
- URL: /api/inventory
- Method: POST
- Request body: {Name, CategoryId, Quantity}
- Response body: {Id}
- Description: Create new good in the inventory.
### [x]  2.2 Get good by id
- URL: /api/inventory?id
- Method: GET
- Request body: none
- Response body: {Id, Name, CategoryId, Quantity, IsInUse}
- Description: Get good by id.
### [x] 2.4 Update good information:
- URL: /api/inventory
- Method: PUT
- Request param: none
- Request body: {Id, Name, CategoryId, IsInUse}
- Response body: null
- Description: Update good information.
### [x]  2.5 Update good quantity:
- URL: /api/inventory/quantity?id&quantity
- Method: PATCH
- Request body: none
- Response body: none
- Description: Update good quantity.
### [x]  2.6 Filter good list
- URL: /api/inventory/list?name&category&isInUse&sortName&sortQuantity&page&size
- Method: GET
- Request body: none
- Response body: result: List<Good>, totalPage
- Description: Filter good list with pagination.
# 3. Product APIs:
### [x]  3.1 Get product by id:
- URL: /api/product?id
- Method: GET
- Request body: none
- Response body: Product {Id, Name, Description, Status, OutOfStock, Image, CategoryProductId}
- Description: Get product by id
### [x]  3.2 Create product:
- URL: /api/product
- Method: POST
- Request param: none
- Request body: {Name, Price, CategoryId, Description}
- Response body: {Id}
- Description: Create new product
### [x]  3.3 Filter product list:
- URL: /api/product/list?name&category&status&sortName&sortPrice&page&size
- Method: GET
- Request body: none
- Response body: result: List<{Id, Name, Description, Status, OutOfStock, Image, CategoryProductId}>, TotalPage
- Description: Filter product list with pagination.
### [x] 3.4 Update product:
- URL: /api/product
- Method: PUT
- Request param: none
- Request body: {Id, Name, Price, CategoryId, Description}
- Response body: null
- Description: Update product information.
### [x] 3.5 Update product image:
- URL: /api/product/image?id
- Method: PATCH
- Request body: file (multipartFile)
- Response body: null
- Description: Update product image.
###[x] 3.6 Update product status:
- URL: /api/product/status?id&status&outOfStock
- Method: PATCH
- Request body: none
- Response body: none
- Description: Update product status.
# 4. Category product API:
### [x] 4.1 Get category list:
- URL: /api/product/category/list
- Method: GET
- Request body: none
- Response body: List<{Id, Name}>
- Description: get category list.
# 5. Category inventory API:
### [x] 5.1 Get category list:
- URL: /api/inventory/category/list
- Method: GET
- Request param: none
- Request body: none
- Response body: List<{Id, Name}>
- Description: get category list.


# 6. Invoice API:
### 6.1 Get invoice list by date:
- URL: /api/invoice/list?date&page&size
- Method: GET
- Request body: none
- Response body: result{List<Id, Date, Payment, Total, Status, StaffId, StaffName, StaffAvatar, VoucherId, VoucherValue, VoucherActive>, TotalPage}
- Description: get invoice list by date.
### 6.2 Get Invoice By Id: (Important)
- URL: /api/invoice?id
- Method: GET
- Request body: none
- Response body: result: {Id, Date, Payment, Total, Status, StaffId, StaffName, StaffAvatar, VoucherId, VoucherValue, VoucherActive, List<{ProductId, ProductName, Quantity, Price, Note, Total}>}
- Description: get invoice by id.
### 6.3 Change Invoice Status:
- URL: /api/invoice/status?id&status
- Method: PATCH
- Request param: 
- Request body: none
- Response body: none
- Description: change invoice list.
### 6.4 Create Invoice:
- URL: /api/invoice
- Method: POST
- Request body: {List<{ProductId, Quantity}>, Payment, VoucherId}
- Response body: result:{Id}
- Description: create invoice.


# 7. Import API:
### [x] 7.1:  Create Import:
- URL: /api/import
- Method: POST
- Request body: {ImageImport, List<{InventoryId, Quantity}>}
- Response body: result: {Id}
- Description: Create Import.
### 7.2: Get Import By Id: 
- URL: /api/import?id
- Method: GET
- Request body: none
- Response body: result: {Id, Date, Status, TotalPrice, ImageImport, StaffId, StaffName, StaffAvatar, List<{InventoryId, InventoryName, Quantity, Price, TotalPrice}>}
- Description: Get Import Detail.
### 7.3: Filter List Import: (sort by date desc)
- URL: /api/import/list?staffName&staffId&page&size
- Method: GET
- Request body: none
- Response body: result: {List<{Id, Date, TotalPrice, Status}, TotalPage>}
- Description: View list Import.
### 7.4: Get List Import of current account: (sort by date desc)
- URL: /api/import/user/list?page&size
- Method: GET
- Request body: none
- Response body: result: {List<{Id, Date, TotalPrice, Status}, TotalPage>}
- Description: View list Import.
### 7.5: Update Import:
- URL: /api/import
- Method: PUT
- Request body: {Id, Status, ImageImport, List<{InventoryId, InventoryName, Quantity, Price, TotalPrice}>}
- Response body: none
- Description: Update Import.


# 9. Overtime Worksheet API:
### 9.1: Create Overtime Worksheet:
- URL: /api/overtime/create
- Method: POST
- Request param: none
- Request body: {UserId, Date, StartTime, EndTime}
- Response body: result: {id}
- Description: Create an overtime worksheet.
### 9.2: View List Overtime Worksheet with pagination:
- URL: /api/overtime/list?pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {List<{Date, UserID, StartTime, EndTime, Total Time}>, PageNumber}
- Description: View list of overtime worksheet with pagination.
### 9.3: Update Overtime Worksheet:
- URL: /api/overtime/update
- Method: PUT
- Request param: none
- Request body: {UserId, Date, StartTime, EndTime}
- Response body: result: {Id}
- Description: Update overtime worksheet.
### 9.4: Delete a Overtime Worksheet:
- URL: /api/overtime/delete?id
- Method: DELETE
- Request param: none
- Request body: none
- Response body: result: [message]
- Description: Delete overtime worksheet.
### 9.5: Filter Overtime Worksheet with pagination:
- URL: /api/overtime/list?userId&date&pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {List<{Date, UserID, StartTime, EndTime, Total Time}>, PageNumber}
- Description: Filter overtime worksheet by userId and date with pagination.
### 9.6: Get user’s Overtime Worksheet with pagination:
- URL: /api/overtime/list?userId&pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {List<{Date, UserID, StartTime, EndTime, Total Time}>, PageNumber}
- Description: Get user’s overtime worksheets by userId with pagination.




# 10. Punishment API:
### 10.1 Create Punishment:
- URL: /api/punishment/create
- Method: POST
- Request param: none
- Request body: {UserId, Reason, Fine, Note}
- Response body: result: {Id}
- Description: Create punishment with userId, reason for punishment, fined money, and note.


### 10.2 Get All Punishment with pagination:
- URL: /api/punishment/list?pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {List<{Id, UserId, Reason, Fine, Note, Date}>, TotalPage}
- Description: Get all punishment with pagination.


### 10.3 Get Punishment By Id:
- URL: /api/punishment?id
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {Id, UserId, Reason, Fine, Note, Date}
- Description: Get a  punishment detail with punishment’s Id.


### 10.4 Update Punishment:
- URL: /api/punishment/update
- Method: PUT
- Request param: none
- Request body: {Id, UserId, Reason, Fine, Note, Date}
- Response body: result: {Id}
- Description: Update a punishment information and return punishment’s Id


### 10.5 Get user Punishments with pagination:
- URL: /api/punishment/list?userId&pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {List<{Id, UserId, Reason, Fine, Note, Date}>, TotalPage}
- Description: Get all user punishment with pagination.


# 11. Request API:
### 11.1: Create Request:
- URL: /api/request
- Method: POST
- Request param: none
- Request body: {Title, Reason, DateOff, Note, RequestTypeId}
- Response body: result: {Id}
- Description: Create a Request.


### 11.2: View List Request with pagination: (sort by date desc)
- URL: /api/request/list?requestTypeId&pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {List<{Id, UserId, ApprovedDate, ApprovedBy, Title, Reason, RequestDate, Status, DateOff, Note, RequestTypeId}>, TotalPage}
- Description: View list Request with pagination.




### 11.3: View personal List Request with pagination: (sort by date desc)
- URL: /api/request/account/list?requestTypeId&pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result: {List<{Id, UserId, ApprovedDate, ApprovedBy, Title, Reason, RequestDate, Status, DateOff, Note, RequestTypeId}>, TotalPage}
- Description: View personal list Request with pagination.


### 11.4: View request detail (Get by Id): 
- URL: /api/request?id
- Method: GET
- Request param: none
- Request body: none
- Response body: result: result: {Id, UserId, ApprovedDate, ApprovedBy, Title, Reason, RequestDate, Status, DateOff, Note, RequestTypeId}
- Description: Update Request.


### 11.5: Update Request: (UserId is current user’s)
- URL: /api/request
- Method: PUT
- Request param: none
- Request body: {Id, ApprovedDate, ApprovedBy, Title, Reason, RequestDate, Status, DateOff, Note, RequestTypeId}
- Response body: result: result: {Id}
- Description: Update Request.


# 12. WorkSheet API:

### Get worksheets by Week (date dd/mm/yyyy):
- URL: /api/worksheet/list?date
- Method: GET
- Request body: none
- Response body: result: List<{worksheetId, date, shift, List<{userId, fullName}>}>
- Description: get worksheet by week by input a date in that week.

### Get worksheet detail:
- URL: /api/worksheet?date&shift
- Method: GET
- Request body: none 
- Response body: result: {worksheetId, List<{userId, fullName}>}
- Description: get worksheet detail. If worksheet does not exist, create new one.

### Get available user in worksheet:
- URL: /api/worksheet/users?worksheetId
- Method: GET
- Request body: none
- Response body: result: List<{userId, fullName}>
- Description: get available users for adding to worksheet. Available users include: users that has not been in that worksheet, has role staff or shop owner, has account status is active and ContractExpiration < the date begin that week (that mean user is currently work in the store at that week).

### Update list user in worksheet:
- URL: /api/worksheet?worksheetId
- Method: PUT
- Request body: users: List<{userId}>
- Response body: none (just need status)
- Description: update list use in a worksheet.

### Duplicate:
- URL: /api/worksheet/duplicate
- Method: POST
- Request body: {currentDate, startDate, endDate}
- Response body: result: none (just need status)
- Description: duplicate worksheet by enter a date in source week, a date in start week of worksheet target, a date in end week of worksheet target. startDate must be > the dates of source week (sunday), end date mus be >= startDate. if endDate is null, just duplicate a week.


# 13. Voucher API:
### 13.1: Create Voucher:
- URL: /api/voucher
- Method: POST
- Request param: none
- Request body: {TypeDiscount, Value}
- Response body: {Id}
- Description: Create a voucher.


### 13.2: View List Voucher with pagination:
- URL: /api/voucher/list?pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none 
- Response body: result: {List<{TypeDiscount, Value, IsActive}>, TotalPage}
- Description: View list voucher.


### 13.3: Update Voucher:
- URL: /api/voucher
- Method: PUT
- Request param: none
- Request body: {Id, TypeDiscount, Value}
- Response body: none
- Description: Update voucher.


### 13.4: Deactivate/Activate Voucher:
- URL: /api/voucher/id
- Method: PUT
- Request param: none
- Request body: {Id, IsActive}
- Response body: none
- Description: Deactivate/activate voucher.


# 14 User - WorkSheet
### 14.1: Create User-Worksheet:
- URL: /api/user-worksheet
- Method: POST
- Request param: none
- Request body: {UserId, WorksheetId}
- Response body: {Id}
- Description: Create a  User-Worksheet.


### 14.2: Update User-WorkSheet:
- URL: /api/user-worksheet
- Method: PUT
- Request param: none
- Request body: {UserId, WorkSheetId, IsAttendence, Status, CheckIn, CheckOut, CheckInImg}
- Response body: {UserId, WorksheetId}
- Description: Update user-worksheet.


### 14.3: Delete a User-Worksheet:
- URL: /api/user-worksheet?id
- Method: DELETE
- Request param: none
- Request body: none
- Response body: result:{message}
- Description: Delete a  user-worksheet.


### 14.4: View User-Worksheet list by UserId with pagination
- URL: /api/user-worksheet?userId&pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result:{List<{UserId, WorkSheerId, IsAttendence, Status, CheckIn, CheckOut, CheckInImg}>, TotalPage}
- Description: Get User WorkSheet by UserId with pagination.


### 14.5 / 14.6: CheckIn / CheckOut User-Worksheet 
- URL: /api/user-worksheet/attend
- Method: PUT
- Request param: none
- Request body: {UserId, WorksheetId, Mode, Time}
- Response body: {UserId, WorkSheetId}
- Description: User CheckIn or CheckOut when Start or Finish a Shift. Mode field specified whether user Checkin or Checkout


### 14.7: Filter/GetList User-Worksheet with pagination
- URL: /api/user-worksheet/list?date&userId&mode&pageNumber&pageSize
- Method: GET
- Request param: none
- Request body: none
- Response body: result:{List<{UserId, WorkSheetId, IsAttendence, Status, CheckIn, CheckOut, CheckInImg}>, TotalPage}
- Description: Filter WorkSheet by Date, UserID, mode (checkIn/checkOut), PageNumber, PageSize.


## Additional api
### Get revenue: (sort theo date desc)
- URL: /api/invoice/revenue?pageNumber?pageSize
- Method: GET
- Request body: none
- Response body: result: {List<{total, date}>, totalPage}
- Description: shop owner can view list revenue, sort by date desc. the total is total price of all bill in that date. If a date has no bill, its total will be 0. You should define the first date to get revenue (nghĩa là tự định nghĩa ngày bắt đầu lấy là ngày nào.(ngày bắt đầu là ngày mở quán đồ)).

### Get revenue detail: (get bills by date, format: mm-dd-yyyy giống với update user api): 
- URL: /api/invoice/list?date&pageNumber&pageSize
- Method: GET
- Request body: none
- Response body: result: {List<{id, staffId, staffName, payment, total, status, voucherId}>, totalPage}
- Description: Lấy danh sách bill theo ngày (revenue detail). 

### Get bill detail: (get bill detail):
- URL: /api/invoice/invoice-detail?id
- Method: GET
- Request body: none
- Response body: result: {List<{productId, productImg, quantity, price, total}>, totalPage}
- Description: lây danh sách sản phẩm của bill theo id. Cái này không cần phân trang




