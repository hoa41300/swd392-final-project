﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ProductCategoryViewModels
{
    public class ProductCategoryViewModel
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
}
