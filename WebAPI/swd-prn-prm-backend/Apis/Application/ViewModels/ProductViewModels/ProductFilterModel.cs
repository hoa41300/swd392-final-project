﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ProductViewModels
{
    public class ProductFilterModel
    {
        public string? Name { get; set; }

        public string? Sortname { get; set; }

        public string? Status { get; set; }
        public string? OutOfStock { get; set; }

        public string? CategoryId { get; set; }

        public string? Sortprice { get; set; }

        public int pageSize { get; set; } = 1;
        public int pageNumber { get; set; } = 1;
    }
}
