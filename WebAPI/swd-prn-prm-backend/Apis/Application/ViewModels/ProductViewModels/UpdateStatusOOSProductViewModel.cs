﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ProductViewModels
{
    public class UpdateStatusOOSProductViewModel
    {
        public string Status { get; set; }

    }
}
