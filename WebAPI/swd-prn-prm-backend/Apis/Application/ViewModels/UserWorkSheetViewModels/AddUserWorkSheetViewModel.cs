﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.UserWorkSheetViewModels
{
    public class AddUserWorkSheetViewModel
    {
        public int UserId { get; set; }
        public int WorkSheetId { get; set; }
    }
}
