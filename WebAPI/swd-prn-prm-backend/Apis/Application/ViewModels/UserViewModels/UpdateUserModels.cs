﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.UserViewModels
{
    public class UpdateUserModels
    {
        public string? FullName { get; set; }
        public DateTime? StartToWork { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public string? Note { get; set; }
        public string? Email { get; set; }
        public DateTime? ContractExpiration { get; set; }
        public string? Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public double? Salary { get; set; }
    }
}
