﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.UserViewModels
{
    public class UserFilterModel
    {
        public string? name {get; set;}
        public string? role { get; set;}
        public string? gender { get; set;}
        public string? status { get; set;}

        public string? sortName { get; set;}
        public string? sortCreateDate { get; set;}
        public int pageSize { get; set; } = 1;
        public int pageNumber { get; set; } = 1;
    }
}
