﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.RequestViewModels
{
    public class RequestViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int? AppovedBy { get; set; }
        public string Title { get; set; }
        public string Reason { get; set; }
        public DateTime DateOff { get; set; }
        public DateTime RequestDate { get; set; }
        public int Status { get; set; }
        public string? Note { get; set; }
    }
}
