﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.RequestViewModels
{
    public class CreateRequestViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Reason { get; set; }
        public DateTime DateOff { get; set; }
        public string? Note { get; set; }
    }
}
