﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PunishmentViewModels
{
    public class CreatePunishmentViewModel
    {
        public int UserId { get; set; }
        public string Reason { get; set; }
        public int Fine { get; set; }
        public string? Note { get; set; }
    }
}
