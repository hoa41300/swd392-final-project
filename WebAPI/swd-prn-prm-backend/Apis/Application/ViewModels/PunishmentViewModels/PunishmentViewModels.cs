﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.PunishmentViewModels
{
    public class PunishmentViewModels
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Reason { get; set; }
        public int Fine { get; set; }
        public string? Note { get; set; }
        public DateTime Date { get; set; }


    }
}
