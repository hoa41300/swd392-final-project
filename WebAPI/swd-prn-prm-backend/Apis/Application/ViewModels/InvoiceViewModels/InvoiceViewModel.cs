﻿using Application.ViewModels.InvoiceDetailsViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.InvoiceViewModels
{
    public class InvoiceViewModel
    {
        public int Id { get; set; }
        public String Date { get; set; }
        public int Payment { get; set; }
        public double Total { get; set; }
        public string Status { get; set; }
        public string Username { get; set; }
        public int? VoucherId { get; set; }
        public List<InvoiceDetailViewModel> InvoiceDetails { get; set; }
    }
}
