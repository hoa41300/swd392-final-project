﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.InvoiceViewModels
{
    public class InvoiceFilter
    {
        public DateTime? beginDate { get; set; }

        public DateTime? endDate { get; set; }
        public int pageSize { get; set; } = 1;
        public int pageNumber { get; set; } = 1;
    }
}
