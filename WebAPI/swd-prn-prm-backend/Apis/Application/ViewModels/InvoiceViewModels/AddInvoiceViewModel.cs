﻿using Application.ViewModels.InvoiceDetailsViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.InvoiceViewModels
{
    public class AddInvoiceViewModel
    {
        /*public DateTime Date { get; set; }*/
        public int Payment { get; set; }
        public int? VoucherId { get; set; }

        public List<AddInvoiceDetailViewModel> InvoiceDetails { get; set; }
    }
}
