﻿using Application.ViewModels.UserWorkSheetViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.WorkSheetViewModels
{
    public class WorkSheetViewModel
    {
        public int Id { get; set; }
        public string? Date { get; set; }
        public string? Shift { get; set; }
        public List<UserWorkSheetViewModel> listUser  { get; set; }
    }
}
