﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.WorkSheetViewModels
{
    public class AddWorkSheetViewModel
    {
        public DateTime Date { get; set; }
        public int Shift { get; set; }
        public int Status { get; set; }

        public AddWorkSheetViewModel(DateTime date, int shift, int status)
        {
            Date = date;
            Shift = shift;
            Status = status;
        }
    }
}
