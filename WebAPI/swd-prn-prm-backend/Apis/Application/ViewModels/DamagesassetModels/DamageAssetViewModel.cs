﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.DamagesAsset
{
    public class DamageAssetViewModel
    {
        public int UserId { get; set; }
        public string NameAsset { get; set; }
        public int Quantity { get; set; }
    }
}
