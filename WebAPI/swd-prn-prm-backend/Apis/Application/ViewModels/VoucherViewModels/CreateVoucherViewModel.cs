﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.VoucherViewModels
{
    public class CreateVoucherViewModel
    {
        public int TypeDiscount { get; set; }
        public int Value { get; set; }
    }
}
