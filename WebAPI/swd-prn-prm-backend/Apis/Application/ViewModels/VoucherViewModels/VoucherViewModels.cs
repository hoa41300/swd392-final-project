﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.VoucherViewModels
{
    public class VoucherViewModels
    {
        public int Id { get; set; }
        public int TypeDiscount { get; set; }
        public int Value { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreateDate { get; set; } 
        public Invoice? Invoice { get; set; }
    }
}

