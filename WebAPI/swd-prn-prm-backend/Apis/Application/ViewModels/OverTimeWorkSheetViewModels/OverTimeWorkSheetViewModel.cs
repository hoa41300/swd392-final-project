﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.OverTimeWorkSheetViewModels
{
    public class OverTimeWorkSheetViewModel
    {
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int Total { get; set; }

    }
}
