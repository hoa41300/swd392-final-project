﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ImportViewModel
{
    public class ImportViewModel
    {
        public int UserId { get; set; }
        public int Price { get; set; }
        public int Status { get; set; }
        public ICollection<ImportDetails>? importDetails { get; set; }
    }
}
