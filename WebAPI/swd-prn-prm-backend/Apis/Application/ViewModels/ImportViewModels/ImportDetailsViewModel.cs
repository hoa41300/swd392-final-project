﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ImportViewModel
{
    public class ImportDetailsViewModel
    {
        public int Id { get; set; }
        public int InventoryId { get; set; }
        public int ImportId { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }

    }
}
