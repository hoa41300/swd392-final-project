﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.ImportViewModel
{
    public class CreateImportDetailViewModel
    {
        public int InventoryId { get; set; }
        public int ImportId { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }

    }
}
