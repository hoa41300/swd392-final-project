﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.ViewModels;
using Application.ViewModels.ProductViewModels;

namespace Application.ViewModels.InvoiceDetailsViewModels
{
    public class InvoiceDetailViewModel
    {
        public ProductViewModel Product { get; set; }
        public int Quantity { get; set; }
        public string? Note { get; set; }
        public double Price { get; set; }
        public double Total { get; set; }

    }
}
