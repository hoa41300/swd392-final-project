﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.Inventoryviewmodels
{
    public class FilterInventoryModels
    {
        public string? Name { get; set; }
        public string? Category { get; set; }
        public string? isInUser { get; set; }
        public string? sortName { get; set; }
        public string? sortQuantity { get; set; }
        public int pageSize { get; set; } = 1;
        public int pageNumber { get; set; } = 1;

    }
}
