﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.InventoryViewModels
{
    public class CreateInventoryViewModel
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int Quantity { set; get; }
    }
}
