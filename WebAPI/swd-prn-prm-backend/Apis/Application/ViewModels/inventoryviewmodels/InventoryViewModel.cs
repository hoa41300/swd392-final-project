﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.InventoryViewModels
{
    public class InventoryViewModel
    {
        public int Id {  get; set; }
        public string Name { get; set; }
        public string isInUser { get; set; }
        public string? CategoryId { get; set; }
        public int Quantity { set; get; }
    }
}
