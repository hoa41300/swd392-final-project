﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels.Inventoryviewmodels
{
    public class UpdateInventoryViewModels
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string isInUser { get; set; }
    }
}
