﻿using Application.ViewModels.ProductViewModels;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.VoucherViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductServices
    {
        Task<int?> AddProduct(AddProductViewModel addProductViewModel);
        Task<ProductViewModel> GetProductById(int productId);

        Task<ProductViewModel> UpdateProduct(int productId, UpdateProductViewModel productUpdate);
         Task<ProductViewModel> UpdateStatus(int productId);
        /*Task<(List<ProductViewModels>, int)> GetListProduct();*/
        /*Task<ProductViewModels> DeleteVoucher(int productId);*/
        Task <List<ProductViewModel>> SortListByName();
        Task<List<ProductViewModel>> SortListByPrice();

        Task<(List<ProductViewModel>, int)> FilterProduct(ProductFilterModel filter);
    }
}
