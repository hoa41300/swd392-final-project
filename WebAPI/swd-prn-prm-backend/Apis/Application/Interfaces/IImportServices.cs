﻿
using Application.ViewModels.ImportViewModel;

namespace Application.Interfaces
{
    public interface IImportServices
    {
        Task<List<ImportViewModel>> GetAllImports();
        Task<ImportViewModel> GetImportById(int id);
        Task<int> CreateImport(CreateImportViewModel import);
        Task<ImportViewModel> UpdateImport(int id, UpdateImportViewModel importViewModel);
        Task<ImportViewModel> UpdateImportStatus(int id, int StatusNumber);
        Task<bool> DeleteImport(int id);
    }
}
