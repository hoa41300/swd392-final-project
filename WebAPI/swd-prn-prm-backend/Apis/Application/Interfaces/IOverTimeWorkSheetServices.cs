﻿using Application.ViewModels.OverTimeWorkSheetViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IOverTimeWorkSheetServices
    {
        Task<OverTimeWorkSheetViewModel> AddOverTimeWorkSheet(OverTimeWorkSheetViewModel ovtwsViewModel);
        Task<OverTimeWorkSheetViewModel> GetOverTimeWorkSheetById(int ovtwsId);
        Task<OverTimeWorkSheetViewModel> UpdateOverTimeWorkSheet(int ovtwsId, OverTimeWorkSheetViewModel ovtwsUpdate);
        Task<List<OverTimeWorkSheetViewModel>> GetListOverTimeWorkSheet();
    }
}
