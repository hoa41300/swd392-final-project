﻿using Application.ViewModels.ImportViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IImportDetailsServices
    {
        Task<List<ImportDetailsViewModel>> GetAllImportDetails();
        Task<ImportDetailsViewModel> GetImportById(int id);
        Task<ImportDetailsViewModel> NewImportDetailsl(CreateImportDetailViewModel importDetail);
        Task<ImportDetailsViewModel> UpdateImportDetail(int id, CreateImportDetailViewModel update);
        Task<bool> DeleteImportDetail(int id);
    }
}
