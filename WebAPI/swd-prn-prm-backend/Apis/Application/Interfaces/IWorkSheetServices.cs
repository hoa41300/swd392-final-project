﻿using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.WorkSheetViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IWorkSheetServices
    {
        Task<List<WorkSheet>> AddWorkSheet(DateTime date);
        Task<WorkSheetViewModel> GetWorkSheetById(int wsId);
        Task<List<WorkSheetViewModel>> GetListWorkSheet(DateTime dayInWeek);

        List<DateTime> GetDateInWeek(DateTime dayInWeek);
        Task<List<WorkSheetViewModel>> GetWorkSheet(DateTime dayInWeek);
    }
}
