﻿using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.RequestViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IRequestServices
    {
        Task<CreateRequestViewModel> AddRequest(CreateRequestViewModel requestViewModel);
        
        Task<RequestViewModel> UpdateRequest(int invoiceId, RequestViewModel requestViewModel);
        Task<List<RequestViewModel>> GetListAllRequest();

        Task<List<RequestViewModel>> GetListRequestByUserId();

        Task<bool> ChangeStatusToApproved(int id);

        Task<bool> ChangeStatusToDenied(int id);
    }
}
