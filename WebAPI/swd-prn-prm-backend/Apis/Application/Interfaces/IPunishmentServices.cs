﻿using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.PunishmentViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IPunishmentServices
    {
        Task<CreatePunishmentViewModel> AddPunishment(CreatePunishmentViewModel createPunishmentViewModel);
        /*Task<InvoiceViewModel> GetInvoiceById(int invoiceId)*/
        Task<PunishmentViewModels> UpdatePunishment(int invoiceId, PunishmentViewModels punishmentViewModels);
        Task<List<PunishmentViewModels>> GetListPunishment();
        Task<List<PunishmentViewModels>> GetListPunishmentByUserId();
        Task<bool> DeletePunishment();

    }
}
