﻿using Application.ViewModels.VoucherViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IVoucherServices
    {
        Task<VoucherViewModels> GetVoucherById(int voucherId);
        Task<VoucherViewModels> addNewVoucher(CreateVoucherViewModel voucherView);
        Task<VoucherViewModels> UpdateVoucher(int voucherId, CreateVoucherViewModel voucherUpdate);
        Task<List<VoucherViewModels>> GetListVoucher();
        Task<VoucherViewModels> changeVoucherStatus(int voucherId);
    }
}
