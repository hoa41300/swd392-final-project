﻿using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.UserWorkSheetViewModels;
using Application.ViewModels.WorkSheetViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserWorkSheetServices
    {
        Task<int> CreateUserWorkSheet(AddUserWorkSheetViewModel addUserWorkSheetViewModel);

        Task<bool>  CheckIn(int wsid);
        Task<bool> CheckOut(int wsid);
        /*Task<InvoiceViewModel> GetInvoiceById(int invoiceId)*/
        /* Task<InvoiceViewModel> UpdateInvoice(InvoiceViewModel invoiceUpdate);
         Task<(List<InvoiceViewModel>, int, double)> GetListInvoice(InvoiceFilter filter);
         Task<InvoiceViewModel> GetInvoiceById(int invoiceId);

         Task<bool> ChangeStatusToDone(int id);

         Task<bool> ChangeStatusToCancel(int id);

         Task<List<InvoiceViewModel>> GetInvoice();*/
    }
}
