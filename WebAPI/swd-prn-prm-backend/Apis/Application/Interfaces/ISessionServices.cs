﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ISessionServices
    {
        void SaveToken(int id, string token);
        string GetTokenByKey(int id);
        bool RemoveToken(int id);
    }
}
