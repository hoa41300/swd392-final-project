﻿using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.ProductViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IInvoiceServices
    {
        Task<int> AddInvoice(AddInvoiceViewModel addInvoiceViewModel);
        /*Task<InvoiceViewModel> GetInvoiceById(int invoiceId)*/
        Task<InvoiceViewModel> UpdateInvoice(InvoiceViewModel invoiceUpdate);
        Task<(List<InvoiceViewModel>,int,double)> GetListInvoice(InvoiceFilter filter);
        Task<InvoiceViewModel> GetInvoiceById(int invoiceId);

        Task<bool> ChangeStatusToDone(int id);

        Task<bool> ChangeStatusToCancel(int id);

        Task<List<InvoiceViewModel>> GetInvoice();
        /*Task<ProductViewModels> DeleteVoucher(int productId);*/
    }
}
