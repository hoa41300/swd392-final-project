﻿using Application.ViewModels.inventoryviewmodels;
using Domain.Models;

namespace Application.Interfaces
{
    public interface IInventoryCategoryService
    {
       Task<List<InventoryCategoryViewModels>> GetInventoryCategoryAsync();
    }
}