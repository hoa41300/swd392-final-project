﻿using Application.ViewModels.UserViewModels;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserServices
    {
        public Task<string> LoginAsync(UserLoginModel userLogin);

        public Task<int?> CreateUserAsync(UserCreateModel userCreate);

        public Task<List<UserDetailModel>> GetAllUsersAsync();

        Task<string> GetUserName(string email);

        Task<UserDetailModel> GetUserByID(int id);

        Task<UserDetailModel> GetUserByEmail(string email);
        Task<(List<UserDetailModel>, int)> FilterUser(UserFilterModel filter);

        Task<UserDetailModel> UpdateUser(int id, UpdateUserModels updateUser);
        Task<UserDetailModel> UpdateRoleForUser(int id);
        Task<UserDetailModel> UpdateUserStatus(int id);
        Task<UserDetailModel> ChangePassword(int id, ChangePasswordModels changePassword);
    }
}
