﻿using Application.ViewModels.DamagesAsset;
using Application.ViewModels.Inventoryviewmodels;
using Application.ViewModels.InventoryViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IInventoryServices
    {
        Task<List<InventoryViewModel>> GetAllInventory();
        Task<InventoryViewModel> GetInventoryById(int id);
        Task<InventoryViewModel> AddInventory(CreateInventoryViewModel inventory);
        Task<InventoryViewModel> UpdateInventory(int id, UpdateInventoryViewModels update);
        Task<InventoryViewModel> UpdateQuantity(int id, int newQuantity);
        Task <InventoryViewModel> UpdateInventoryStatus(int id);
        Task<(List<InventoryViewModel>, int)> FilterInventory(FilterInventoryModels filter);
    }
}
