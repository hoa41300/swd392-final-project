﻿using Application.ViewModels.DamagesAsset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IDamagesAssetServies
    {
        Task<List<DamageAssetViewModel>> GetAllDameAssets();
        Task<DamageAssetViewModel> GetDameAssetById(int id);
        Task<DamageAssetViewModel> AddNewDameAsset(CreateDamageAssetViewModels DameAsset);
        Task<DamageAssetViewModel> UpdateDameAsset(int id, UpdateDamageAssetViewModel dameAssetUpdate);
        Task <bool> DeleteDameAsset(int id);

    }
}
