﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IImportRepositories : IGenericRepository<Import>
    {
    }
}
