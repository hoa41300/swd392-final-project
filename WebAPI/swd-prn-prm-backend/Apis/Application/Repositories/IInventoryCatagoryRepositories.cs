﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IInventoryCatagoryRepositories : IGenericRepository<InventoryCategory>
    {
    }
}
