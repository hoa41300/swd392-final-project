﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IWorkSheetRepositories : IGenericRepository<WorkSheet>
    {
        Task<List<WorkSheet>> GetWorkSheetByDate(DateTime date);
    }
}
