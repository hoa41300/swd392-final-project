﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IProductRepositories : IGenericRepository<Product>
    {
        Task<List<Product>> SortByName();
        Task<List<Product>> SortByPrice();
    }
}
