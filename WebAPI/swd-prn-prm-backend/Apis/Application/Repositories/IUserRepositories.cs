﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IUserRepositories : IGenericRepository<Users>
    {
        Task<Users?> GetUserByEmailAndPassword(string email, string password);
        Task<string> GetNameByEmail(string email);
        Task<Users> GetUserByEmail(string email);
    }
}
