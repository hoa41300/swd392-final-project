﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Repositories
{
    public interface IUserWorkSheetRepositories : IGenericRepository<UserWorkSheet>
    {
        UserWorkSheet GetUserWorkSheetByUserAndWS(int userId, int wsid);
    }
}
