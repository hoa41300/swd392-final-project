﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IInventoryRepositories : IGenericRepository<Inventory>
    {
    }
}
