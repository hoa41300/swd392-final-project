﻿using Application.ViewModels.InvoiceDetailsViewModels;
using Domain.Models;

namespace Application.Repositories
{
    public interface IInvoiceDetailsRepositories : IGenericRepository<InvoiceDetails>
    {
        List<InvoiceDetails> GetInvoiceDetails(int id);

        bool DeleteInvoiceDetail(int id);

    }
}
