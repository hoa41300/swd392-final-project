﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IRequestRepositories : IGenericRepository<Request>
    {
        List<Request> GetListRequestByUserId(int userId);
    }
}
