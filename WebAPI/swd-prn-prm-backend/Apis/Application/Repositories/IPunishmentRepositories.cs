﻿using Domain.Models;


namespace Application.Repositories
{
    public interface IPunishmentRepositories : IGenericRepository<Punishment>
    {
        List<Punishment> GetListPunishmentByUserId(int userId);
    }
}
