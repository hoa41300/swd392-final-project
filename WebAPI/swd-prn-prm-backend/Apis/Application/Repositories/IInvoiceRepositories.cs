﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Application.Repositories
{
    public interface IInvoiceRepositories : IGenericRepository<Invoice>
    {
    }
}
