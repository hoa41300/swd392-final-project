﻿using Domain.Models;

namespace Application.Repositories
{
    public interface IVoucherRepositories : IGenericRepository<Voucher>
    {
    }
}
