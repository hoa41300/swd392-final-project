﻿using Application.Repositories;

namespace Application
{
    public interface IUnitOfWorks
    {
        public IImportDetailsRepositories ImportDetailsRepositories { get; }
        public IImportRepositories ImportItemsRepositories { get; }
        public IInventoryRepositories InventoryRepositories { get; }
        public IInvoiceDetailsRepositories InvoiceDetailsRepositories { get; }
        public IInvoiceRepositories InvoiceRepositories { get; }
        public IProductRepositories ProductRepositories { get; }
        public IPunishmentRepositories PunishmentRepositories { get; }
        public IRequestRepositories RequestRepositories { get; }
        public IUserRepositories UserRepositories { get; }
        public IVoucherRepositories VoucherRepositories { get; }
        public IWorkSheetRepositories WorkSheetRepositories { get; }
        public IDamagesAssetRepositories IDamagesAssetRepositories { get; }
        public IUserWorkSheetRepositories UserWorkSheetRepositories { get; }
        public IOverTimeWorkSheetRepositories OvertimeWorkSheetRepositories { get; }
        public IProfitRepositories ProfitRepositories { get; }
        public ISalaryRepository SalaryRepositories { get; }
        public IProductCategoryServicesRepo ProductCategoryServicesRepositories { get; }
        public IInventoryCatagoryRepositories InventoryCatagoryRepositories { get; }
        public IRequestTypeRepositories RequestTypeRepositories { get; }

        public Task<int> SaveChangesAsync();
    }
}
