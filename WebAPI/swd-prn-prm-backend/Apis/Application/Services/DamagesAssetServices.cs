﻿using Application.Interfaces;
using Application.ViewModels.DamagesAsset;
using AutoMapper;
using Domain.Models;
using Microsoft.AspNetCore.Http.Metadata;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class DamagesAssetServices : IDamagesAssetServies
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public DamagesAssetServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<DamageAssetViewModel> AddNewDameAsset(CreateDamageAssetViewModels dameAsset)
        {
            var newDA = _mapper.Map<DamagesAsset>(dameAsset);
            newDA.UserId = 1;
            newDA.Users = await _unitOfWorks.UserRepositories.GetByIdAsync(1);

            await _unitOfWorks.IDamagesAssetRepositories.AddAsync(newDA);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<DamageAssetViewModel>(newDA);
            }
            else return null;

        }

        public async Task<bool> DeleteDameAsset(int id)
        {
            var listDA = await _unitOfWorks.IDamagesAssetRepositories.GetAllAsync();
            if (listDA == null)
            {
                throw new Exception();
            }
            else
            {
                var Asset = listDA.FirstOrDefault(x => x.Id.Equals(id));
                if (Asset != null)
                {
                    await _unitOfWorks.IDamagesAssetRepositories.DeleteAsync(Asset);
                    await _unitOfWorks.SaveChangesAsync();
                    return true;
                }
                else return false;
            }
        }

        public async Task<List<DamageAssetViewModel>> GetAllDameAssets()
        {
            var listDA = await _unitOfWorks.IDamagesAssetRepositories.GetAllAsync();
            var listView = _mapper.Map<List<DamageAssetViewModel>>(listDA);
            return listView;
        }

        public async Task<DamageAssetViewModel> GetDameAssetById(int id)
        {
            var listDA = await _unitOfWorks.IDamagesAssetRepositories.GetAllAsync();
            if (listDA == null)
            {
                throw new Exception();
            }
            else
            {
                var Asset = listDA.FirstOrDefault(x => x.Id.Equals(id));
                if (Asset != null)
                {
                    return _mapper.Map<DamageAssetViewModel>(Asset);
                }
                else return null;
            }
        }

        public async Task<DamageAssetViewModel> UpdateDameAsset(int id, UpdateDamageAssetViewModel dameAssetUpdate)
        {
            var dameAsset = await _unitOfWorks.IDamagesAssetRepositories.GetByIdAsync(id);
            if (dameAsset == null)
            {
                return null;
            }
            else
            {
                dameAsset.NameAsset = dameAssetUpdate.NameAsset;
                dameAsset.Quantity = dameAssetUpdate.Quantity;

                _unitOfWorks.IDamagesAssetRepositories.Update(dameAsset);
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<DamageAssetViewModel>(dameAsset);
            }
        }
    }
}
