﻿using Application.Interfaces;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.UserViewModels;
using Application.ViewModels.VoucherViewModels;
using AutoMapper;
using Domain.Enums;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Drawing;

namespace Application.Services
{
    public class ProductServices : IProductServices

    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public ProductServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<int?> AddProduct(AddProductViewModel addProductViewModel)
        {

            Product product = _mapper.Map<Product>(addProductViewModel);
            await _unitOfWorks.ProductRepositories.AddAsync(product);

            var check = await _unitOfWorks.SaveChangesAsync() > 0;
            AddProductViewModel view = _mapper.Map<AddProductViewModel>(product);
            //view.
            if (check)
            {
                return product.Id;
            }

            return null;
        }

        public async Task<(List<ProductViewModel>,int)> FilterProduct(ProductFilterModel filter)
        {
            var listProduct = await _unitOfWorks.ProductRepositories.GetAllAsync();

            if(!string.IsNullOrEmpty(filter.Name))
            {
                listProduct = listProduct.Where(x=> x.Name.ToUpper().Contains(filter.Name.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(filter.Status)) 
            {
                listProduct = listProduct.Where(x=>x.Status.ToString().ToUpper().Equals(filter.Status.ToUpper())).ToList();
            }
            if(!string.IsNullOrEmpty(filter.OutOfStock)) 
            {
                listProduct = listProduct.Where(x => x.OutOfStock.ToString().ToUpper().Equals(filter.OutOfStock.ToUpper())).ToList();
            } 
            if(!string.IsNullOrEmpty(filter.CategoryId))
            {
                listProduct = listProduct.Where(x => x.CategoryId.ToString().Equals(filter.CategoryId)).ToList();
            }
            if(!string.IsNullOrEmpty(filter.Sortprice) && !string.IsNullOrEmpty(filter.Sortname))
            {
                if (filter.Sortprice.ToUpper().Equals("ASC") && filter.Sortname.ToUpper().Equals("ASC"))
                {
                    listProduct = listProduct.OrderBy(x => x.Price).OrderBy(x => x.Name).ToList();
                }
                if (filter.Sortprice.ToUpper().Equals("ASC") && filter.Sortname.ToUpper().Equals("DESC"))
                {
                    listProduct = listProduct.OrderBy(x => x.Price).OrderByDescending(x => x.Name).ToList();
                }
                if (filter.Sortprice.ToUpper().Equals("DESC") && filter.Sortname.ToUpper().Equals("DESC"))
                {
                    listProduct = listProduct.OrderByDescending(x => x.Price).OrderByDescending(x => x.Name).ToList();
                }
                if (filter.Sortprice.ToUpper().Equals("DESC") && filter.Sortname.ToUpper().Equals("ASC"))
                {
                    listProduct = listProduct.OrderByDescending(x => x.Price).OrderBy(x => x.Name).ToList();
                }
            }
            else if (string.IsNullOrEmpty(filter.Sortprice) && !string.IsNullOrEmpty(filter.Sortname))
            {

                if (filter.Sortname.ToUpper().Equals("DESC"))
                {
                    listProduct = listProduct.OrderByDescending(x => x.Name).ToList();
                }
                if (filter.Sortname.ToUpper().Equals("ASC"))
                {
                    listProduct = listProduct.OrderBy(x => x.Name).ToList();
                }
            }
            else if (!string.IsNullOrEmpty(filter.Sortprice) && string.IsNullOrEmpty(filter.Sortname))
            {

                if (filter.Sortprice.ToUpper().Equals("DESC"))
                {
                    listProduct = listProduct.OrderByDescending(x => x.Price).ToList();
                }
                if (filter.Sortprice.ToUpper().Equals("ASC"))
                {
                    listProduct = listProduct.OrderBy(x => x.Price).ToList();
                }
            }

            var list = _mapper.Map<List<ProductViewModel>>(listProduct);
            var totalPages = (int)Math.Ceiling((double)list.Count / filter.pageSize);
            int? itemsToSkip = (filter.pageNumber - 1) * filter.pageSize;
            list = list.Skip((int)itemsToSkip)
                        .Take((int)filter.pageSize)
                         .ToList();
            return (list, totalPages);
        }



        /*public async Task<ProductViewModels> DeleteVoucher(int productId)
        {
            var product = await _unitOfWorks.ProductRepositories.GetByIdAsync(productId);
            if (product == null)
            {
                return null;
            }
            else
            {
                product.Status = false;
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<VoucherViewModels>(product);
            }
        }*/

        public async Task<List<ProductViewModel>> GetListProduct()
        {
            var ProductList = await _unitOfWorks.ProductRepositories.GetAllAsync();
            ProductList = (List<Product>)ProductList.Where(x => (int)x.Status == 1);
            var result = _mapper.Map<List<ProductViewModel>>(ProductList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public async Task<ProductViewModel> GetProductById(int productId)
        {
            
            var product = await _unitOfWorks.ProductRepositories.GetByIdAsync(productId);
            var category = await _unitOfWorks.ProductCategoryServicesRepositories.GetByIdAsync(product.CategoryId);

            ProductViewModel result = _mapper.Map<ProductViewModel>(product);
            result.CategoryName = category.Name;
            return result;
        }

        public async Task<List<ProductViewModel>> SortListByName()
        {
            var listProduct = await _unitOfWorks.ProductRepositories.SortByName();
            var list = _mapper.Map<List<ProductViewModel>>(listProduct);
            return list;
        }

        public async Task<List<ProductViewModel>> SortListByPrice()
        {
            var listProduct = await _unitOfWorks.ProductRepositories.SortByPrice();
            var list = _mapper.Map<List<ProductViewModel>>(listProduct);
            return list;
        }

        #region 
        public async Task<ProductViewModel> UpdateProduct(int productId, UpdateProductViewModel productUpdate)
        {
            var product = await _unitOfWorks.ProductRepositories.GetByIdAsync(productId);
            if (product == null)
            {
                return null;
            }
            else
            {
                product.Name = productUpdate.Name;
                product.Description = productUpdate.Description;
                product.ProductImg= productUpdate.ProductImg;
                product.CategoryId = productUpdate.CategoryId;
                product.Price = productUpdate.Price;

                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<ProductViewModel>(product);
            }
        }
        public async Task<ProductViewModel> UpdateStatus(int productId)
        {
            var product = await _unitOfWorks.ProductRepositories.GetByIdAsync(productId);
            if (product == null)
            {
                return null;
            }
            else
            {
                var status = product.Status.ToString();
                if (status.ToUpper().Equals("INACTIVE")){
                    product.Status = Status.Active; 
                }
                else
                {
                    product.Status = Status.Inactive ;
                }
                /*bool oot = (updateProduct.OutOfStock.ToString().ToUpper().Equals("TRUE")) ? true : false;*/
                /*product.OutOfStock = oot;*/
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<ProductViewModel>(product);
            }
        }

        public Task<ProductViewModel> UpdateStatus(int productId, UpdateStatusOOSProductViewModel updateProduct)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
