﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.ViewModels.inventoryviewmodels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;

namespace Application.Services
{
    public class CategoryInventoryService : IInventoryCategoryService
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        public CategoryInventoryService(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }
        public async Task<List<InventoryCategoryViewModels>> GetInventoryCategoryAsync()
        {
            var InventoryCategory = await _unitOfWorks.InventoryCatagoryRepositories.GetAllAsync();
            var list = _mapper.Map<List<InventoryCategoryViewModels>>(InventoryCategory);
            return list;
        }
    }
}
