﻿
using Application.Interfaces;
using Application.ViewModels.VoucherViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Application.Services
{
    public class VoucherServies : IVoucherServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        private readonly IClaimsServices _claimsServices;
        private readonly IInvoiceServices _invoiceServices;
        public VoucherServies(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration, IClaimsServices claimsServices, IInvoiceServices invoiceServices)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _claimsServices = claimsServices;
            _invoiceServices = invoiceServices;
        }

        public async Task<VoucherViewModels> changeVoucherStatus(int voucherId)
        {
            var voucher = await _unitOfWorks.VoucherRepositories.GetByIdAsync(voucherId);
            if (voucher == null)
            {
                return null;
            }
            else
            {
                voucher.isActive = false;
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<VoucherViewModels>(voucher);
            }
        }

        public async Task<List<VoucherViewModels>> GetListVoucher()
        {
            var VoucherList = await _unitOfWorks.VoucherRepositories.GetAllAsync();
            var result = _mapper.Map<List<VoucherViewModels>>(VoucherList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public async Task<VoucherViewModels> GetVoucherById(int voucherId)
        {
            var VoucherList = await _unitOfWorks.VoucherRepositories.GetAllAsync();
            if(VoucherList.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                var voucher = _mapper.Map<VoucherViewModels>(VoucherList.FirstOrDefault(x => x.Id.Equals(voucherId)));
                return voucher;
            }
        }

        public async Task<VoucherViewModels> addNewVoucher(CreateVoucherViewModel voucherView)
        {
            var voucherNew = _mapper.Map<Voucher>(voucherView); 

            voucherNew.isActive = true;
            voucherNew.CreateDate = _currentTime.GetCurrentTime();
            await _unitOfWorks.VoucherRepositories.AddAsync(voucherNew);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<VoucherViewModels>(voucherNew);
            }
            else throw new Exception("Create Fail!!!");
        }

        public async Task<VoucherViewModels> UpdateVoucher(int voucherId, CreateVoucherViewModel voucherUpdate)
        {
            var voucher = await _unitOfWorks.VoucherRepositories.GetByIdAsync(voucherId);
            if (voucher == null)
            {
                return null;
            }
            else
            {
                voucher.TypeDiscount = voucherUpdate.TypeDiscount;
                voucher.Value = voucherUpdate.Value;

                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<VoucherViewModels>(voucher);
            }
        }
    }
}
