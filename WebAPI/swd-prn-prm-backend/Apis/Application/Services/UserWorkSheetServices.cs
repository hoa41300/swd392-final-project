﻿using Application.Interfaces;
using Application.Repositories;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.UserWorkSheetViewModels;
using Application.ViewModels.WorkSheetViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class UserWorkSheetServices : IUserWorkSheetServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        private readonly IClaimsServices _claimsServices;

        public UserWorkSheetServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration, IClaimsServices claimsServices)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _claimsServices = claimsServices;
        }
        public async Task<bool> CheckIn(int wsid)
        {
            var userWorksheet = _unitOfWorks.UserWorkSheetRepositories.GetUserWorkSheetByUserAndWS(_claimsServices.GetCurrentUserId, wsid);
            userWorksheet.CheckIn = _currentTime.GetCurrentTime();
            userWorksheet.SalaryID = 1;
            _unitOfWorks.UserWorkSheetRepositories.Update(userWorksheet);
            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                //return workSheets;
                return true;
            }
            return false;
        }
        public async Task<bool> CheckOut(int wsid)
        {
            var userWorksheet = _unitOfWorks.UserWorkSheetRepositories.GetUserWorkSheetByUserAndWS(_claimsServices.GetCurrentUserId, wsid);
            userWorksheet.CheckIn = _currentTime.GetCurrentTime();
            userWorksheet.SalaryID = 1;
            _unitOfWorks.UserWorkSheetRepositories.Update(userWorksheet);
            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                //return workSheets;
                return true;
            }
            return false;
        }
        public async Task<int> CreateUserWorkSheet(AddUserWorkSheetViewModel addUserWorkSheetViewModel)
        {
            var userWorkSheet = _mapper.Map<UserWorkSheet>(addUserWorkSheetViewModel);
            /*userWorkSheet.UserId = _claimsServices.GetCurrentUserId;*/

            await _unitOfWorks.UserWorkSheetRepositories.AddAsync(userWorkSheet);

            if (await _unitOfWorks.SaveChangesAsync() == 0)
            {
                throw new Exception("Create Fail!!!");
            }
            return userWorkSheet.Id;
        }
    }
}
