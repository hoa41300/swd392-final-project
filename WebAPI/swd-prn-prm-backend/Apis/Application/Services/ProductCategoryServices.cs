﻿using Application.Interfaces;
using Application.ViewModels.InventoryViewModels;
using Application.ViewModels.ProductCategoryViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProductCategoryServices : IProductCategoryServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public ProductCategoryServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<List<ProductCategoryViewModel>> GetAllProductCategorysAsync()
        {
            var productCategory = await _unitOfWorks.ProductCategoryServicesRepositories.GetAllAsync();

            var listProductCategory = _mapper.Map<List<ProductCategoryViewModel>>(productCategory);
            return listProductCategory;
        }
    }
}
