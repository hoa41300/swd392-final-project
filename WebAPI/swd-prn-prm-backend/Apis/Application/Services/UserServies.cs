﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Enums;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Application.Services
{
    public class UserServies : IUserServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        private readonly ISessionServices _sessionServices;

        public UserServies(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration, ISessionServices sessionServices)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _sessionServices = sessionServices;
        }

        public async Task<string> LoginAsync(UserLoginModel userLogin)
        {
            var user = await _unitOfWorks.UserRepositories.GetUserByEmailAndPassword(userLogin.Email, userLogin.Password.Hash());
            if (user != null)
            {
                var token = user.GenerateJsonWebToken(_configuration["Jwt:Key"]!, _currentTime.GetCurrentTime(), _configuration);
                _sessionServices.SaveToken(user.Id, token);
                return token;
            };

            return string.Empty;
        }

        public async Task<int?> CreateUserAsync(UserCreateModel createdUser)
        {
            var user = _mapper.Map<Users>(createdUser);

            var checkDuplicatUsername = await _unitOfWorks.UserRepositories.GetUserByEmail(user.Email);

            if (checkDuplicatUsername == null)
            {
                user.Role = 3;
                user.Password = "123123".Hash();
                user.Avatar = "https://manga-reader-clone.s3.amazonaws.com/avatar.jpg";
                user.isActive = true;
                user.CreateDate = DateTime.Now;
                user.CofficientsSalary = 1;
                await _unitOfWorks.UserRepositories.AddAsync(user);
                var result = await _unitOfWorks.SaveChangesAsync() > 0;

                return user.Id;
            }

            return null;

        }

        public async Task<List<UserDetailModel>> GetAllUsersAsync()
        {
            var list = await _unitOfWorks.UserRepositories.GetAllAsync();
            var listView = _mapper.Map<List<UserDetailModel>>(list);
            return listView;
        }

        public async Task<string> GetUserName(string email)
        {
            var user = await _unitOfWorks.UserRepositories.GetNameByEmail(email);
            return user;
        }
        public async Task<UserDetailModel> GetUserByID(int id)
        {
            return _mapper.Map<UserDetailModel>(await _unitOfWorks.UserRepositories.GetByIdAsync(id));
        }
        public async Task<UserDetailModel> GetUserByEmail(string email)
        {
            var user = _mapper.Map<UserDetailModel>(await _unitOfWorks.UserRepositories.GetUserByEmail(email));
            return user;
        }

        public async Task<UserDetailModel> UpdateRoleForUser(int id)
        {
            var user = await _unitOfWorks.UserRepositories.GetByIdAsync(id);
            if (user == null) return null;

            user.Role = 2;

            _unitOfWorks.UserRepositories.Update(user);
            if(await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<UserDetailModel>(user);
            }
            throw new Exception();
        }
        public async Task<UserDetailModel> UpdateUserStatus(int id)
        {
            var user = await _unitOfWorks.UserRepositories.GetByIdAsync(id);
            if (user == null) return null;
            else {
                var status = user.isActive == true ? false : true;
                user.isActive = status;
            }

            _unitOfWorks.UserRepositories.Update(user);
            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<UserDetailModel>(user);
            }
            throw new Exception();
        }
        public async Task<UserDetailModel> ChangePassword(int id, ChangePasswordModels changePassword)
        {
            var user = await _unitOfWorks.UserRepositories.GetByIdAsync(id);
            if (user == null) return null;
            if (user.Password != changePassword.OldPassword.Hash()) return null;
            if (user.Password == changePassword.OldPassword.Hash())
            {
                user.Password = changePassword.NewPassword.Hash();
                _unitOfWorks.UserRepositories.Update(user);

            }
            else return null;

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<UserDetailModel>(user);
            }
            throw new Exception();
        }
        public async Task<UserDetailModel> UpdateUser(int id, UpdateUserModels updateUser)
        {
            var user = await _unitOfWorks.UserRepositories.GetByIdAsync(id);
            if (user == null) return null;

            #region checkUpdatenull
            string data = (updateUser.FullName == null) ? user.FullName : updateUser.FullName;
            user.FullName = data;

            data = (updateUser.Phone == null) ? user.Phone : updateUser.Phone;
            user.Phone = data;

            data = (updateUser.Address == null) ? user.Address : updateUser.Address;
            user.Address = data;

            data = (updateUser.Note == null) ? user.Note : updateUser.Note;
            user.Note = data;

            data = (updateUser.Email == null) ? user.Email : updateUser.Email;
            user.Email = data;

            if (updateUser.Gender == null) { user.Gender = user.Gender; }
            else
            {
                var gender = updateUser.Gender.ToUpper().Equals("MALE") ? true : false;
                user.Gender = gender;
            }

            Double? salary = (updateUser.Salary == null) ? user.CofficientsSalary : updateUser.Salary;
            user.CofficientsSalary = (double)salary;

            DateTime? date = (updateUser.DateOfBirth == null) ? user.DateOfBirth : updateUser.DateOfBirth;
            user.DateOfBirth = (DateTime)date;

            date = (updateUser.ContractExpiration == null) ? user.ContractExpiration : updateUser.ContractExpiration;
            user.ContractExpiration = (DateTime)date;

            date = (updateUser.StartToWork == null) ? user.StartToWork : updateUser.StartToWork;
            user.StartToWork = (DateTime)date;
            #endregion

            _unitOfWorks.UserRepositories.Update(user);
            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<UserDetailModel>(user);
            }
            throw new Exception();
        }
        public async Task<(List<UserDetailModel>, int)> FilterUser(UserFilterModel filter)
        {
            var listUser = await _unitOfWorks.UserRepositories.GetAllAsync();

            if (!string.IsNullOrEmpty(filter.name))
            {
                listUser = listUser.Where(x => x.FullName.ToUpper().Contains(filter.name.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(filter.role))
            {
                Role getRole = (Role)Enum.Parse(typeof(Role), filter.role);
                int intRole = (int)getRole;
                listUser = listUser.Where(x => x.Role.Equals(intRole)).ToList();
            }
            if (!string.IsNullOrEmpty(filter.gender))
            {
                bool ses = (filter.gender.ToUpper().Equals("MALE")) ? true : false;
                if (ses == true) listUser = listUser.Where(x => x.Gender.Equals(true)).ToList();
                else listUser = listUser.Where(x => x.Gender.Equals(false)).ToList();
            }
            if (!string.IsNullOrEmpty(filter.status))
            {
                bool isActive = (filter.status.ToUpper().Equals("ACTIVE") ? true : false);
                if (isActive == true) listUser = listUser.Where(x => x.isActive.Equals(true)).ToList();
                else listUser = listUser.Where(x => x.isActive.Equals(false)).ToList();
            }
            else if (!string.IsNullOrEmpty(filter.sortName) && !string.IsNullOrEmpty(filter.sortCreateDate))
            {
                if (filter.sortName.ToUpper().Equals("ASC") && filter.sortCreateDate.ToUpper().Equals("ASC"))
                {
                    listUser = listUser.OrderBy(x => x.FullName).OrderBy(x => x.CreateDate).ToList();
                }
                if (filter.sortName.ToUpper().Equals("ASC") && filter.sortCreateDate.ToUpper().Equals("DESC"))
                {
                    listUser = listUser.OrderBy(x => x.FullName).OrderByDescending(x => x.CreateDate).ToList();
                }
                if (filter.sortName.ToUpper().Equals("DESC") && filter.sortCreateDate.ToUpper().Equals("DESC"))
                {
                    listUser = listUser.OrderByDescending(x => x.FullName).OrderByDescending(x => x.CreateDate).ToList();
                }
                if (filter.sortName.ToUpper().Equals("DESC") && filter.sortCreateDate.ToUpper().Equals("ASC"))
                {
                    listUser = listUser.OrderByDescending(x => x.FullName).OrderBy(x => x.CreateDate).ToList();
                }
            }
            else if (string.IsNullOrEmpty(filter.sortName) && !string.IsNullOrEmpty(filter.sortCreateDate))
            {

                if (filter.sortCreateDate.ToUpper().Equals("DESC"))
                {
                    listUser = listUser.OrderByDescending(x => x.CreateDate).ToList();
                }
                if (filter.sortCreateDate.ToUpper().Equals("ASC"))
                {
                    listUser = listUser.OrderBy(x => x.CreateDate).ToList();
                }
            }
            else if (!string.IsNullOrEmpty(filter.sortName) && string.IsNullOrEmpty(filter.sortCreateDate))
            {

                if (filter.sortName.ToUpper().Equals("DESC"))
                {
                    listUser = listUser.OrderByDescending(x => x.FullName).ToList();
                }
                if (filter.sortName.ToUpper().Equals("ASC"))
                {
                    listUser = listUser.OrderBy(x => x.FullName).ToList();
                }
            }

            //paging
            var list = _mapper.Map<List<UserDetailModel>>(listUser);
            var totalPages = (int)Math.Ceiling((double)list.Count / filter.pageSize);
            int? itemsToSkip = (filter.pageNumber - 1) * filter.pageSize;
            list = list.Skip((int)itemsToSkip)
                        .Take((int)filter.pageSize)
                         .ToList();
            return (list, totalPages);
        }
    }
}
