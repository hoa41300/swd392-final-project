﻿using Application.Interfaces;
using Application.ViewModels.UserWorkSheetViewModels;
using Application.ViewModels.VoucherViewModels;
using Application.ViewModels.WorkSheetViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Application.Services
{
    public class WorkSheetServies : IWorkSheetServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public WorkSheetServies(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<List<WorkSheet>> AddWorkSheet(DateTime date)
        {
            List<AddWorkSheetViewModel> addworksheets = new List<AddWorkSheetViewModel>();
            addworksheets.Add(new AddWorkSheetViewModel(date, 1, 1));
            addworksheets.Add(new AddWorkSheetViewModel(date, 2, 1));
            addworksheets.Add(new AddWorkSheetViewModel(date, 3, 1));
            List<WorkSheet> workSheets =_mapper.Map<List<WorkSheet>>(addworksheets);
            await _unitOfWorks.WorkSheetRepositories.AddRangeAsync(workSheets);
            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                //return workSheets;
                return workSheets;
            }
            else
                throw new Exception("Create Fail!!!");
        }

        public List<DateTime> GetDateInWeek(DateTime dayInWeek) // return 7 days in week
        {
            List<DateTime> dates = new List<DateTime>();

            // Calculate the Monday and Sunday dates for the week containing the input date
            DateTime monday = dayInWeek.AddDays(-(int)dayInWeek.DayOfWeek + 1);

            // Create an array of DateTime objects representing the dates in the week
            for (int i = 0; i < 7; i++)
            {
                dates.Add(monday.AddDays(i));
            }

            return dates;
        }

        public async Task<List<WorkSheetViewModel>> GetWorkSheet(DateTime dayInWeek)
        {
            List<WorkSheetViewModel> dates = new List<WorkSheetViewModel>();
            var week = GetDateInWeek(dayInWeek);
            foreach (var date in week)
            {
                var worksheetList =await _unitOfWorks.WorkSheetRepositories.GetWorkSheetByDate(date);
                if (worksheetList.IsNullOrEmpty())
                {
                    var a = await AddWorkSheet(date);
                    worksheetList =  _mapper.Map<List<WorkSheet>>(a);
                }
                
                dates.AddRange(_mapper.Map<List<WorkSheetViewModel>>(worksheetList));
            }
            /*foreach (var workSheetViewModel in dates)
            {
                var userWorkSheet = await _unitOfWorks.UserWorkSheetRepositories.GetByIdAsync(workSheetViewModel.Id);
                workSheetViewModel.listUser = _mapper.Map<List<UserWorkSheetViewModel>>(userWorkSheet);
             }*/

            return dates;
        }

        public async Task<List<WorkSheetViewModel>> GetListWorkSheet(DateTime dayInWeek)
        {

            var wsList = await _unitOfWorks.WorkSheetRepositories.GetAllAsync();
            var result = _mapper.Map<List<WorkSheetViewModel>>(wsList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public async Task<WorkSheetViewModel> GetWorkSheetById(int wsId)
        {
            var wsList = await _unitOfWorks.WorkSheetRepositories.GetAllAsync();
            if (wsList.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                var workSheet = _mapper.Map<WorkSheetViewModel>(wsList.FirstOrDefault(x => x.Id.Equals(wsId)));
                return workSheet;
            }
        }

        /*public async Task<WorkSheetViewModel> UpdateWorkSheet(int wsId, WorkSheetViewModel wsViewModel)
        {
            var workSheet = await _unitOfWorks.WorkSheetRepositories.GetByIdAsync(wsId);
            if (workSheet == null)
            {
                return null;
            }
            else
            {
                workSheet.Shift = wsViewModel.Shift;
                workSheet.Date = wsViewModel.Date;
                workSheet.Status = wsViewModel.Status;

                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<WorkSheetViewModel>(workSheet);
            }
        }*/


    }
}
