﻿
using Application.Interfaces;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.RequestViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Application.Services
{
    public class RequestServices : IRequestServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        private readonly IClaimsServices _claimsServices;

        public RequestServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration, IClaimsServices claimsServices)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _claimsServices = claimsServices;
        }

        public async Task<CreateRequestViewModel> AddRequest(CreateRequestViewModel requestViewModel)
        {
            var request = _mapper.Map<Request>(requestViewModel);

            request.UserId = _claimsServices.GetCurrentUserId;
            request.Status = 1;
            await _unitOfWorks.RequestRepositories.AddAsync(request);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<CreateRequestViewModel>(request);
            }
            else throw new Exception("Create Fail!!!");
        }

        public async Task<bool> ChangeStatusToApproved(int id)
        {
            var request = await _unitOfWorks.RequestRepositories.GetByIdAsync(id);
            if (request == null)
            {
                return false;
            }
            else
            {
                request.AppovedBy = _claimsServices.GetCurrentUserId;
                request.Status = 2;

                await _unitOfWorks.SaveChangesAsync();
                return true;
            }
        }

        public async Task<bool> ChangeStatusToDenied(int id)
        {
            var request = await _unitOfWorks.RequestRepositories.GetByIdAsync(id);
            if (request == null)
            {
                return false;
            }
            else
            {
                request.AppovedBy = _claimsServices.GetCurrentUserId;
                request.Status = 3;

                await _unitOfWorks.SaveChangesAsync();
                return true;
            }
        }

        public async Task<List<RequestViewModel>> GetListAllRequest()
        {
            var requestList = await _unitOfWorks.RequestRepositories.GetAllAsync();
            var result = _mapper.Map<List<RequestViewModel>>(requestList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public async Task<List<RequestViewModel>> GetListRequestByUserId()
        {
            var requestList = _unitOfWorks.RequestRepositories.GetListRequestByUserId(_claimsServices.GetCurrentUserId);
            var result = _mapper.Map<List<RequestViewModel>>(requestList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public Task<RequestViewModel> UpdateRequest(int invoiceId, RequestViewModel requestViewModel)
        {
            throw new NotImplementedException();
        }
    }
}
