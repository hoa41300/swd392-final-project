﻿

using Application.Interfaces;
using Application.ViewModels.ImportViewModel;
using Application.ViewModels.Inventoryviewmodels;
using Application.ViewModels.InventoryViewModels;
using Application.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Application.Services
{
    public class InventoryServices : IInventoryServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public InventoryServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<InventoryViewModel> AddInventory(CreateInventoryViewModel inventory)
        {
            var newInvenroty = _mapper.Map<Inventory>(inventory);
            newInvenroty.isInUser = true;

            await _unitOfWorks.InventoryRepositories.AddAsync(newInvenroty);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                var InvenList =await _unitOfWorks.InventoryRepositories.GetAllAsync();
                var Inven = InvenList.FirstOrDefault (x => x.Id == newInvenroty.Id);
                return _mapper.Map<InventoryViewModel>(Inven);
            }
            else return null;
        }

        public async Task<bool> DeleteInventory(int id)
        {
            var listInventory = await _unitOfWorks.InventoryRepositories.GetAllAsync();
            if (listInventory == null)
            {
                throw new Exception();
            }
            else
            {
                var inventory = listInventory.FirstOrDefault(x => x.Id.Equals(id));
                if (inventory != null)
                {
                    await _unitOfWorks.InventoryRepositories.DeleteAsync(inventory);
                    await _unitOfWorks.SaveChangesAsync();
                    return true;
                }
                else return false;
            }
        }

        public async Task<List<InventoryViewModel>> GetAllInventory()
        {
            var inventory = await _unitOfWorks.InventoryRepositories.GetAllAsync();
            var listView = _mapper.Map<List<InventoryViewModel>>(inventory);
            return listView;
        }

        public async Task<InventoryViewModel> GetInventoryById(int id)
        {
            var Listinventory = await _unitOfWorks.InventoryRepositories.GetAllAsync();
            if (Listinventory == null)
            {
                throw new Exception();
            }
            else
            {
                var inventory = Listinventory.FirstOrDefault(x => x.Id.Equals(id));
                if (inventory != null)
                {
                    return _mapper.Map<InventoryViewModel>(inventory);
                }
                else return null;
            }
        }

        public async Task<InventoryViewModel> UpdateInventoryStatus(int id)
        {
            var inventory = await _unitOfWorks.InventoryRepositories.GetByIdAsync(id);
            if (inventory == null)
            {
                return null;
            }
            else
            {
                var result = inventory.isInUser == true ? false : true;
                inventory.isInUser = result;

                _unitOfWorks.InventoryRepositories.Update(inventory);
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<InventoryViewModel>(inventory);
            }
        }

        public async Task<InventoryViewModel> UpdateInventory(int id, UpdateInventoryViewModels update)
        {
            var inventory = await _unitOfWorks.InventoryRepositories.GetByIdAsync(id);
            if (inventory == null)
            {
                return null;
            }
            else
            {
                var temp = 0;
                temp = inventory.CategoryId == 0 ? inventory.CategoryId : update.CategoryId;
                inventory.CategoryId = temp;
                //
                string name = (inventory.Name.IsNullOrEmpty()) ? inventory.Name : update.Name;
                inventory.Name = name;
                //
                bool isUse = update.isInUser.ToUpper().Equals("TRUE") ? true : false;
                inventory.isInUser = isUse;


                _unitOfWorks.InventoryRepositories.Update(inventory);
                await _unitOfWorks.SaveChangesAsync();

                var result = await _unitOfWorks.InventoryRepositories.GetByIdAsync(id);

                return _mapper.Map<InventoryViewModel>(result);
            }
        }
        public async Task<(List<InventoryViewModel>, int)> FilterInventory(FilterInventoryModels filter)
        {
            var listInventory = await _unitOfWorks.InventoryRepositories.GetAllAsync();

            if (!string.IsNullOrEmpty(filter.Name))
            {
                listInventory = listInventory.Where(x => x.Name.ToUpper().Contains(filter.Name.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(filter.Category))
            {
                var InventoryCategoryList = await _unitOfWorks.InventoryCatagoryRepositories.GetAllAsync();
                var getCategory = InventoryCategoryList.Where(x => x.Name.ToUpper().Contains(filter.Category.ToUpper())).ToList();
                if (getCategory == null) { listInventory = null; }
                else
                {
                    List<Inventory> listCate = new List<Inventory>();
                    foreach (var item in getCategory)
                    {
                        var categoryItems = listInventory.FindAll(x => x.CategoryId == item.Id);
                        listCate.AddRange(categoryItems);
                    }
                    listInventory = listCate;
                }
            }
            if (!string.IsNullOrEmpty(filter.isInUser))
            {
                bool isUse = (filter.isInUser.ToUpper().Equals("TRUE")) ? true : false;
                listInventory = listInventory.Where(x => x.isInUser == isUse).ToList();
            }
            //sort
            else
            {
                if (!string.IsNullOrEmpty(filter.sortName) && !string.IsNullOrEmpty(filter.sortQuantity))
                {
                    if (filter.sortName.ToUpper().Equals("ASC") && filter.sortQuantity.ToUpper().Equals("ASC"))
                    {
                        listInventory = listInventory.OrderBy(x => x.Name).OrderBy(x => x.Quantity).ToList();
                    }
                    if (filter.sortName.ToUpper().Equals("ASC") && filter.sortQuantity.ToUpper().Equals("DESC"))
                    {
                        listInventory = listInventory.OrderBy(x => x.Name).OrderByDescending(x => x.Quantity).ToList();
                    }
                    if (filter.sortName.ToUpper().Equals("DESC") && filter.sortQuantity.ToUpper().Equals("DESC"))
                    {
                        listInventory = listInventory.OrderByDescending(x => x.Name).OrderByDescending(x => x.Quantity).ToList();
                    }
                    if (filter.sortName.ToUpper().Equals("DESC") && filter.sortQuantity.ToUpper().Equals("ASC"))
                    {
                        listInventory = listInventory.OrderByDescending(x => x.Name).OrderBy(x => x.Quantity).ToList();
                    }
                }
                else if (string.IsNullOrEmpty(filter.sortName) && !string.IsNullOrEmpty(filter.sortQuantity))
                {

                    if (filter.sortQuantity.ToUpper().Equals("DESC"))
                    {
                        listInventory = listInventory.OrderByDescending(x => x.Quantity).ToList();
                    }
                    if (filter.sortQuantity.ToUpper().Equals("ASC"))
                    {
                        listInventory = listInventory.OrderBy(x => x.Quantity).ToList();
                    }
                }
                else if (!string.IsNullOrEmpty(filter.sortName) && string.IsNullOrEmpty(filter.sortQuantity))
                {

                    if (filter.sortName.ToUpper().Equals("DESC"))
                    {
                        listInventory = listInventory.OrderByDescending(x => x.Name).ToList();
                    }
                    if (filter.sortName.ToUpper().Equals("ASC"))
                    {
                        listInventory = listInventory.OrderBy(x => x.Name).ToList();
                    }
                }
            }
            //paging
            var list = _mapper.Map<List<InventoryViewModel>>(listInventory);
            var totalPages = (int)Math.Ceiling((double)list.Count / filter.pageSize);
            int? itemsToSkip = (filter.pageNumber - 1) * filter.pageSize;
            list = list.Skip((int)itemsToSkip)
                        .Take((int)filter.pageSize)
                         .ToList();
            return (list, totalPages);
        }

        public async Task<InventoryViewModel> UpdateQuantity(int id, int newQuantity)
        {
            var inventory = await _unitOfWorks.InventoryRepositories.GetByIdAsync(id);
            if (inventory == null)
            {
                return null;
            }
            else
            {
                inventory.Quantity = newQuantity;
                _unitOfWorks.InventoryRepositories.Update(inventory);
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<InventoryViewModel>(inventory);
            }
        }
    }
}
