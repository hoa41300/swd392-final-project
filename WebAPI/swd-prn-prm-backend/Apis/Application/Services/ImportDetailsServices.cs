﻿using Application.Interfaces;
using Application.ViewModels.ImportViewModel;
using AutoMapper;
using Domain.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;

namespace Application.Services
{
    public class ImportDetailsServices : IImportDetailsServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public ImportDetailsServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<bool> DeleteImportDetail(int id)
        {
            var listImport = await _unitOfWorks.ImportDetailsRepositories.GetAllAsync();
            if (listImport == null)
            {
                throw new Exception();
            }
            else
            {
                var Import = listImport.FirstOrDefault(x => x.Id.Equals(id));
                if (Import != null)
                {
                    await _unitOfWorks.ImportDetailsRepositories.DeleteAsync(Import);
                    await _unitOfWorks.SaveChangesAsync();
                    return true;
                }
                else return false;
            }
        }

        public async Task<List<ImportDetailsViewModel>> GetAllImportDetails()
        {
            var listImport = await _unitOfWorks.ImportDetailsRepositories.GetAllAsync();
            var listView = _mapper.Map<List<ImportDetailsViewModel>>(listImport);
            return listView;
        }

        public async Task<ImportDetailsViewModel> GetImportById(int id)
        {

            var listIm = await _unitOfWorks.ImportDetailsRepositories.GetAllAsync();
            if (listIm == null)
            {
                throw new Exception();
            }
            else
            {
                var Import = listIm.FirstOrDefault(x => x.Id.Equals(id));
                if (Import != null)
                {
                    return _mapper.Map<ImportDetailsViewModel>(Import);
                }
                else return null;
            }
        }

        public async Task<ImportDetailsViewModel> NewImportDetailsl(CreateImportDetailViewModel importDetail)
        {
            var newImport = _mapper.Map<ImportDetails>(importDetail);

            newImport.ImportId = importDetail.ImportId;
            newImport.InventoryId = importDetail.InventoryId;
            newImport.Quantity = importDetail.Quantity;
            newImport.Price = importDetail.Price;

            await _unitOfWorks.ImportDetailsRepositories.AddAsync(newImport);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<ImportDetailsViewModel>(newImport);
            }
            else return null;
        }

        public async Task<ImportDetailsViewModel> UpdateImportDetail(int id, CreateImportDetailViewModel Update)
        {
            var Import = await _unitOfWorks.ImportDetailsRepositories.GetByIdAsync(id);    
            if (Import == null)
            {
                return null;
            }
            else
            {
                var result = 0;
                //
                result = Update.ImportId == 0? Import.ImportId : Update.ImportId;
                Import.ImportId = result;
                //
                result = Update.InventoryId == 0 ? Import.InventoryId : Update.InventoryId;
                Import.InventoryId = result;
                //
                result = Update.Quantity == 0 ? Import.Quantity : Update.Quantity;
                Import.Quantity = result;
                //
                result = Update.Price == 0 ? Import.Price : Update.Price;
                Import.Price = result;

                _unitOfWorks.ImportDetailsRepositories.Update(Import);
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<ImportDetailsViewModel>(Import);
            }
        }
    }
}
