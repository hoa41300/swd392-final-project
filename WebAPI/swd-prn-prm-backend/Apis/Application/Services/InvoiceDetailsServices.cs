﻿using Application.Interfaces;
using Application.ViewModels.InvoiceDetailsViewModels;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.OverTimeWorkSheetViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;

namespace Application.Services
{
    public class InvoiceDetailsServices : IInvoiceDetailsServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public InvoiceDetailsServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

    }
}
