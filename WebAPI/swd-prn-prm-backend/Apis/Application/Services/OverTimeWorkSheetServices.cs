﻿using Application.Interfaces;
using Application.ViewModels.OverTimeWorkSheetViewModels;
using Application.ViewModels.VoucherViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class OverTimeWorkSheetServices : IOverTimeWorkSheetServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly IClaimsServices _claimServices;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;

        public OverTimeWorkSheetServices(IClaimsServices claimServices,IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration)
        {
            _unitOfWorks = unitOfWorks;
            _claimServices = claimServices;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
        }

        public async Task<OverTimeWorkSheetViewModel> AddOverTimeWorkSheet(OverTimeWorkSheetViewModel ovtwsViewModel)
        {
            var overTimeWorkSheetNew = _mapper.Map<OverTimeWorkSheet>(ovtwsViewModel);

            /*overTimeWorkSheetNew.UserId= _claimServices.*/
            await _unitOfWorks.OvertimeWorkSheetRepositories.AddAsync(overTimeWorkSheetNew);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<OverTimeWorkSheetViewModel>(overTimeWorkSheetNew);
            }
            else throw new Exception("Create Fail!!!");
        }

        public async Task<List<OverTimeWorkSheetViewModel>> GetListOverTimeWorkSheet()
        {
            var ovtwsList = await _unitOfWorks.OvertimeWorkSheetRepositories.GetAllAsync();
            var result = _mapper.Map<List<OverTimeWorkSheetViewModel>>(ovtwsList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public async Task<OverTimeWorkSheetViewModel> GetOverTimeWorkSheetById(int ovtwsId)
        {
            var ovtwsList = await _unitOfWorks.OvertimeWorkSheetRepositories.GetAllAsync();
            if (ovtwsList.IsNullOrEmpty()) return null;
            else
            {
                var ovtws = _mapper.Map<OverTimeWorkSheetViewModel>(ovtwsList.FirstOrDefault(x => x.Id.Equals(ovtwsId)));
                return ovtws;
            }
        }

        public async Task<OverTimeWorkSheetViewModel> UpdateOverTimeWorkSheet(int ovtwsId, OverTimeWorkSheetViewModel ovtwsUpdate)
        {
            var ovtws = await _unitOfWorks.OvertimeWorkSheetRepositories.GetByIdAsync(ovtwsId);
            if (ovtws == null)
            {
                return null;
            }
            else
            {
                ovtws.Total = ovtwsUpdate.Total;
                ovtws.StartTime = ovtwsUpdate.StartTime;
                ovtws.EndTime = ovtwsUpdate.EndTime;


                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<OverTimeWorkSheetViewModel>(ovtws);
            }
        }
    }
}
