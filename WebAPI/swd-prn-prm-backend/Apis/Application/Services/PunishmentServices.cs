﻿
using Application.Interfaces;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.PunishmentViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Application.Services
{
    public class PunishmentServices : IPunishmentServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        private readonly IClaimsServices _claimsServices;

        public PunishmentServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration, IClaimsServices claimsServices)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _claimsServices= claimsServices;
        }

        public async Task<CreatePunishmentViewModel> AddPunishment(CreatePunishmentViewModel createPunishmentViewModel)
        {
            var punishment = _mapper.Map<Punishment>(createPunishmentViewModel);

            punishment.Date = _currentTime.GetCurrentTime().Date;
            punishment.isDeleted = false;
            await _unitOfWorks.PunishmentRepositories.AddAsync(punishment);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return _mapper.Map<CreatePunishmentViewModel>(punishment);
            }
            else throw new Exception("Create Fail!!!");
        }

        public Task<bool> DeletePunishment()
        {
            throw new NotImplementedException();
        }

        public async Task<List<PunishmentViewModels>> GetListPunishment()
        {
            var punishmentList = await _unitOfWorks.PunishmentRepositories.GetAllAsync();
            var result = _mapper.Map<List<PunishmentViewModels>>(punishmentList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public async Task<List<PunishmentViewModels>> GetListPunishmentByUserId()
        {
            var punishmentList = _unitOfWorks.PunishmentRepositories.GetListPunishmentByUserId(_claimsServices.GetCurrentUserId);
            var result = _mapper.Map<List<PunishmentViewModels>>(punishmentList);
            if (result.IsNullOrEmpty())
            {
                return null;
            }
            else
            {
                return result;
            }
        }

        public async Task<PunishmentViewModels> UpdatePunishment(int invoiceId, PunishmentViewModels punishmentViewModels)
        {
            var punishment = await _unitOfWorks.PunishmentRepositories.GetByIdAsync(invoiceId);
            if (punishment == null)
            {
                return null;
            }
            else
            {
                punishment.Date = _currentTime.GetCurrentTime().Date;
                punishment.Fine = punishmentViewModels.Fine;
                punishment.Note = punishmentViewModels.Note;
                punishment.Reason = punishmentViewModels.Reason;

                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<PunishmentViewModels>(punishment);
            }
        }
    }
}
