﻿using Application.Interfaces;
using Application.ViewModels.DamagesAsset;
using Application.ViewModels.ImportViewModel;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;

namespace Application.Services
{
    public class ImportServices : IImportServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        private readonly IUserServices _userServices;
        private readonly IClaimsServices _claimsServices;

        public ImportServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration,IUserServices userServices, IClaimsServices claimsServices)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _userServices = userServices;
            _claimsServices = _claimsServices;
        }

        public async Task<bool> DeleteImport(int id)
        {
            var listImport = await _unitOfWorks.ImportItemsRepositories.GetAllAsync();
            if (listImport == null)
            {
                throw new Exception();
            }
            else
            {
                var Import = listImport.FirstOrDefault(x => x.Id.Equals(id));
                if (Import != null)
                {
                    await _unitOfWorks.ImportItemsRepositories.DeleteAsync(Import);
                    await _unitOfWorks.SaveChangesAsync();
                    return true;
                }
                else return false;
            }
        }

        public async Task<List<ImportViewModel>> GetAllImports()
        {
            var listImport = await _unitOfWorks.ImportItemsRepositories.GetAllAsync();
            var listView = _mapper.Map<List<ImportViewModel>>(listImport);
            return listView;
        }

        public async Task<ImportViewModel> GetImportById(int id)
        {
            var listIm = await _unitOfWorks.ImportItemsRepositories.GetAllAsync();
            if (listIm == null)
            {
                throw new Exception();
            }
            else
            {
                var Import = listIm.FirstOrDefault(x => x.Id.Equals(id));
                if (Import != null)
                {
                    return _mapper.Map<ImportViewModel>(Import);
                }
                else return null;
            }
        }

        public async Task<int> CreateImport(CreateImportViewModel import)
        {
            var newImport = _mapper.Map<Import>(import);

            newImport.Price = import.Price;
            newImport.UserId = _claimsServices.GetCurrentUserId;
            newImport.Users = await _unitOfWorks.UserRepositories.GetByIdAsync(1);
            await _unitOfWorks.ImportItemsRepositories.AddAsync(newImport);

            if (await _unitOfWorks.SaveChangesAsync() > 0)
            {
                return newImport.Id;
            }
            else return 0;
        }

        public async Task<ImportViewModel> UpdateImport(int id, UpdateImportViewModel importViewModel)
        {
            var Import = await _unitOfWorks.ImportItemsRepositories.GetByIdAsync(id);
            if (Import == null)
            {
                return null;
            }
            else
            {
                Import.Price = importViewModel.Price;

                _unitOfWorks.ImportItemsRepositories.Update(Import);
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<ImportViewModel>(Import);
            }
        }
        public async Task<ImportViewModel> UpdateImportStatus(int id, int StatusNumber)
        {
            var Import = await _unitOfWorks.ImportItemsRepositories.GetByIdAsync(id);
            if (Import == null)
            {
                return null;
            }
            else
            {
                Import.Status = StatusNumber;
                _unitOfWorks.ImportItemsRepositories.Update(Import);
                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<ImportViewModel>(Import);
            }
        }
    }
}
