﻿using Application.Interfaces;
using Application.ViewModels.InvoiceDetailsViewModels;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.ProductViewModels;
using Application.ViewModels.VoucherViewModels;
using AutoMapper;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Application.Services
{
    public class InvoiceServices : IInvoiceServices
    {
        private readonly IUnitOfWorks _unitOfWorks;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;
        private readonly IConfiguration _configuration;
        private readonly IClaimsServices _claimsServices;

        public InvoiceServices(IUnitOfWorks unitOfWorks, IMapper mapper, ICurrentTime currentTime, IConfiguration configuration, IClaimsServices claimsServices)
        {
            _unitOfWorks = unitOfWorks;
            _mapper = mapper;
            _currentTime = currentTime;
            _configuration = configuration;
            _claimsServices = claimsServices;
        }

        public async Task<int> AddInvoice(AddInvoiceViewModel addInvoiceViewModel)
        {
            var invoice = _mapper.Map<Invoice>(addInvoiceViewModel);


            var invoiceDetaiList = new List<InvoiceDetails>();
            foreach (var x in addInvoiceViewModel.InvoiceDetails)
            {
                var product = await _unitOfWorks.ProductRepositories.GetByIdAsync(x.ProductId);
                InvoiceDetails invoiceDetail = new InvoiceDetails();
                invoiceDetail.ProductId = product.Id;
                invoiceDetail.Quantity = x.Quantity;
                invoiceDetail.Price = product.Price;
                invoiceDetail.Note = x.Note;
                invoiceDetail.Total = product.Price * x.Quantity;

                invoiceDetaiList.Remove(_mapper.Map<InvoiceDetails>(x));
                invoiceDetaiList.Add(invoiceDetail);
            }
            
            foreach (var x in invoiceDetaiList)
            {
                invoice.Total += x.Total;
            }

            invoice.UserId = _claimsServices.GetCurrentUserId;
            invoice.Status = (Domain.Enums.InvoiceEnum)1;
            invoice.Date = DateTime.Today;
            invoice.InvoiceDetails = null;
            //profit để tạm
            invoice.ProfitID = 1;
            await _unitOfWorks.InvoiceRepositories.AddAsync(invoice);

            if (await _unitOfWorks.SaveChangesAsync() == 0)
            {
                throw new Exception("Create Fail!!!");
            }
            foreach (var x in invoiceDetaiList)
            {
                x.InvoiceId = invoice.Id;
            }
            await _unitOfWorks.InvoiceDetailsRepositories.AddRangeAsync(invoiceDetaiList);
            
            if (await _unitOfWorks.SaveChangesAsync()==0)
            {
                throw new Exception("Create Fail!!!");
            }
            return invoice.Id;
        }

        public async Task<bool> ChangeStatusToDone(int id)
        {
            var invoice = await _unitOfWorks.InvoiceRepositories.GetByIdAsync(id);
            if (invoice == null)
            {
                return false;
            }
            else
            {
                invoice.Status = (Domain.Enums.InvoiceEnum)2;

                await _unitOfWorks.SaveChangesAsync();
                return true;
            }
        }
        public async Task<bool> ChangeStatusToCancel(int id)
        {
            var invoice = await _unitOfWorks.InvoiceRepositories.GetByIdAsync(id);
            if (invoice == null)
            {
                return false;
            }
            else
            {
                invoice.Status = (Domain.Enums.InvoiceEnum)3;

                await _unitOfWorks.SaveChangesAsync();
                return true;
            }
        }

        public async Task<InvoiceViewModel> GetInvoiceById(int invoiceId)
        {
            var invoice = await _unitOfWorks.InvoiceRepositories.GetByIdAsync(invoiceId);
            if (invoice != null)
            {
                return _mapper.Map<InvoiceViewModel>(invoice);
            }
            throw new Exception("Not found Invoice");
        }

        public async Task<(List<InvoiceViewModel>,int,double)> GetListInvoice(InvoiceFilter filter)
        {
            var invoiceList = await _unitOfWorks.InvoiceRepositories.GetAllAsync();

            if (filter.beginDate.HasValue && filter.endDate.HasValue)
            {
                invoiceList = invoiceList.Where(x => x.Date >= filter.beginDate && x.Date <=filter.endDate).ToList();
            } 
            else if (filter.beginDate.HasValue)
            {
                invoiceList = invoiceList.Where(x => x.Date >= filter.beginDate).ToList();
            }
            else if (filter.endDate.HasValue)
            {
                invoiceList = invoiceList.Where(x => x.Date <= filter.endDate).ToList();
            }

            invoiceList = invoiceList.OrderByDescending(x => x.Date).ToList();
            var list = _mapper.Map<List<InvoiceViewModel>>(invoiceList);
            var totalPages = (int)Math.Ceiling((double)list.Count / filter.pageSize);
            int? itemsToSkip = (filter.pageNumber - 1) * filter.pageSize;
            list = list.Skip((int)itemsToSkip)
                        .Take((int)filter.pageSize)
                         .ToList();
            var totalPrice = invoiceList.Sum(x => x.Total);
            return (list, totalPages,totalPrice);
        }

        public async Task<InvoiceViewModel> UpdateInvoice(InvoiceViewModel invoiceUpdate)
        {
            var invoice = await _unitOfWorks.InvoiceRepositories.GetByIdAsync(invoiceUpdate.Id);


            if (invoice == null)
            {
                return null;
            }
            else
            {
                _unitOfWorks.InvoiceDetailsRepositories.DeleteInvoiceDetail(invoiceUpdate.Id);

                var invoiceDetaiList = _mapper.Map<List<InvoiceDetails>>(invoiceUpdate.InvoiceDetails);

                await _unitOfWorks.InvoiceDetailsRepositories.AddRangeAsync(invoiceDetaiList);
                invoice.Date = DateTime.Parse(invoiceUpdate.Date);
                invoice.VoucherId = invoiceUpdate.VoucherId;

                await _unitOfWorks.SaveChangesAsync();
                return _mapper.Map<InvoiceViewModel>(invoice);
            }
        }

        public async Task<List<InvoiceViewModel>> GetInvoice()
        {
            var invoiceList = await _unitOfWorks.InvoiceRepositories.GetAllAsync();

            var list = _mapper.Map<List<InvoiceViewModel>>(invoiceList);
            
            return list;
        }
    }
}
