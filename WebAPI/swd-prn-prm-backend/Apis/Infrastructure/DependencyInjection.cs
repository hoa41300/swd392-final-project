﻿using Application;
using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using Domain.Models;
using Infrastructure;
using Infrastructure.Mappers;
using Infrastructure.Repositories;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfractstructure(this IServiceCollection services, IConfiguration config)
    {
        services.AddScoped<IUnitOfWorks, UnitOfWorks>();

        #region Config Repository and Service
        //User
        services.AddTransient<IUserRepositories, UserRepository>(); 
        services.AddTransient<IUserServices, UserServies>();
        
        //DameAsset
        services.AddTransient<IDamagesAssetRepositories, DamagesAssetRepository>();
        services.AddTransient<IDamagesAssetServies, DamagesAssetServices>();

        //Import
        services.AddTransient<IImportRepositories, ImportRepository>();
        services.AddTransient<IImportServices, ImportServices>();
        services.AddTransient<IImportDetailsRepositories, ImportDetailsReporitory>();
        services.AddTransient<IImportDetailsServices, ImportDetailsServices>();

        //Invoice
        services.AddTransient<IInvoiceDetailsRepositories, InvoiceDetailsRepository>();
        services.AddTransient<IInvoiceDetailsServices, InvoiceDetailsServices>();
        services.AddTransient<IInvoiceRepositories, InvoiceRepository>();
        services.AddTransient<IInvoiceServices, InvoiceServices>();

        //OverTime
        services.AddTransient<IOverTimeWorkSheetRepositories, OverTimeWorkSheetRepository>();
        services.AddTransient<IOverTimeWorkSheetServices, OverTimeWorkSheetServices>();

        //Product
        services.AddTransient<IProductRepositories, ProductRepository>();
        services.AddTransient<IProductServices, ProductServices>();

        //Punish
        services.AddTransient<IPunishmentRepositories, PunishmentRepository>();
        services.AddTransient<IPunishmentServices, PunishmentServices>();

        //Request
        services.AddTransient<IRequestRepositories, RequestRepository>();
        services.AddTransient<IRequestServices, RequestServices>();

        //UserWorkSheet
        services.AddTransient<IUserWorkSheetRepositories, UserWorkSheetRepository>();
        services.AddTransient<IUserWorkSheetServices, UserWorkSheetServices>();

        //Voucher
        services.AddTransient<IVoucherRepositories, VoucherRepository>();
        services.AddTransient<IVoucherServices, VoucherServies>();

        //WorkSheet
        services.AddTransient<IWorkSheetRepositories, WorkSheetRepository>();
        services.AddTransient<IWorkSheetServices, WorkSheetServies>();
        
        //Inventory
        services.AddTransient<IInventoryRepositories, InventoryRepository>();
        services.AddTransient<IInventoryServices, InventoryServices>();

        services.AddTransient<ISessionServices, SessionServices>();

        services.AddTransient<ISalaryRepository, SalaryRepository>();
        services.AddTransient<IProfitRepositories, ProfitRepository>();

        services.AddTransient<IProductCategoryServicesRepo, ProductCategoryRepository>();
        services.AddTransient<IProductCategoryServices, ProductCategoryServices>();

        services.AddTransient<IInventoryCatagoryRepositories, InventoryCategoryRepository>();
        services.AddTransient<IInventoryCategoryService, CategoryInventoryService>();

        #endregion

        services.AddSingleton<ICurrentTime, CurrentTime>();
        // Use local DB
        services.AddDbContext<AppDbContext>(opt => opt.UseSqlServer(config.GetConnectionString("Default")));
        services.AddAutoMapper(typeof(MapperConfigs).Assembly);

        return services;

    }
}
