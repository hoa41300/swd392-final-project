﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() { }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        //{
        //    optionBuilder.UseSqlServer("ConnectionsString");
        //}
        public DbSet<Users> Users { get; set; } 
        public DbSet<WorkSheet> WorkSheet { get; set; }
        public DbSet<UserWorkSheet> WorkSheetWork { get; set; }
        public DbSet<Request> Request { get; set; }
        public DbSet<Punishment> Punishments { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<InvoiceDetails> InvoiceDetails { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Import> Imports { get; set; }
        public DbSet<Inventory> Inventory { get; set; }
        public DbSet<Voucher> Voucher { get; set; }
        public DbSet<ImportDetails> ImportDetails { get; set; }
        public DbSet<DamagesAsset> DamagesAsset { get; set; }
        public DbSet<ProductCategory> ProductCategoryduct { get; set; }
        public DbSet<InventoryCategory> InventoryCategories { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")
                   .Build();
                var connectionString = configuration.GetConnectionString("Default");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

    }
}
