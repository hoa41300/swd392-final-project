﻿using Application.Interfaces;
using Application.ViewModels.UserWorkSheetViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/userworksheet")]
    [ApiController]
    public class UserWorkSheetController : Controller
    {
        private readonly IUserWorkSheetServices _services;
        public UserWorkSheetController(IUserWorkSheetServices services)
        {
            _services = services;
        }

        [HttpPut("CheckIn")]
        public async Task<IActionResult> CheckIn(int worksheetId)
        {
            var result = await _services.CheckIn(worksheetId);
            if (result) return Ok("Check In Success");
               
            return BadRequest();
        }

        [HttpPut("CheckOut")]
        public async Task<IActionResult> CheckOut(int worksheetId)
        {
            var result = await _services.CheckOut(worksheetId);
            if (result) return Ok("Check Out Success");

            return BadRequest();
        }
        [HttpPost()]
        public async Task<IActionResult> AddUserWorkSheet(AddUserWorkSheetViewModel addUserWorkSheetViewModel)
        {
            var result = await _services.CreateUserWorkSheet(addUserWorkSheetViewModel);
            if (result!= null) return Ok("Create User WorkSheet Success");

            return BadRequest();
        }

    }
}
