﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.Services;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.PunishmentViewModels;
using Application.ViewModels.RequestViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/punishment")]
    public class PunishmentController : Controller
    {
        private readonly IPunishmentServices _punishmentServices;
        public PunishmentController(IPunishmentServices punishmentServices)
        {
            _punishmentServices = punishmentServices;
        }

        [HttpPost("/CreatePunishment")]
        public async Task<IActionResult> CreatePunishment(CreatePunishmentViewModel createPunishmentViewModel)
        {
            try
            {
                var result = await _punishmentServices.AddPunishment(createPunishmentViewModel);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Create Punishment Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpGet("/GetAllPunishment")]
        public async Task<IActionResult> GetAllPunishment()
        {
            try
            {
                var result = await _punishmentServices.GetListPunishment();

                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get List Punishment Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpGet("/GetPunishmentByUserId")]
        public async Task<IActionResult> GetPunishmentByUserId()
        {
            try
            {
                var result = await _punishmentServices.GetListPunishmentByUserId();

                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get List Punishment By User Id Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpPut("/UpdatePunishment")]
        public async Task<IActionResult> UpdatePunishment(int id, PunishmentViewModels punishment)
        {
            try
            {
                var result = await _punishmentServices.UpdatePunishment(id, punishment);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Update Punishment Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
    }
}
