﻿using Application.Interfaces;
using Application.ViewModels.DamagesAsset;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/damage-asset")]
    [ApiController]
    public class DamageAssetController : ControllerBase
    {
        private readonly IDamagesAssetServies _services;
        public DamageAssetController(IDamagesAssetServies services)
        {
            _services = services;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllDamageAsset()
        {
            var result = await _services.GetAllDameAssets();
            try
            {
                if (result == null)
                {
                    return BadRequest("Get All Fail!");
                }
                return Ok(result);
            } catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetDamageAssetByID(int id)
        {
            var result = await _services.GetDameAssetById(id);
            if (result == null)
            {
                return BadRequest("Id does not exsist!!");
            }
            return Ok(result);
        }
        [HttpPost()]
        public async Task<IActionResult> NewDamageAsset(CreateDamageAssetViewModels dameasset)
        {
            var result = await _services.AddNewDameAsset(dameasset);
            if (result == null)
            {
                return BadRequest("Add new Fail!");
            }
            return Ok(result);
        }
        [HttpPut()]
        public async Task<IActionResult> UpdateDamageAsset(int id, UpdateDamageAssetViewModel updateDA)
        {
            var result = await _services.UpdateDameAsset(id, updateDA);
            if (result == null)
            {
                return BadRequest("Update Fail!");
            }
            return Ok(result);
        }
        [HttpDelete()]
        public async Task<IActionResult> DeleteDameAsset(int id)
        {
            var result = await _services.DeleteDameAsset(id);
            if (result == true)
            {
                return Ok("Delete Succesfully!");
            }
            return BadRequest("Delete Fail!");
        }
    }
}
