﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.Services;
using FluentAssertions.Common;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/product-category")]
    [ApiController]
    public class ProductCategoryController : Controller
    {

        private readonly IProductCategoryServices _services;
        public ProductCategoryController(IProductCategoryServices services)
        {
            _services = services;
        }


        [HttpGet]
        public async Task<IActionResult> GetAllProductCategory()
        {
            try
            {
                var result = await _services.GetAllProductCategorysAsync();

                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get List Product Category Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }

        }

    }
}
