﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.ViewModels.OverTimeWorkSheetViewModels;
using Application.ViewModels.VoucherViewModels;
using FluentAssertions.Common;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/overtime-worksheet")]
    [ApiController]
    public class OverTimeWorkSheetController : Controller
    {
        private readonly IOverTimeWorkSheetServices _overTimeWorkSheetServices;

        public OverTimeWorkSheetController(IOverTimeWorkSheetServices overTimeWorkSheetServices)
        {
            _overTimeWorkSheetServices = overTimeWorkSheetServices;
        }
        [HttpGet()]
        public async Task<IActionResult> GetOverTimeWorkSheet()
        {
            var result = await _overTimeWorkSheetServices.GetListOverTimeWorkSheet();
            try
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOverTimeWorkSheet(int id)
        {
            var result = await _overTimeWorkSheetServices.GetOverTimeWorkSheetById(id);
            try
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPut()]
        public async Task<IActionResult> UpdateOverTimeWorkSheet(int id, OverTimeWorkSheetViewModel ovtwsUpdate)
        {
            var result = await _overTimeWorkSheetServices.UpdateOverTimeWorkSheet(id, ovtwsUpdate);
            try
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Update Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPost()]
        public async Task<IActionResult> CreateOverTimeWorkSheet(OverTimeWorkSheetViewModel ovtws)
        {
            var result = await _overTimeWorkSheetServices.AddOverTimeWorkSheet(ovtws);
            try
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Create Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
    }
}
