﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.ViewModels.VoucherViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/voucher")]
    [ApiController]
    public class VouchersController : ControllerBase
    {
        private readonly IVoucherServices _services;
        public VouchersController(IVoucherServices services)
        {
            _services = services;
        }

        [HttpGet()]
        public async Task<IActionResult> GetVouchers()
        {
            try
            {
                var result = await _services.GetListVoucher();
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Voucher Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

    
    [HttpGet("{id}")]
        public async Task<IActionResult> GetVoucherById(int id)
        {
            var result = await _services.GetVoucherById(id);
            if (result != null) return Ok(result);
            return NotFound();
        }
        [HttpPut()]
        public async Task<IActionResult> updateVoucher(int id, CreateVoucherViewModel voucherU)
        {
            var result = await _services.UpdateVoucher(id, voucherU);
            try
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Voucher Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpPost()]
        public async Task<IActionResult> addVoucher(CreateVoucherViewModel Voucher)
        {
            var result = await _services.addNewVoucher(Voucher);
            try
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Create Success",
                    Result = result.Id
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

    [HttpPut("{sid}")]
        public async Task<IActionResult> changeVoucherStatus(int sid)
        {
            var result = await _services.changeVoucherStatus(sid);
            try
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Change Voucher Status Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

    }
}

