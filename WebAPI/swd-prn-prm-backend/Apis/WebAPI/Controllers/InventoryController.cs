﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.Services;
using Application.ViewModels.Inventoryviewmodels;
using Application.ViewModels.InventoryViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace WebAPI.Controllers
{
    [Route("api/inventory")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly IInventoryCategoryService _Cateservices;
        private readonly IInventoryServices _services;
        public InventoryController(IInventoryServices services, IInventoryCategoryService Cateservices)
        {

        _Cateservices = Cateservices;
        _services = services;
        }

        [HttpGet()]
        public async Task<IActionResult> GetAllInventory()
        {
            try
            {
                var result = await _services.GetAllInventory();
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Inventory Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetInventoryByID(int id)
        {
            try
            {
                var result = await _services.GetInventoryById(id);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Inventory Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPost()]
        public async Task<IActionResult> AddInventory(CreateInventoryViewModel Inventory)
        {
            try
            {
                var result = await _services.AddInventory(Inventory);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Create new Inventory Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPut()]
        public async Task<IActionResult> UpdateInventory(int id, [FromForm] UpdateInventoryViewModels Inventory)
        {
            try
            {
                var result = await _services.UpdateInventory(id, Inventory);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Udpate Inventory Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPatch]
        public async Task<IActionResult> UpdateInventoryQuantity(int id, int quantity)
        {
            try
            {
                var result = await _services.UpdateQuantity(id, quantity);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Udpate Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPut("{sid}")]
        public async Task<IActionResult> UpdateInventoryStatus(int id)
        {
            try
            {
                var result = await _services.UpdateInventoryStatus(id);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Update Inventory Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        //[HttpDelete()]
        //public async Task<IActionResult> DeleteInventory(int id)
        //{
        //    try
        //    {
        //        var result = await _services.DeleteInventory(id);
        //        return Ok(new BaseResponseModel
        //        {
        //            Status = Ok().StatusCode,
        //            Message = "Delete Inventory Success",
        //        });;
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(new BaseFailedResponseModel
        //        {
        //            Status = BadRequest().StatusCode,
        //            Message = ex.Message,
        //            Errors = ex
        //        });
        //    }
        //}
        [HttpGet("filter")]
        public async Task<IActionResult> FilterInventory ([FromQuery]FilterInventoryModels Filter)
        {
            var (list, totalPage) = await _services.FilterInventory(Filter);
            if (list.IsNullOrEmpty())
            {
                return Ok(new BaseResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = "Not Found"
                });
            }
            if (totalPage < Filter.pageNumber)
            {
                return BadRequest(new BaseResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = "Over number page"
                });
            }
            return Ok(new BaseResponseModel
            {
                Status = StatusCodes.Status200OK,
                Result = new
                {
                    List = list,
                    TotalPage = totalPage,
                }
            });
        }

        [HttpGet("category/list")]
        public async Task<IActionResult> GetInventoryCategory()
        {
            var inventoryCate = await _Cateservices.GetInventoryCategoryAsync();
            return Ok(new BaseResponseModel
            {
                Status = Ok().StatusCode,
                Result = inventoryCate
            });
        }
    }
}