﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.Services;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.VoucherViewModels;
using Domain.Models;
using FluentAssertions.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/invoice/")]
    public class InvoiceController : ControllerBase
    {
        private readonly IInvoiceServices _invoiceService;
        private readonly IInvoiceDetailsServices _invoiceDetailsService;

        public InvoiceController(IInvoiceServices invoiceService, IInvoiceDetailsServices invoiceDetailsService)
        {
            _invoiceService = invoiceService;
            _invoiceDetailsService = invoiceDetailsService;
        }
        
        [HttpPost()]
        public async Task<IActionResult> CreateInvoice(AddInvoiceViewModel invoice)
        {
            try
            {
                var result = await _invoiceService.AddInvoice(invoice);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Create Invoice Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpGet("filter")]
        public async Task<IActionResult> GetInvoices([FromQuery]InvoiceFilter filter)
        {
            try
            {
                var (list, totalPage,totalPrice) = await _invoiceService.GetListInvoice(filter);
                if (totalPage < filter.pageNumber)
                {
                    return BadRequest(new BaseResponseModel
                    {
                        Status = BadRequest().StatusCode,
                        Message = "Over number page",
                    });
                }
                return Ok(new BaseResponseModel
                {
                    Status = StatusCodes.Status200OK,
                    Result = new
                    {
                        List = list,
                        TotalPage = totalPage,
                        TotalPrice = totalPrice
                    }
                });
            }
            catch (Exception ex)
            {
                return NotFound(new BaseFailedResponseModel
                {
                    Status = NotFound().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        /*[HttpGet()]
        public async Task<IActionResult> GetInvoices()
        {
            try
            {
                var result = await _invoiceService.GetInvoice();

                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Invoices Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }*/


        [HttpGet("id")]
        public async Task<IActionResult> GetInvoicesById(int id)
        {
            try
            {
                InvoiceViewModel result = await _invoiceService.GetInvoiceById(id);

                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Invoices Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return NotFound(new BaseFailedResponseModel
                {
                    Status = NotFound().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        /* [HttpPut()]
         public async Task<IActionResult> UpdateInvoice(InvoiceViewModel invoice)
         {
             try
             {
                 var result = await _invoiceService.UpdateInvoice(invoice);
                 return Ok(new BaseResponseModel
                 {
                     Status = Ok().StatusCode,
                     Message = "Update Invoices Success",
                     Result = result
                 });
             }
             catch (Exception ex)
             {
                 return BadRequest(new BaseFailedResponseModel
                 {
                     Status = BadRequest().StatusCode,
                     Message = ex.Message,
                     Errors = ex
                 });
             }
         }*/

        [HttpPut("/ChangeStatusToDone/{id}")]
        public async Task<IActionResult> ChangeStatusToDone(int id)
        {
            try
            {
                if (await _invoiceService.ChangeStatusToDone(id))
                {
                    return Ok(new BaseResponseModel
                    {
                        Status = Ok().StatusCode,
                        Message = "Update Status To Done Success",
                    });
                }
                else
                {
                    return BadRequest(new BaseFailedResponseModel
                    {
                        Status = BadRequest().StatusCode,
                        Message = "Update Status To Done Failed",
                        Errors = "Not Found Invoice Id to Update"
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPut("/ChangeStatusToCancel/{id}")]
        public async Task<IActionResult> ChangeStatusToCancel(int id)
        {
            try
            {
                if (await _invoiceService.ChangeStatusToCancel(id))
                {
                    return Ok(new BaseResponseModel
                    {
                        Status = Ok().StatusCode,
                        Message = "Update Status To Cancel Success",
                    });
                }
                else
                {
                    return BadRequest(new BaseFailedResponseModel
                    {
                        Status = BadRequest().StatusCode,
                        Message = "Update Status To Cancel Failed",
                        Errors = "Not Found Invoice Id to Update"
                    });
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

    }
}
