﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.ViewModels.DamagesAsset;
using Application.ViewModels.ImportViewModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/import")]
    [ApiController]
    public class ImportController : ControllerBase
    {
        private readonly IImportServices _services;
        public ImportController(IImportServices services)
        {
            _services = services;
        }

        [HttpGet()]
        public async Task<IActionResult> GetAllImport()
        {
            try
            {
                var result = await _services.GetAllImports();
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Imports Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetImportByID(int id)
        {
            try
            {
                var result = await _services.GetImportById(id);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get Import Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpPost()]
        public async Task<IActionResult> NewImport(CreateImportViewModel import)
        {
            var result = await _services.CreateImport(import);
            if (result == null)
            {
                return BadRequest("Add new Fail!");
            }
            return Ok(result);
        }
        [HttpPut()]
        public async Task<IActionResult> UpdateImport(int id, UpdateImportViewModel updateIm)
        {
            var result = await _services.UpdateImport(id, updateIm);
            if (result == null)
            {
                return BadRequest("Update Fail!");
            }
            return Ok(result);
        }
        [HttpPut("{sid}")]
        public async Task<IActionResult> UpdateImportStatus(int id, int stats)
        {
            var result = await _services.UpdateImportStatus(id, stats);
            if (result == null)
            {
                return BadRequest("Update Fail!");
            }
            return Ok(result);
        }
        [HttpDelete()]
        public async Task<IActionResult> DeleteImport(int id)
        {
            var result = await _services.DeleteImport(id);
            if (result == true)
            {
                return Ok("Delete Succesfully!");
            }
            return BadRequest("Delete Fail!");
        }
    }
}
