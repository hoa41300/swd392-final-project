﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.Services;
using Application.ViewModels.InvoiceViewModels;
using Application.ViewModels.RequestViewModels;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/request")]
    public class RequestController : Controller
    {
        private readonly IRequestServices _requestServices;
        public RequestController(IRequestServices requestServices)
        {
            _requestServices = requestServices;
        }

        [HttpPost("/CreateRequest")]
        public async Task<IActionResult> CreateRequest(CreateRequestViewModel createRequestViewModel)
        {
            try
            {
                var result = await _requestServices.AddRequest(createRequestViewModel);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Create Invoice Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
        [HttpGet("/GetAllRequest")]
        public async Task<IActionResult> GetAllRequest()
        {
            try
            {
                var result = await _requestServices.GetListAllRequest();

                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get List Request Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpGet("/GetListRequestByUserId")]
        public async Task<IActionResult> GetRequestByUserId()
        {
            try
            {
                var result = await _requestServices.GetListRequestByUserId();

                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get List Request By User Id Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpPut("/UpdateRequest")]
        public async Task<IActionResult> UpdateRequest(int id, RequestViewModel invoice)
        {
            try
            {
                var result = await _requestServices.UpdateRequest(id, invoice);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Update Invoices Success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpPut("/ChangeStatusToApproved/{id}")]
        public async Task<IActionResult> ChangeStatusToApproved(int id)
        {
            try
            {
                if (await _requestServices.ChangeStatusToApproved(id))
                {
                    return Ok(new BaseResponseModel
                    {
                        Status = Ok().StatusCode,
                        Message = "Update Status To Done Approved",
                    });
                }
                else
                {
                    return BadRequest(new BaseFailedResponseModel
                    {
                        Status = BadRequest().StatusCode,
                        Message = "Update Status To Approved Failed",
                        Errors = "Not Found Request Id to Update"
                    });
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }

        [HttpPut("/ChangeStatusToDenied/{id}")]
        public async Task<IActionResult> ChangeStatusToDenied(int id)
        {
            try
            {
                if (await _requestServices.ChangeStatusToDenied(id))
                {
                    return Ok(new BaseResponseModel
                    {
                        Status = Ok().StatusCode,
                        Message = "Update Status To Done Denied",
                    });
                }
                else
                {
                    return BadRequest(new BaseFailedResponseModel
                    {
                        Status = BadRequest().StatusCode,
                        Message = "Update Status To Denied Failed",
                        Errors = "Not Found Request Id to Update"
                    });
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
    }
}
