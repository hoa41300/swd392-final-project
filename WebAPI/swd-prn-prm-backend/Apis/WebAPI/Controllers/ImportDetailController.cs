﻿using Application.Interfaces;
using Application.ViewModels.ImportViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/import-detail")]
    [ApiController]
    public class ImportDetailController : ControllerBase
    {
        private readonly IImportDetailsServices _services;
        public ImportDetailController(IImportDetailsServices services)
        {
            _services = services;
        }
            
        [HttpGet()]
        public async Task<IActionResult> GetAllImport()
        {
            var result = await _services.GetAllImportDetails();
            try
            {
                if (result == null)
                {
                    return BadRequest("Get All Fail!");
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetImportByID(int id)
        {
            var result = await _services.GetImportById(id);
            if (result == null)
            {
                return BadRequest("Id does not exsist!!");
            }
            return Ok(result);
        }
        [HttpPost()]
        public async Task<IActionResult> NewImport(CreateImportDetailViewModel import)
        {
            var result = await _services.NewImportDetailsl(import);
            if (result == null)
            {
                return BadRequest("Add new Fail!");
            }
            return Ok(result);
        }
        [HttpPut()]
        public async Task<IActionResult> UpdateImport(int id, CreateImportDetailViewModel updateIm)
        {
            var result = await _services.UpdateImportDetail(id, updateIm);
            if (result == null)
            {
                return BadRequest("Update Fail!");
            }
            return Ok(result);
        }
        [HttpDelete()]
        public async Task<IActionResult> DeleteImport(int id)
        {
            var result = await _services.DeleteImportDetail(id);
            if (result == true)
            {
                return Ok("Delete Succesfully!");
            }
            return BadRequest("Delete Fail!");
        }
    }
}
