﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.Services;
using Application.ViewModels.WorkSheetViewModels;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/work-sheet")]
    [ApiController]
    public class WorkSheetController : Controller
    {
        private readonly IWorkSheetServices _workSheetServices;
        public WorkSheetController(IWorkSheetServices workSheetServices)
        {
            _workSheetServices = workSheetServices;
        }

        /*[HttpGet()]
        public async Task<IActionResult> GetAllVouchers()
        {
            try
            {
                var result = await _workSheetServices.GetListWorkSheet();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }*/
        [HttpGet("{id}")]
        public async Task<IActionResult> GetWorkSheetById(int id)
        {
            var result = await _workSheetServices.GetWorkSheetById(id);
            if (result != null) return Ok(result);
            return NotFound();
        }
        /*[HttpPut()]
        public async Task<IActionResult> UpdateWorkSheet(int id, WorkSheetViewModel workSheet)
        {
            var result = await _workSheetServices.UpdateWorkSheet(id, workSheet);
            if (result != null) return Ok(result);
            return BadRequest("Update Fail");
        }*/
        [HttpGet()]
        public async Task<IActionResult> GetWorkSheet(String date)
        {
            try
            {
                DateTime inputDate = DateTime.ParseExact(date, "dd-MM-yyyy", null);
                var result = await _workSheetServices.GetWorkSheet(inputDate);
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "Get worksheet success",
                    Result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new BaseFailedResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = ex.Message,
                    Errors = ex
                });
            }
        }
    }
}
