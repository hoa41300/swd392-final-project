﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.ViewModels.UserViewModels;
using FluentValidation.AspNetCore;
using AutoMapper.Configuration.Conventions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.HttpResults;

namespace WebAPI.Controllers
{

    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private readonly IUserServices _userServices;

        public UserController(IUserServices userServices)
        {
            _userServices = userServices;
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromForm] UserLoginModel userLogin)
        {
            var token = await _userServices.LoginAsync(userLogin);
            var u = await _userServices.GetUserByEmail(userLogin.Email);
            if (!string.IsNullOrEmpty(token))
                return Ok(new BaseResponseModel
                {
                    Result = new
                    {
                        Token = token,
                        User = u
                    }
                });
            return NotFound(new BaseFailedResponseModel
            {
                Status = Ok().StatusCode,
                Message = "Incorrect Email or Password."
            });
        }

        [HttpPost()]
        public async Task<IActionResult> CreateUserAsync([FromForm] UserCreateModel userCreate)
        {
            var result = await _userServices.CreateUserAsync(userCreate);
            if (result != null)
            {
                var User = await _userServices.GetUserByEmail(userCreate.Email);
                return Ok(new BaseResponseModel
                {
                    Status = StatusCodes.Status200OK,
                    Message = "Create Succeed.",
                    Result = User.Id
                });
            }

            return BadRequest(new BaseFailedResponseModel
            {
                Status = Ok().StatusCode,
                Message = "Create Failed."
            });
        }

        [HttpGet("email")]
        public async Task<IActionResult> UserDetailAsync(string email)
        {
            var user = await _userServices.GetUserByEmail(email);
            if (user != null)
                return Ok(new BaseResponseModel
                {
                    Status = StatusCodes.Status200OK,
                    Message = "User Found.",
                    Result = user
                });
            return NotFound(new BaseFailedResponseModel
            {
                Status = Ok().StatusCode,
                Message = "User Not Found."
            });
        }
        [HttpGet("userId")]
        public async Task<IActionResult> UserDetailAsync(int Id)
        {
            var user = await _userServices.GetUserByID(Id);
            if (user != null)
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Message = "User Found.",
                    Result = user
                });
            return NotFound(new BaseFailedResponseModel
            {
                Status = Ok().StatusCode,
                Message = "User Not Found."
            });
        }

        [HttpGet]
        public async Task<IActionResult> UserDetailListAsync()
        {
            var list = await _userServices.GetAllUsersAsync();
            if (list.Count != 0)
                return Ok(new BaseResponseModel
                {
                    Status = StatusCodes.Status200OK,
                    Result = new
                    {
                        List = list
                    }
                });
            return NotFound(new BaseFailedResponseModel
            {
                Status = Ok().StatusCode,
                Message = "No User Found!"
            });
        }
        [HttpGet("filter")]
        public async Task<IActionResult> FilterUser([FromQuery] UserFilterModel filter)
        {
            var (list, totalPage) = await _userServices.FilterUser(filter);
            if (totalPage < filter.pageNumber)
            {
                return BadRequest(new BaseResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = "Over number page"
                });
            }
            return Ok(new BaseResponseModel
            {
                Status = StatusCodes.Status200OK,
                Result = new
                {
                    List = list,
                    TotalPage = totalPage,
                }
            }) ;
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdateUser(int id, [FromForm] UpdateUserModels updateUser)
        {
            var user = await _userServices.UpdateUser(id, updateUser);

            return Ok(new BaseResponseModel
            {
                Status = StatusCodes.Status200OK,
                Result = user
            });
        }
        [HttpPatch("role")]
        public async Task<IActionResult> setUserRoler(int id)
        {
            var user = await _userServices.UpdateRoleForUser(id);

            return Ok(new BaseResponseModel
            {
                Status = StatusCodes.Status200OK,
                Result = user
            });
        }
        [HttpPatch("password")]
        public async Task<IActionResult> ChangeUserPassword(int id,[FromForm] ChangePasswordModels changePassword)
        {
            if(changePassword.NewPassword != changePassword.ConfirmPassword)
            {
                return BadRequest(new BaseResponseModel
                {
                    Status = BadRequest().StatusCode,
                    Message = "Confirm password and New password not match!"

                });
            }
            var user = await _userServices.ChangePassword(id, changePassword);
            if (user != null)
            {
                return Ok(new BaseResponseModel
                {
                    Status = StatusCodes.Status200OK,
                    Message = "Password update successfully."
                });
            }
            else return BadRequest(new BaseResponseModel
            {
                Status = BadRequest().StatusCode,
                Message = "Incorrect Old password."
            });
        }
        [HttpPatch("status")]
        public async Task<IActionResult> setStatusUser(int id)
        {
            var user = await _userServices.UpdateUserStatus(id);

            return Ok(new BaseResponseModel
            {
                Status = StatusCodes.Status200OK,
                Result = user
            });
        }
    }
}
