﻿using Application.Interfaces;
using Application.ResponseModels;
using Application.Services;
using Application.ViewModels.ProductViewModels;
using FluentAssertions.Common;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/product")]
    public class ProductController : ControllerBase
    {
        private readonly IProductServices _productService;
        private readonly IProductCategoryServices _productCateService;

        public ProductController(IProductServices productService, IProductCategoryServices productCateService)
        {
            _productCateService = productCateService;
            _productService = productService;
        }

        //[Authorize(Roles = "Trainer,ClassAdmin,SuperAdmin")]

        [HttpPost]
        public async Task<IActionResult> CreateProduct(AddProductViewModel addProductViewModel)
        {
            try
            {
                var tempProduct = await _productService.AddProduct(addProductViewModel);
                if (tempProduct != null)
                {
                    return Ok(new BaseResponseModel
                    {
                        Status = Ok().StatusCode,
                        Result = tempProduct
                    }) ;
                }
                else
                {
                    return BadRequest("Please check your information of Product");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpGet]
        public async Task<IActionResult> GetProductById(int id)
        {
            var result = await _productService.GetProductById(id);
            if (result != null) return Ok(result);
            return NotFound();
        }

        /*[HttpGet("sortName")]
        public async Task<IActionResult> SortProductByName()
        {
            var result = await _productService.SortListByName();
            if (result != null) return Ok(result);
            return NotFound();
        }
        [HttpGet("sortPrice")]
        public async Task<IActionResult> SortProductByPrice()
        {
            var result = await _productService.SortListByPrice();
            if (result != null) return Ok(result);
            return NotFound();
        }*/


        [HttpPut()]
        public async Task<IActionResult> UpdateProduct(int id, UpdateProductViewModel updateProduct)
        {
            var result = await _productService.UpdateProduct(id, updateProduct);
            if (result == null) return NotFound();
            else
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Result = result,
                    Message = "Update Successfully"
                });
            }
        }
        [HttpPatch()]
        public async Task<IActionResult> UpdateProductStatus(int id)
        {
            var result = await _productService.UpdateStatus(id);
            if (result == null) return NotFound();
            else
            {
                return Ok(new BaseResponseModel
                {
                    Status = Ok().StatusCode,
                    Result = result,
                    Message = "Update Successfully"
                });
            }
        }

        [HttpGet("filter")]
        public async Task<IActionResult> FilterProduct([FromQuery]ProductFilterModel filter )
        {
            var (list, totalPage) = await _productService.FilterProduct(filter);
            if (totalPage < filter.pageNumber)
            {
                return NotFound(new BaseResponseModel
                {
                    Status = NotFound().StatusCode,
                    Message = "Over number page"
                });
            }
            return Ok(new BaseResponseModel
            {
                Status = StatusCodes.Status200OK,
                Result = new
                {
                    List = list,
                    TotalPage = totalPage
                }
            });
        }
        [HttpGet("category/list")]
        public async Task<IActionResult> GetProductCategory()
        {
            var productCate = await _productCateService.GetAllProductCategorysAsync();
            return Ok(new BaseResponseModel
            {
                Status = Ok().StatusCode,
                Result = productCate
            });
        }
    }
}
