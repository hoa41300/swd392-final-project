﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum UserPermission
    {
        AccessDenied = 1,
        View = 2,
        Modify = 3,
        Create = 4,
        FullAccess = 5
    }
}
