﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Salary
    {
        public int Id { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public DateTime CreateDate { get; set; }
        public int UserId { get; set; }
        public int ProfitId { get; set; }
        public int ProfitTotalImport { get; set; }

        public ICollection<UserWorkSheet> UserWorkSheets { get; set; }
        public Profit Profit { get; set; }
        public Users Users { get; set; }
    }
}
