﻿using Domain.Enums;

namespace Domain.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Payment { get; set; }
        public double Total { get; set; }
        public InvoiceEnum Status { get; set; }
        public int UserId { get; set; }
        public int? VoucherId { get; set; }
        public int ProfitID { get; set; }
        public int ProfitTatolImport { get; set; } = 1;

        public Users Users { get; set; }
        public Voucher? voucher { get; set; }


        public ICollection<InvoiceDetails?> InvoiceDetails { get; set; }
        public Profit? Profit { get; set; }
    }
}
