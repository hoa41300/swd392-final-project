﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class ImportDetails
    {
        public int Id {get; set;}
        public int InventoryId { get; set; }
        public int ImportId { get; set; }
        public int Quantity { get; set; }
        public int Price { get; set; }

        public Import? Import { get; set; }
        public Inventory? Inventory { get; set; }

    }
}
