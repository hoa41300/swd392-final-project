﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Users
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public DateTime StartToWork { get; set; }
        public string Phone { get; set; }
        public int? Role { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public string Email { get; set; }
        public bool isActive { get; set; } = true;
        public DateTime ContractExpiration { get; set; }
        public string? Avatar { get; set; }
        public bool Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime CreateDate { get; set; }
        public double CofficientsSalary { get; set; }

        //User Relationship
        public ICollection<UserWorkSheet> UserWorkSheets { get; set; }
        public ICollection<Request> requests { get; set; }
        public ICollection<Punishment> Punishments { get; set;}
        public ICollection<Import> imports { get; set; }
        public ICollection<DamagesAsset> damagesAssets { get; set; }
        public ICollection<Invoice> invoices { get; set; }
        public ICollection<OverTimeWorkSheet> overTimeWorkSheets { get; set;}
        public ICollection<Salary> Salaries { get; set; }

        //Permission
        //public UserPermission? UserPermission { get; set; }
        //public UserPermission? ImportPermission { get; set; }
        //public UserPermission? InventoryPermission { get; set; }
        //public UserPermission? InvoicePermission { get; set; }
        //public UserPermission? OverTimeWorkSheetPermission { get; set; }
        //public UserPermission? ProductPermission{ get; set; }
        //public UserPermission? PushnimentPermisstion { get; set; }
        //public UserPermission? RequestPermission { get; set; }
        //public UserPermission? VoucherPermission { get; set; }
    }
}
