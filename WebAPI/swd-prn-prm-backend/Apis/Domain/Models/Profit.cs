﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Profit
    {
        public int Id { get; set; }
        public double TotalSalary { get; set; }
        public double ToTalRevenue { get; set; }
        public double TotalImport {get; set; }
        public double Order { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string Note { get; set; }

        public ICollection<Import> Imports { get; set; }
        public ICollection<Salary> Salary { get; set; }
        public ICollection<Invoice> Invoices { get; set; }
    }
}
