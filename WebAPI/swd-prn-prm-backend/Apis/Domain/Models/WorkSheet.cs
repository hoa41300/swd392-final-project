﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class WorkSheet
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public ShiftEnum Shift { get; set; }
        public int? Status { get; set; }

        public ICollection <UserWorkSheet>? UserWorkSheet { get; set; }
    }
}
