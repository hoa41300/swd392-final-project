﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class DamagesAsset
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string NameAsset { get; set; } = string.Empty;
        public int Quantity { get; set; }

        public Users? Users { get; set; }
    }
}
