﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Import
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
        public int Price { get; set; }
        public int Status { get; set; }
        public string ImportIMG { get; set; }
        public int ProfitID { get; set; }
        public int ProfitTotalImport { get; set; }



        public Users? Users { get; set; }
        public ICollection<ImportDetails>? importDetails { get; set; }
        public Profit profit { get; set; }
    }
}
