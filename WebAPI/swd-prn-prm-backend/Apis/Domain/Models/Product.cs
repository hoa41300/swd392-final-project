﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }
        public Status Status { get; set; }
        public int Unit {get; set; }
        public bool OutOfStock { get; set; } 
        public string? ProductImg { get; set; }
        public double Price { get; set; }

        public ICollection<InvoiceDetails>? InvoiceDetails { get; set; }    
        public ProductCategory? ProductCategory { get; set; }
    }
}
