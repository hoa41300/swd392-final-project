﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class OverTimeWorkSheet
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int Total { get; set; }

        public Users Users { get; set; }
    }
}
