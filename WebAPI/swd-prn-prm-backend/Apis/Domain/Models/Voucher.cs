﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Voucher
    {
        public int Id { get; set; }
        public int TypeDiscount { get; set; }
        public int Value { get; set; }
        public bool isActive { get; set; }
        public DateTime CreateDate { get; set; }

        public ICollection<Invoice>? invoice { get; set; }
    }
}
