﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class InvoiceDetails
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public string? Note { get; set; }
        public double Total { get; set; }
        public int InvoiceId { get; set; }

        public Product? Product { get; set; }
        public Invoice? Invoice { get; set; }
    }
}
