﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Inventory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? Name { get; set; }
        public bool isInUser { get; set; }
        public int CategoryId { get; set; }
        public int Quantity { set; get; }

        public ICollection<ImportDetails>? ImportDetails { get; set; }
        public InventoryCategory InventoryCategories { get; set; }  
    }
}
