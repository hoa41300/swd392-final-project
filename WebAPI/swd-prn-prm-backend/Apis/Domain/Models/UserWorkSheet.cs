﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class UserWorkSheet
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int WorkSheetId { get; set; }
        public bool? Attend { get; set; } = false;
        public DateTime? CheckIn { get; set; }
        public string? CheckInImg { get; set; }
        public DateTime? CheckOut { get; set; }
        public int SalaryID { get; set; }

        public WorkSheet? WorkSheets { get; set; }
        public Users? Users { get; set; }
        public Salary? Salary { get; set; }
    }
}
