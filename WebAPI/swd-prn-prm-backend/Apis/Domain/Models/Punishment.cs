﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class Punishment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required] public string Reason { get; set; }
        public int Fine { get; set; }
        public string? Note { get; set; }
        public bool isDeleted { get; set; } = false;
        public DateTime Date { get; set; }

        public Users? Users { get; set; }
    }
}
